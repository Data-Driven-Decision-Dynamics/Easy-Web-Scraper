#' @return Provides generic Elo for a given race from geny website
#' @export
#' @importFrom read_csv readr
#' @importFrom group_by dplyr
#' @importFrom summarize dplyr
#' @importFrom xml2 read_html
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_ranking_generic_elo ("http://www.turfoo.fr/programmes-courses/180615/reunion1-vincennes/course1-prix-eudora/")
#' @export

get_ranking_generic_elo <- function(url_target_race_geny = "http://www.geny.com/partants-pmu/2018-11-11-avenches-pmu-prix-d-espagne_c1022359",
                                    path_input_ranking_data = "/userhomes/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_line_by_line_rating.rds") 
{
  
  # Getting information for current race
  mat_infos_target_race <- get_details_race_geny (url_path_race_details = url_target_race_geny)
  current_candidates <- mat_infos_target_race[,"Cheval"]
  current_candidates <- unlist(lapply(current_candidates,function(x){unlist(strsplit(x,"\r"))[1]}))
  current_candidates <- stringr::str_trim(current_candidates)
  current_discipline <- unique(mat_infos_target_race[,"Discipline"])

  # Reading line by line comparison file
  mat_line_by_line <- readRDS(path_input_ranking_data)
  mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Date),]
  mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Discipline),]
  mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Looser),]
  mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Looser_Distance),]
  mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Winner),]
  mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Winner_Distance),]
  
  # Subsetting to focus on current candidates
  # vec_starting_horses <- unique(unlist(subset(mat_line_by_line,Looser %in% current_candidates | Winner %in% current_candidates )[,c("Winner","Looser")]))
  mat_line_by_line_rating_focus <- subset(mat_line_by_line,Looser %in% current_candidates | Winner %in% current_candidates )
  mat_line_by_line_rating_focus_global <- mat_line_by_line_rating_focus
  
  # Calculating global ranking based on all distances
  if(nrow(mat_line_by_line_rating_focus_global)>0)
  {
    mat_line_by_line_rating_focus_global$Date <- as.Date(mat_line_by_line_rating_focus_global$Date)
    mat_line_by_line_rating_focus_global <- subset(mat_line_by_line_rating_focus_global,Discipline %in% current_discipline)
    mat_line_by_line_rating_focus_global  <- mat_line_by_line_rating_focus_global[order(mat_line_by_line_rating_focus_global$Date),]
    mat_line_by_line_rating_focus_global  <- unique(mat_line_by_line_rating_focus_global)
    mat_line_by_line_rating_focus_global <- mat_line_by_line_rating_focus_global [,c("Winner","Looser","Date")]
    mat_line_by_line_rating_focus_global <- mat_line_by_line_rating_focus_global[complete.cases(mat_line_by_line_rating_focus_global),]
    mat_line_by_line_rating_focus_global$Winner <- as.vector(mat_line_by_line_rating_focus_global$Winner)
    mat_line_by_line_rating_focus_global$Looser <- as.vector(mat_line_by_line_rating_focus_global$Looser)
    mat_line_by_line_rating_focus_global <- mat_line_by_line_rating_focus_global[complete.cases(mat_line_by_line_rating_focus_global),]
    mat_line_by_line_rating_focus_global <- mat_line_by_line_rating_focus_global[mat_line_by_line_rating_focus_global$Winner!=mat_line_by_line_rating_focus_global$Looser,]
    infos_res_generic_elo_golbal <- elo.seq(winner=mat_line_by_line_rating_focus_global$Winner, loser=mat_line_by_line_rating_focus_global$Looser, Date=mat_line_by_line_rating_focus_global$Date,runcheck=FALSE)
    
    mat_generic_elo_golbal <- data.frame(Cheval=names(extract.elo(infos_res_generic_elo_golbal)),Elo=as.numeric(extract.elo(infos_res_generic_elo_golbal)))
    mat_generic_elo_golbal <- subset(mat_generic_elo_golbal,Cheval %in% current_candidates)
    mat_generic_elo_temporal_profile_golbal <- infos_res_generic_elo_golbal$mat[,intersect(colnames(infos_res_generic_elo_golbal$mat),current_candidates)]
    
    if(length(colnames(mat_generic_elo_temporal_profile_golbal))>1){
      mat_generic_elo_temporal_profile_golbal <- as.data.frame(mat_generic_elo_temporal_profile_golbal)
      mat_generic_elo_temporal_profile_golbal$Date <- NA
      mat_generic_elo_temporal_profile_golbal$Date <- infos_res_generic_elo_golbal$truedates
      mat_generic_elo_temporal_profile_golbal$Year <- year(mat_generic_elo_temporal_profile_golbal$Date)
      mat_generic_elo_temporal_profile_golbal <- mat_generic_elo_temporal_profile_golbal[,setdiff(colnames(mat_generic_elo_temporal_profile_golbal),"Date")]
      
      mat_generic_elo_temporal_profile_golbal <- mat_generic_elo_temporal_profile_golbal %>%
        group_by(Year) %>% 
        summarise_all(funs(my_median)) %>% 
        as.data.frame()
      rownames(mat_generic_elo_temporal_profile_golbal) <- as.vector(mat_generic_elo_temporal_profile_golbal$Year)
      mat_generic_elo_temporal_profile_golbal <- mat_generic_elo_temporal_profile_golbal[,-1]
      mat_generic_elo_temporal_profile_golbal <- as.data.frame(t(mat_generic_elo_temporal_profile_golbal))
      mat_generic_elo_temporal_profile_golbal$Cheval <- NA
      mat_generic_elo_temporal_profile_golbal$Cheval <- as.vector(rownames(mat_generic_elo_temporal_profile_golbal))
      mat_generic_elo_temporal_profile_golbal <- mat_generic_elo_temporal_profile_golbal[,rev(colnames(mat_generic_elo_temporal_profile_golbal))[1:2]]
      rownames(mat_generic_elo_temporal_profile_golbal) <- NULL
      colnames(mat_generic_elo_temporal_profile_golbal)[2] <- "Elo_Global"
    }
    
    # Clustering found distances to be able to assess partial ranking
    vec_distance_found <- unique(as.vector(unlist(mat_line_by_line_rating_focus[,c("Winner_Distance","Looser_Distance")])))
    res_clusters_distances <- kmeans(vec_distance_found,4)
    mat_cluster_distance <- data.frame(Distance=vec_distance_found,Cluster=res_clusters_distances$cluster)
    mat_cluster_distance <- mat_cluster_distance[order(mat_cluster_distance$Distance),]
    mat_cluster_distance$Group <- NA
    vec_value_cluster <- unique(mat_cluster_distance$Cluster)
    for(clus in 1:length(vec_value_cluster)){
      mat_cluster_distance$Group[mat_cluster_distance$Cluster==vec_value_cluster[clus]] <- clus
    }
    mat_ranges_cluster_distance <- NULL
    for(clus in 1:length(vec_value_cluster)) {
      vec_ranges_distances <- sort(range(mat_cluster_distance[mat_cluster_distance$Cluster==vec_value_cluster[clus],"Distance"]))
      mat_ranges_cluster_distance_current <- data.frame(Speciality=clus,Lower=vec_ranges_distances[1],Upper=vec_ranges_distances[2])
      mat_ranges_cluster_distance <- rbind(mat_ranges_cluster_distance,mat_ranges_cluster_distance_current)
    }
    
    # Calculate partial ranking for each distance cluster
    list_distances <- vector('list',length(unique(mat_ranges_cluster_distance$Speciality)))
    names(list_distances) <- unique(mat_ranges_cluster_distance$Speciality)
    list_res_rating_specific_compliled <- vector("list",length(sort(unique(mat_ranges_cluster_distance$Speciality))))
    names(list_res_rating_specific_compliled) <- sort(unique(mat_ranges_cluster_distance$Speciality))
    for (clus in mat_ranges_cluster_distance$Speciality)
    {
      ranges_distance <- sort(range(as.numeric(unlist(subset(mat_ranges_cluster_distance,Speciality==clus)[,c("Lower","Upper")]))))
      list_distances [[clus]] <- ranges_distance
      id_distance_cluster <- mat_line_by_line_rating_focus[,'Winner_Distance']>=ranges_distance[1] & mat_line_by_line_rating_focus[,'Looser_Distance']>=ranges_distance[1] & mat_line_by_line_rating_focus[,'Winner_Distance']<ranges_distance[2] & mat_line_by_line_rating_focus[,'Looser_Distance']<ranges_distance[2]
      mat_line_by_line_subset <- mat_line_by_line_rating_focus[id_distance_cluster,]
      mat_line_by_line_subset$Date <- as.Date(mat_line_by_line_subset$Date)
      mat_line_by_line_subset <- subset(mat_line_by_line_subset,Discipline %in% current_discipline)
      mat_line_by_line_subset <- subset(mat_line_by_line_subset,Looser %in% current_candidates | Winner %in% current_candidates )
      if(nrow(mat_line_by_line_subset)>5){
        mat_line_by_line_subset_rating <- mat_line_by_line_subset[mat_line_by_line_subset$Winner!=mat_line_by_line_subset$Looser,]
        infos_res_specific_elo_golbal <- elo.seq(winner=mat_line_by_line_subset_rating$Winner, loser=mat_line_by_line_subset_rating$Looser, Date=mat_line_by_line_subset_rating$Date,runcheck=FALSE)
        mat_specific_elo_golbal <- data.frame(Cheval=names(extract.elo(infos_res_specific_elo_golbal)),Elo=as.numeric(extract.elo(infos_res_specific_elo_golbal)))
        mat_specific_elo_golbal <- subset(mat_specific_elo_golbal,Cheval %in% current_candidates)
        colnames(mat_specific_elo_golbal) <- c(colnames(mat_specific_elo_golbal)[1],paste(paste(ranges_distance,collapse ='-'),colnames(mat_specific_elo_golbal)[2],sep="_"))
        mat_specific_elo_temporal_profile_golbal <- infos_res_specific_elo_golbal$mat[,intersect(colnames(infos_res_specific_elo_golbal$mat),current_candidates)]
        mat_specific_elo_temporal_profile_golbal <- as.data.frame(mat_specific_elo_temporal_profile_golbal)
        mat_specific_elo_temporal_profile_golbal$Date <- NA
        mat_specific_elo_temporal_profile_golbal$Date <- infos_res_specific_elo_golbal$truedates
        mat_specific_elo_temporal_profile_golbal$Year <- year(mat_specific_elo_temporal_profile_golbal$Date)
        mat_specific_elo_temporal_profile_golbal <- mat_specific_elo_temporal_profile_golbal[,setdiff(colnames(mat_specific_elo_temporal_profile_golbal),"Date")]
        my_median <- function(x){median(x,na.rm = TRUE)}
        mat_specific_elo_temporal_profile_golbal <- mat_specific_elo_temporal_profile_golbal %>%
          group_by(Year) %>% 
          summarise_all(funs(my_median)) %>% 
          as.data.frame()
        rownames(mat_specific_elo_temporal_profile_golbal) <- as.vector(mat_specific_elo_temporal_profile_golbal$Year)
        mat_specific_elo_temporal_profile_golbal <- mat_specific_elo_temporal_profile_golbal[,-1]
        mat_specific_elo_temporal_profile_golbal <- as.data.frame(t(mat_specific_elo_temporal_profile_golbal))
        mat_specific_elo_temporal_profile_golbal$Cheval <- NA
        mat_specific_elo_temporal_profile_golbal$Cheval <- as.vector(rownames(mat_specific_elo_temporal_profile_golbal))
        mat_specific_elo_temporal_profile_golbal <- mat_specific_elo_temporal_profile_golbal[,rev(colnames(mat_specific_elo_temporal_profile_golbal))[1:2]]
        rownames(mat_specific_elo_temporal_profile_golbal) <- NULL
        colnames(mat_specific_elo_temporal_profile_golbal)[2] <- paste(ranges_distance,collapse ="_")
        list_res_rating_specific_compliled[[clus]] <- mat_specific_elo_temporal_profile_golbal
      }
    }
    
    # Combine the partial ranking
    list_res_rating_specific_compliled <- list_res_rating_specific_compliled[lapply(list_res_rating_specific_compliled,length)>0]
    mat_res_rating_specific_compliled <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_res_rating_specific_compliled)
  }
  
  # Combine global and partial ranking
  mat_res_rating_all_together <- Reduce(function(x,y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list(A=mat_generic_elo_temporal_profile_golbal,B=mat_res_rating_specific_compliled))
  return(mat_res_rating_all_together)
}