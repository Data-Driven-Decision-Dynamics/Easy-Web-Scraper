getDetailsGivenRacingTurfoo <- function(url.path='http://www.turfoo.fr/programmes-courses/180809/reunion1-enghien-soisy/course3-prix-de-la-haute-saone/')
{
  require(Hmisc)
  page.race <- tryCatch(read_html(url.path),error=function(e){cat("ERROR :",conditionMessage(e), "\n")})
  if(!is.null(page.race))
  {
    xpath.discipline <- "//*[@title=\"Discipline\"]"
    discipline <- str_trim(page.race %>% html_nodes(xpath=xpath.discipline) %>% html_text())
    discipline <- ifelse(length(discipline)==0,NA,discipline)
    
    xpath.distance <- "//*[@title=\"Distance\"]"
    distance <- str_trim(page.race %>% html_nodes(xpath=xpath.distance) %>% html_text())
    distance <- unlist(strsplit(distance,' '))[1]
    distance <- as.numeric(as.vector(sub(' mètres','',distance)))
    distance <- ifelse(length(distance)==0,NA,distance)
    
    xpath.course <- "//*[@class=\"course\"]"
    course <- str_trim(page.race %>% html_nodes(xpath=xpath.course) %>% html_text())
    course <- ifelse(length(course)==0,NA,course)
    
    xpath.nb.chevaux <- "//*[@title=\"Nombre\"]"
    nb.chevaux <- str_trim(page.race %>% html_nodes(xpath=xpath.nb.chevaux) %>% html_text())
    nb.chevaux <- as.numeric(as.vector(nb.chevaux))
    nb.chevaux <- ifelse(length(nb.chevaux)==0,NA,nb.chevaux)
    
    xpath.conditions <- "//*[@title=\"Conditions\"]"
    conditions.details <- str_trim(page.race %>% html_nodes(xpath=xpath.conditions) %>% html_text())
    conditions.participation <- conditions.details[1]
    conditions.participation <- ifelse(length(conditions.participation)==0,NA,conditions.participation)
    
    corde <- NA
    if(length(grep('corde |CORDE',conditions.participation))>0)
    {
      corde <- unlist(strsplit(unlist(strsplit(conditions.participation,'corde'))[2],'\\.'))[1]
      corde <- str_trim(sub('à','',corde))
      corde <- str_trim(sub('Ã','',corde))
      corde <- capitalize(tolower(corde))
    }
    
    corde <- ifelse(length(corde)==0,NA,corde)
    
    allocations <- conditions.details[2]
    allocations <- ifelse(length(allocations)==0,NA,allocations)
    
    xpath.lieu.date <- "//*[@class=\"lieudatecourse\"]"
    lieu.date <- str_trim(page.race %>% html_nodes(xpath=xpath.lieu.date) %>% html_text())
    lieu.date <- unlist(strsplit(lieu.date,','))
    
    lieu <- lieu.date[1]
    lieu <- sub('^-','',lieu)
    lieu <- ifelse(length(lieu)==0,NA,lieu)
    
    date <- str_trim(lieu.date[2])
    date <- ifelse(length(date)==0,NA,date)
    
    xpath.allocation <- "//*[@title=\"Prix\"]"
    allocation <- str_trim(page.race %>% html_nodes(xpath=xpath.allocation) %>% html_text())
    allocation <- as.numeric(unlist(strsplit(allocation,' '))[1])
    allocation <- ifelse(length(allocation)==0,NA,allocation)
    
    xpath.heure.depart <- "//*[@title=\"Heure\"]"
    heure.depart <- str_trim(page.race %>% html_nodes(xpath=xpath.heure.depart) %>% html_text())
    heure.depart <- ifelse(length(heure.depart)==0,NA,heure.depart)
    
    xpath.terrain <- "//*[@title=\"Terrain\"]"
    terrain <- str_trim(page.race %>% html_nodes(xpath=xpath.terrain) %>% html_text())
    terrain <- ifelse(length(terrain)==0,NA,terrain)
    
    reference <- NA
    if(sum(grep('handicap',conditions.details,ignore.case = TRUE))>0)
    {
      reference <- unlist(strsplit(as.vector(conditions.details),"Réf. +"))
      reference  <- reference [seq(2,length(reference ),by=2)]
      reference  <- substr(reference ,2,4)
      reference  <- as.numeric(reference)
      if(!is.na(reference) & reference>99.9)
      {
        reference  <- reference/10
      }
      
    }
  }
  reference <- ifelse(length(reference)==0,NA,reference)
  
  list.tables <- html_table(read_html(url.path), fill=TRUE)
  infos.search.good.table <- as.vector(unlist(lapply(lapply(list.tables,colnames),getGoodTableTurfoo)))
  index.horses.infos.table <- which.max(infos.search.good.table)
  mat.infos.horses <- list.tables[[index.horses.infos.table]] 
  mat.infos.horses <- mat.infos.horses[mat.infos.horses[,1]!='',]
  mat.infos.horses <- mat.infos.horses[!is.na(mat.infos.horses[,1]),]
  colnames(mat.infos.horses)[colnames(mat.infos.horses) == "Réd. km"] ="Reduction"
  
  if(nrow(mat.infos.horses)>0)
  {
    id.cotes <- c(grep("([0-9]+)([h])" ,colnames(mat.infos.horses)),grep("Arriv" ,colnames(mat.infos.horses)),grep("Cot" ,colnames(mat.infos.horses)),which(colnames(mat.infos.horses)==""))
    id.cotes <- sort(unique(id.cotes))
    colnames(mat.infos.horses)[id.cotes] <- paste('Cote',1:length(id.cotes),sep='-')
    for(col in id.cotes)
    {
      mat.infos.horses[,col] <- as.numeric(as.vector(mat.infos.horses[,col]))
    }
    
    if("Poids" %in% colnames(mat.infos.horses))
    {
      mat.infos.horses$Poids <- as.numeric(as.vector(mat.infos.horses$Poids))
    }
    
    if("Distance" %in% colnames(mat.infos.horses))
    {
      mat.infos.horses$Distance <- as.numeric(as.vector(gsub('m','',mat.infos.horses$Distance)))
    } else {
      mat.infos.horses <- transform(mat.infos.horses,Distance=distance)
    }
    
    if(!is.na(corde))
    {
      mat.infos.horses$Corde <- corde
    }
    
    mat.infos.horses <- transform(mat.infos.horses,Reference=reference)
    mat.infos.horses <- transform(mat.infos.horses,Discipline=discipline)
    mat.infos.horses <- transform(mat.infos.horses,Course=course)
    mat.infos.horses <- transform(mat.infos.horses,Prix=allocation)
    
    mat.infos.horses <- transform(mat.infos.horses,Date=date)
    mat.infos.horses <- transform(mat.infos.horses,Heure=heure.depart)
    
    mat.infos.horses <- transform(mat.infos.horses,Terrain=terrain)
    mat.infos.horses <- transform(mat.infos.horses,Lieu=lieu)
    mat.infos.horses <- transform(mat.infos.horses,Conditions=conditions.participation)
    mat.infos.horses <- transform(mat.infos.horses,Race=paste(mat.infos.horses$Date,mat.infos.horses$Heure,mat.infos.horses$Lieu,mat.infos.horses$Course,sep='-'))
    
    
    mat.infos.horses$Numero =NA
    colnames(mat.infos.horses) <- gsub("\\.",'-',colnames(mat.infos.horses))
    colnames(mat.infos.horses) <- gsub('N°','Numero',colnames(mat.infos.horses))
    colnames(mat.infos.horses) <- gsub('N-','Numero',colnames(mat.infos.horses))
    colnames(mat.infos.horses) <- gsub("NÂ-",'Numero',colnames(mat.infos.horses))
    mat.infos.horses$Numero <- as.numeric(as.vector(mat.infos.horses$Numero))
    mat.infos.horses <- transform(mat.infos.horses,Allocation=do.call(paste,as.list(allocations)))
    
  }
  return(mat.infos.horses)
}