#' Extract date from one deatailed page of the site http://www.medicaldevices-business-review.com
#' @param  url.path URL of the page to scrape
#'
#' @return dates vector of extracted dates
#'
#' @export
#' @importFrom xml2 read_html
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#'
#' @examples
#' getDateMedicalDevice('http://cardiovasculardevices.medicaldevices-business-review.com/news/boston-scientifics-blazer-oi-catheter-secures-fda-approval-110316-4836201')
#' @export

getDateMedicalDevice <- function(url.path)
{
  page <- read_html(url.path)
  xpath.date <- "//*[@class=\"type_news\"]"
  dates <- str_trim(page %>% html_nodes(xpath=xpath.date) %>% html_text())
  dates <- gsub('Published ','',dates)
  return(dates)
}




