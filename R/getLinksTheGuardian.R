#' @return mat.offres a matrix with news
#' @export
#' @importFrom xml2 read_html
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' getLinksTheGuardian (url.path='https://www.infoworld.com/news/?start=1')
#' @export

getLinksTheGuardian <- function(url.path='https://www.theguardian.com/business/fooddrinks?page=2')
{
  page <- read_html(url.path)
  xpath.title <- "//*[@class=\"fc-item__content\"]"
  links <- page %>%
  html_nodes(xpath=xpath.title) %>%
  html_nodes('a') %>%
  html_attr("href") %>%
  unique()
  return(links)
}
