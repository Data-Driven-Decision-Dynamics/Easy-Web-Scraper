#' @return Provides KPI for a given race
#' @export
#' @importFrom read_csv readr
#' @importFrom group_by dplyr
#' @importFrom summarize dplyr
#' @importFrom xml2 read_html
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_horse_score_racing_turfoo("http://www.turfoo.fr/programmes-courses/180615/reunion1-vincennes/course1-prix-eudora/")
#' @export
get_ranking_speed_geny <- function (url_target_race_geny   = "http://www.geny.com/partants-pmu/2018-11-10-lyon-la-soie-pmu-prix-ourasi_c1021988",
                                    path_input_speed_ranking_data = "/userhomes/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_historical_races.rds")
{
  source("/userhomes/mpaye/Easy-Web-Scraper/R/get_race_current_date_geny.R")
  source("/userhomes/mpaye/Easy-Web-Scraper/R/get_details_race_geny.R")
  source("/userhomes/mpaye/Easy-Web-Scraper/R/get_table_details_geny.R")
  
  mat_infos_races_current_date <- get_details_race_geny(path_url_current_race)
  vec_horses_current_race <- as.vector(mat_infos_races_current_date$Cheval)
  current_race_discipline <- as.vector(mat_infos_races_current_date$Discipline)[1]
  
  if(!exists("mat_historical_races")) {
    mat_historical_races = readRDS(path_mat_historical_races)
    mat_historical_races$Date <- as.vector(mat_historical_races$Date)
    mat_historical_races$Cheval <- as.vector(mat_historical_races$Cheval)
    mat_historical_races$Race <- as.vector(mat_historical_races$Race)
    mat_historical_races <- mat_historical_races[,-grep("Numero",colnames(mat_historical_races))[2]]
    colnames(mat_historical_races)[grep("Numero",colnames(mat_historical_races))] ="Numero"
  }
  
  mat_historical_races_focus <- subset(mat_historical_races,Discipline %in% current_race_discipline & Cheval %in% vec_horses_current_race)
  mat_historical_races_focus <- unique(mat_historical_races_focus)

  mat_profile_combined <- NULL
  for(horse in vec_horses_current_race)
  {
    mat_profile_current <- NULL
    mat_hist_horse <- subset(mat_historical_races_focus,Cheval==horse)
    
    if(current_race_discipline %in% c("Trot Monté","Trot Attelé") ) {
      mat_hist_horse <- mat_hist_horse[!is.na(mat_hist_horse[,'Reduction']),]
    } 
    
    if(nrow(mat_hist_horse)>0) {
      for(row in 1:nrow(mat_hist_horse) )
      {
        row_poids_current <- NULL
        if(current_race_discipline %in% c("Plat","Steeple","Haies"))
        {
          row_poids_current <- as.vector(mat_hist_horse[row,"Poids"])
        }
        row_race_current <- as.vector(mat_hist_horse[row,"Race"])
        perf_winner_current <- subset(mat_historical_races,Race==row_race_current)[,"Reduction"]
        perf_winner_current <- min(perf_winner_current[!is.na(perf_winner_current)])
        row_lieu_current <- as.vector(mat_hist_horse[row,"Lieu"])
        row_distance_current <- as.vector(mat_hist_horse[row,"Distance"])
        row_date_current <- as.Date(as.vector(mat_hist_horse[row,"Date"]))
        mat_records <- mat_historical_races %>% filter(Discipline==current_race_discipline,Distance==row_distance_current) %>% top_n(-1,Reduction)
        perf_record_global <- mat_records[1,'Reduction']
        mat_records_tracks <- mat_historical_races %>% filter(Discipline==current_race_discipline, Lieu==row_lieu_current, Distance==row_distance_current) %>% top_n(-1,Reduction)
        perf_current <- as.vector(mat_hist_horse[row,"Reduction"])
        perf_record <- mat_records_tracks[1,'Reduction']
        
        delta_perf_current <- (perf_current-perf_record)/perf_record
        delta_perf_current <- delta_perf_current*100
        delta_perf_current <- 100-delta_perf_current
        
        delta_best_perf_current_best_tract <- (perf_winner_current-perf_record)/perf_record
        delta_best_perf_current_best_tract <- delta_best_perf_current_best_tract*100
        delta_best_perf_current_best_tract <- 100-delta_best_perf_current_best_tract
        
        delta_perf_current_record <- (perf_current-perf_record_global)/perf_record_global
        delta_perf_current_record <- delta_perf_current_record*100
        delta_perf_current_record <- 100-delta_perf_current_record
        
        delta_best_perf_current_record <- (perf_winner_current-perf_record_global)/perf_record_global
        delta_best_perf_current_record <- delta_best_perf_current_record*100
        delta_best_perf_current_record <- 100-delta_best_perf_current_record
        
        delta_perf_tract_record <- (perf_record-perf_record_global)/perf_record_global
        delta_perf_tract_record <- delta_perf_tract_record*100
        delta_perf_tract_record <- 100-delta_perf_tract_record
        
        mat_profile_current.row <- data.frame(Cheval=horse,Distance=row_distance_current,Perf_Candidat=perf_current,Perf_Winner=perf_winner_current,Perf_Tract=perf_record,Perf_Record=perf_record_global,Score_Candidat_Tract=delta_perf_current,Score_Winner_Tract=delta_best_perf_current_best_tract,Score_Candidat_Record=delta_perf_current_record,Score_Winner_Record=delta_best_perf_current_record,Score_Tract_Record=delta_perf_tract_record,Date=row_date_current,Hyppodrome=row_lieu_current)
        mat_profile_current <- rbind (mat_profile_current.row,mat_profile_current)
        mat_profile_current <- mat_profile_current[order(mat_profile_current$Date),]
      }
      mat_profile_combined <- rbind(mat_profile_combined ,mat_profile_current)
    }
  }

  res_kmeans_distances_horses <- kmeans(sort(unique(mat_profile_combined$Distance)),4)
  mat_cluster_horses_distances <- data.frame(Distance=sort(unique(mat_profile_combined$Distance)),Cluster=res_kmeans_distances_horses$cluster)
  mat_cluster_horses_distances <- mat_cluster_horses_distances[order(mat_cluster_horses_distances$Distance),]
  vec_id_cluster_distances <- unique(mat_cluster_horses_distances$Cluster)
  mat_cluster_horses_distances <- as.data.frame(mat_cluster_horses_distances)
  mat_cluster_horses_distances$Group <- NULL
  for(id_cluster in vec_id_cluster_distances)
  {
    id_group <- paste(range(mat_cluster_horses_distances[mat_cluster_horses_distances$Cluster==id_cluster,"Distance"]),collapse ="_")
    mat_cluster_horses_distances$Group[mat_cluster_horses_distances$Cluster==id_cluster] <- id_group
  }
  
  mat_cluster_horses_distances <- mat_cluster_horses_distances[,c("Distance","Group")]
  
  mat_profile_combined <- mat_profile_combined[,c("Cheval","Distance","Score_Candidat_Record")]
  mat_profile_combined <- merge(mat_profile_combined,mat_cluster_horses_distances,by.x = "Distance",by.y="Distance")
  mat_profile_combined <- mat_profile_combined[,setdiff(colnames(mat_profile_combined),"Distance")]
  
  mat_profile_combined_melt <- melt(mat_profile_combined, id.vars=c("Cheval", "Group"), measure.vars='Score_Candidat_Record',na.rm=TRUE)
  mat_profile_combined_cast <- dcast(mat_profile_combined_melt, Cheval ~ Group,mean,na.rm=TRUE)
  mat_profile_combined_cast[is.na(mat_profile_combined_cast)] <- 0
  mat_profile_combined_cast <- replace(mat_profile_combined_cast, is.na(mat_profile_combined_cast), 0)
  mat_profile_combined <- mat_profile_combined_cast
  colnames(mat_profile_combined)[-1] <- paste("Speed",colnames(mat_profile_combined)[-1],sep="_")
  
  res_heatmap <- heatmap(as.matrix(mat_profile_combined[,-1]))
  mat_profile_combined <- mat_profile_combined[res_heatmap$rowInd,]
  
  return(mat_profile_combined)
  
}

