get_scores_defeated_candidates_race <- function (target_horse ="Clarck Sotho",
                                                 path_res_ranking = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed",
                                                 path_historical_races = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_historical_races.rds") {

  max_scores_defeated <- NA
  print("Loading historical performance matrix")
  if(!exists("mat_historical_races")) {
    mat_historical_races <- readRDS(path_historical_races)
    mat_historical_races$Cheval <- as.vector(mat_historical_races$Cheval)
    mat_historical_races$Race <- as.vector(mat_historical_races$Race)
    mat_historical_races$Date <- as.Date(mat_historical_races$Date)
    mat_historical_races$Prix <- as.numeric(mat_historical_races$Prix)
    mat_historical_races$Gains <- as.numeric(mat_historical_races$Gains)
  }
  
  mat_historical_races_focus <- mat_historical_races[mat_historical_races$Cheval %in% target_horse ,]
  mat_historical_races_focus <- mat_historical_races_focus[!is.na(mat_historical_races_focus$Place),]
  
  if(nrow(mat_historical_races_focus)>0){
    infos_last_race <- mat_historical_races_focus %>% top_n(1,Date)
    name_last_race <- as.data.frame(infos_last_race)[,"Race"]
    mat_infos_last_races <- mat_historical_races[mat_historical_races$Race %in% name_last_race & !is.na(mat_historical_races$Place),]
    if(nrow(mat_infos_last_races)>0){
      current_race_discipline <- as.vector(unique(mat_infos_last_races$Discipline))
      current_race_discipline <- tolower(current_race_discipline)
      current_race_discipline <- gsub("é","e",current_race_discipline)
      current_race_discipline <- tolower(gsub(" ","-",current_race_discipline))
      path_to_ranking_file <- grep(sub("-","_",current_race_discipline),grep("mat_ranking_season_temporal_glicko_global_",dir(path_res_ranking),value = TRUE),value=TRUE)
      file_to_read <- paste(path_res_ranking,path_to_ranking_file,sep="/")
      if(!file.exists(file_to_read)){
        stop("Input file not found")
      } else {
        mat_ranking <- readRDS(file_to_read)
      }
      place_target_horse <- mat_infos_last_races[mat_infos_last_races$Cheval==target_horse,"Place"]
      id_defeated_candidates <- mat_infos_last_races$Place>place_target_horse
      
      if(sum(id_defeated_candidates)>0) {
        vec_defeated_candidates <- mat_infos_last_races[id_defeated_candidates,"Cheval"]
      }
      
      vec_defeated_candidates <- intersect(vec_defeated_candidates,rownames(mat_ranking))
      if(length(vec_defeated_candidates)){
        max_scores_defeated <- max(mat_ranking [vec_defeated_candidates,ncol(mat_ranking)]) 
      }
    }
  }
  df_max_scores_defeated <- data.frame(Cheval=target_horse,Score=max_scores_defeated)
  return(df_max_scores_defeated) 
}  
