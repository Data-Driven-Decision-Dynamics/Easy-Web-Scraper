get_features_based_context_race <- function(mat_perf_race_target_horses=NULL,mat_infos_target_race_add=NULL,path_driver_score = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed") 
{

  current_race_date <- as.vector(unique(mat_infos_target_race_add$Date))
  
  if(class(current_race_date)!="Date") {
    current_race_date <- as.Date(current_race_date,origin="1970-01-01")
  }
  # extract distance of the target race
  current_race_distance <- min(mat_infos_target_race_add$Distance)
  # extract discipline of the target race
  current_race_discipline <- as.vector(unique(mat_infos_target_race_add$Discipline))
  # extract corde of the target race
  current_race_corde <- as.vector(unique(mat_infos_target_race_add$Corde))
  # extract depart of the target race
  current_race_depart <- as.vector(unique(mat_infos_target_race_add$Depart))
  # extract location of the target race
  current_race_location <- as.vector(unique(mat_infos_target_race_add$Lieu))
  # extract location of the target race
  current_race_cluster_distance <- as.vector(unique(mat_infos_target_race_add[mat_infos_target_race_add$Distance==names(table(mat_infos_target_race_add$Distance)[which.max(table(mat_infos_target_race_add$Distance))]),"Cluster_Distance"]))
  # extract horses of the target race
  
  current_race_horses <- as.vector(unique(mat_infos_target_race_add$Cheval))
  
  if(length(grep("Jockey",colnames(mat_infos_target_race_add)))>0) {
    colnames(mat_infos_target_race_add)[grep("Jockey",colnames(mat_infos_target_race_add))] <- "Driver"
  }
  
  mat_infos_target_race_add$Team <- paste(mat_infos_target_race_add$Cheval,mat_infos_target_race_add$Driver,sep="_")
  current_teams <- as.vector(unique(mat_infos_target_race_add$Team))
  
  mat_perf_race_target_horses$Team <- paste(mat_perf_race_target_horses$Cheval,mat_perf_race_target_horses$Driver,sep="_")
  
  if(current_race_discipline %in% c("Plat","Haies","Steeplechase","Cross","Trot Monté") & "Jockey.x" %in% colnames(mat_perf_race_target_horses)) {
    mat_perf_race_target_horses$Team <- paste(mat_perf_race_target_horses$Cheval,mat_perf_race_target_horses$Jockey.x,sep="_")
  }
  
  current_race_drivers <- as.vector(unique(mat_infos_target_race_add$Driver))

  # Initialization of the results
  mat_percent_podium_location <- NULL
  mat_percent_podium_depart <-NULL
  mat_percent_podium_corde <- NULL
  mat_percent_podium_ferrage <- NULL
  mat_percent_podium_recul <- NULL
  mat_mean_earning_horses_category <- NULL
  mat_percent_podium_team <- NULL
  mat_features_context <- NULL
  mat_infos_heaviest_weight <- NULL

  mat_num_races_class_gains_sup_equal <- NULL
  mat_num_days_last_class_sup_equal <- NULL
  mat_mean_dist_cluster_with_gain <- NULL
  mat_num_wins_class_sup_equal <- NULL

  mat_scale_numero <- mat_infos_target_race_add[,c("Cheval","Numero")]
  mat_scale_numero$"Numero" <- as.numeric(as.vector(mat_scale_numero$"Numero"))
  mat_scale_numero$"Numero" <- (mat_scale_numero$"Numero"-min(mat_scale_numero$"Numero"))/diff(range(mat_scale_numero$"Numero"))
  colnames(mat_scale_numero) <- gsub("Numero","CONTEXT_NUMBER_SCALE",colnames(mat_scale_numero))

  # Create indicator of success
  mat_perf_race_target_horses$Place  <- as.numeric(mat_perf_race_target_horses$Place)
  mat_perf_race_target_horses$Placer <- 0
  mat_perf_race_target_horses$Placer <- ifelse(mat_perf_race_target_horses$Place<6,1,0)
  
  mat_perf_race_target_horses$Date <- as.Date(mat_perf_race_target_horses$Date)

  # Calculate percent success of association horse and driver
  mat_percent_podium_team <- mat_perf_race_target_horses %>% 
    dplyr::filter(Team %in% current_teams) %>%
    group_by(Team) %>%
    dplyr::summarise(NUMBER_TEAMING = n(),
                     NUMBER_TEAMING_PODIUM=get_number_podium_geny(Place),
                     PERCENT_SUCCESS_TEAM=NUMBER_TEAMING_PODIUM/NUMBER_TEAMING) %>%
    select(Team,NUMBER_TEAMING,PERCENT_SUCCESS_TEAM) %>%
    as.data.frame()
  
  if(!is.null(mat_percent_podium_team)) {
    mat_percent_podium_team$Cheval <- unlist(lapply(mat_percent_podium_team$Team,function(x){unlist(strsplit(x,"_"))[1]}))
    mat_percent_podium_team <- mat_percent_podium_team[,rev(colnames(mat_percent_podium_team))]
    mat_percent_podium_team <- mat_percent_podium_team[,1:3]
  }

  
  mat_percent_podium_recul <- subset(mat_perf_race_target_horses,Recul>0) %>% 
    group_by(Cheval) %>%
    dplyr::summarise(PERCENT_SUCCESS_RECUL = sum(Placer)/n()) %>%
    as.data.frame() 
  
  # Calculate engagement for each horse
  mat_average_engagement <- mat_perf_race_target_horses %>% 
    dplyr::filter(Placer==1) %>%
    dplyr::filter(Cluster_Distance==current_race_cluster_distance) %>%
    group_by(Cheval) %>%
    dplyr::summarise(AVERAGE_LEVEL_ENGAGEMENT=mean(Prix,na.rm = TRUE)) %>%
    mutate(AVERAGE_LEVEL_ENGAGEMENT=AVERAGE_LEVEL_ENGAGEMENT/unique(mat_infos_target_race_add$Prix)) %>%
    mutate(AVERAGE_LEVEL_ENGAGEMENT=round(AVERAGE_LEVEL_ENGAGEMENT,2))
  mat_average_engagement <- merge(mat_average_engagement,mat_infos_target_race_add[,c('Cheval',"Numero")],by.x="Cheval",by.y="Cheval",all = TRUE)
  mat_average_engagement <- mat_average_engagement[,-ncol(mat_average_engagement)]
  mat_average_engagement$AVERAGE_LEVEL_ENGAGEMENT [is.na(mat_average_engagement$AVERAGE_LEVEL_ENGAGEMENT)] <- 0
    
  # Calculate percent success on current location
  mat_percent_podium_location <- mat_perf_race_target_horses %>% 
    group_by(Cheval,Lieu) %>%
    dplyr::filter(Lieu==current_race_location) %>%
    dplyr::summarise(NUMBER_RACES_HYPPODROME = n(),
                     DATE_LAST_RACE = max(Date),
                     DATE_CURRENT_RACE=as.Date(current_race_date),
                     DELAY_SINCE_LAST_RACE_HYPPODROME =as.numeric(as.Date(current_race_date)-DATE_LAST_RACE),
                     NUMBER_PODIUM_HYPPODROME=get_number_podium_geny(Place),
                     PERCENT_SUCCESS_HYPPODROME=NUMBER_PODIUM_HYPPODROME/NUMBER_RACES_HYPPODROME) %>%
    select(Cheval,NUMBER_RACES_HYPPODROME,PERCENT_SUCCESS_HYPPODROME,DELAY_SINCE_LAST_RACE_HYPPODROME)
    
  mat_percent_podium_location <- merge(mat_percent_podium_location,mat_infos_target_race_add[,c('Cheval',"Numero")],by.x="Cheval",by.y="Cheval",all = TRUE)
  mat_percent_podium_location <- mat_percent_podium_location[,-ncol(mat_percent_podium_location)]
  mat_percent_podium_location$PERCENT_SUCCESS_HYPPODROME <- round( mat_percent_podium_location$PERCENT_SUCCESS_HYPPODROME,2)

  # specific clenaing and computing for galop
  if(current_race_discipline %in% c("Plat","Haies","Steeplechase","Cross","Trot Monté")) {
    mat_perf_race_target_horses$"Déch."[mat_perf_race_target_horses$"Déch."=="-"] <-NA
    mat_perf_race_target_horses$"Déch." <- gsub(",",".",mat_perf_race_target_horses$"Déch.")
    mat_perf_race_target_horses$"Déch." <- as.numeric(mat_perf_race_target_horses$"Déch.")
      
    mat_perf_race_target_horses$"Valeur"[mat_perf_race_target_horses$"Valeur"=="-"] <-NA
    mat_perf_race_target_horses$"Valeur" <- gsub(",",".",mat_perf_race_target_horses$"Valeur")
    mat_perf_race_target_horses$"Valeur" <- as.numeric(mat_perf_race_target_horses$"Valeur")
      
    mat_perf_race_target_horses$"Poids"[mat_perf_race_target_horses$"Poids"=="-"] <-NA
    mat_perf_race_target_horses$"Poids" <- gsub(",",".",mat_perf_race_target_horses$"Poids")
    mat_perf_race_target_horses$"Poids" <- as.numeric(mat_perf_race_target_horses$"Poids")

    mat_infos_heaviest_weight <- do.call("rbind",lapply(split(subset(mat_perf_race_target_horses,Placer==1), subset(mat_perf_race_target_horses,Placer==1)$Cheval), function(x)
      x[which.max(x$Poids),]))
    mat_infos_heaviest_weight <- mat_infos_heaviest_weight[,c("Cheval","Poids","Prix","Gains","Date")]
    mat_infos_heaviest_weight <- transform(mat_infos_heaviest_weight,DELAY_SINCE_LAST_RACE_HEAVIEST_WEIGHT_PLACE=as.numeric(as.Date(current_race_date)-Date))
    colnames(mat_infos_heaviest_weight) <- sub("Poids","HEAVIEST_WEIGHT_PLACE",colnames(mat_infos_heaviest_weight))
    mat_infos_heaviest_weight <- merge(mat_infos_heaviest_weight,mat_infos_target_race_add[,c("Cheval","Poids")],by.x="Cheval",by.y="Cheval",all=TRUE)
    colnames(mat_infos_heaviest_weight) <- sub("Poids","WEIGHT_CURRENT",colnames(mat_infos_heaviest_weight))
    mat_infos_heaviest_weight <- transform(mat_infos_heaviest_weight,WEIGHT_ADVANTAGE_HEAVIEST_WEIGHT=HEAVIEST_WEIGHT_PLACE-WEIGHT_CURRENT)
    colnames(mat_infos_heaviest_weight) <- sub("Prix","PRIZE_HEAVIEST_WEIGHT_PLACE",colnames(mat_infos_heaviest_weight))
    mat_infos_heaviest_weight <- mat_infos_heaviest_weight[,c("Cheval","PRIZE_HEAVIEST_WEIGHT_PLACE","WEIGHT_ADVANTAGE_HEAVIEST_WEIGHT","DELAY_SINCE_LAST_RACE_HEAVIEST_WEIGHT_PLACE")]
  }
    
  if(current_race_discipline=="Trot Attelé")
  {
    mat_percent_podium_corde <- mat_perf_race_target_horses %>% 
      group_by(Cheval,Corde) %>%
      dplyr::filter(Corde==current_race_corde) %>%
      dplyr::summarise(NUMBER_RACES_CORDE = n(),
                       DATE_LAST_RACE = max(Date),
                       DELAY_SINCE_LAST_RACE_CORDE =as.numeric(as.Date(current_race_date)-DATE_LAST_RACE),
                       NUMBER_PODIUM_CORDE=get_number_podium_geny(Place),
                       PERCENT_SUCCESS_CORDE=NUMBER_PODIUM_CORDE/NUMBER_RACES_CORDE) %>%
      select(Cheval,NUMBER_RACES_CORDE,PERCENT_SUCCESS_CORDE,DELAY_SINCE_LAST_RACE_CORDE)

    mat_percent_podium_depart <- mat_perf_race_target_horses %>% 
      group_by(Cheval,Depart) %>%
      dplyr::filter(Depart==current_race_depart) %>%
      dplyr::summarise(NUMBER_RACES_DEPART = n(),
                       DATE_LAST_RACE = max(Date),
                       DELAY_SINCE_LAST_RACE_DEPART = as.numeric(as.Date(current_race_date)-DATE_LAST_RACE),
                       NUMBER_PODIUM_DEPART = get_number_podium_geny(Place),
                       PERCENT_SUCCESS_DEPART=NUMBER_PODIUM_DEPART/NUMBER_RACES_DEPART) %>%
      select(Cheval,NUMBER_RACES_DEPART,PERCENT_SUCCESS_DEPART,DELAY_SINCE_LAST_RACE_DEPART)
      
    mat_infos_percent_podium_ferrage <- mat_perf_race_target_horses %>% 
      group_by(Cheval,Ferrage) %>%
      dplyr::summarise(NUMBER_RACES_FERRAGE = n(),
                       DATE_LAST_RACE = max(Date),
                       DELAY_SINCE_LAST_RACE_FERRAGE = as.numeric(as.Date(current_race_date)-DATE_LAST_RACE),
                       NUMBER_PODIUM_FERRAGE = sum(Placer),
                       PERCENT_SUCCESS_FERRAGE=NUMBER_PODIUM_FERRAGE/NUMBER_RACES_FERRAGE) %>%
      as.data.frame()
    
    mat_percent_podium_ferrage <- NULL
    for(current_horse in current_race_horses)
    {
      val_ferrage_current_horse <- mat_infos_target_race_add[mat_infos_target_race_add$Cheval==current_horse,'Ferrage']
      mat_percent_podium_ferrage_current <- mat_infos_percent_podium_ferrage[mat_infos_percent_podium_ferrage$Cheval==current_horse & mat_infos_percent_podium_ferrage$Ferrage==val_ferrage_current_horse,]
      if(nrow(mat_percent_podium_ferrage_current)>0) {
        mat_percent_podium_ferrage <- rbind(mat_percent_podium_ferrage,mat_percent_podium_ferrage_current)
      }
    }
    mat_percent_podium_ferrage <- mat_percent_podium_ferrage[,-c(2,4,6)]  
    
    mat_percent_podium_recul <- subset(mat_perf_race_target_horses,Recul>0) %>% 
      group_by(Cheval) %>%
        dplyr::summarise(NUMBER_RACES_RECUL  = n(),
                         NUMBER_PODIUM_RECUL = sum(Placer),
                         DATE_LAST_RACE = max(Date),
                         DELAY_SINCE_LAST_RACE_RECUL = as.numeric(as.Date(current_race_date)-DATE_LAST_RACE),
                         PERCENT_SUCCESS_RECUL = NUMBER_PODIUM_RECUL/NUMBER_RACES_RECUL) %>%
        as.data.frame() 
    mat_percent_podium_recul[is.na(mat_percent_podium_recul)] <- 0
    mat_percent_podium_recul <- mat_percent_podium_recul[,-c(3,4)] 
    
    list_race_group_type <- lapply(as.vector(mat_perf_race_target_horses$Details),get_race_group_type_geny)
    mat_race_group_type <- bind_rows(list_race_group_type)
    mat_perf_race_target_horses <- cbind(mat_perf_race_target_horses,mat_race_group_type)
    vec_column_categories <- c("Cheval","Date","Valeur","Poids","Place","Gains","Group","Epreuve","Listed","Handicap","Reclamer","Maiden","Inedit","Prix")
    vec_column_categories <- intersect(vec_column_categories,colnames(mat_perf_race_target_horses))
    mat_perf_race_category_target_horses <- mat_perf_race_target_horses [,vec_column_categories]
    mat_perf_race_category_target_horses$Group <- as.vector(mat_perf_race_category_target_horses$Group)
    mat_perf_race_category_target_horses$Handicap <- as.vector(mat_perf_race_category_target_horses$Handicap)
    vec_idx_without_epreuve <- is.na(mat_perf_race_category_target_horses$Epreuve)
    vec_idx_with_group <- !is.na(mat_perf_race_category_target_horses$Group)
    
    if(sum(vec_idx_without_epreuve & vec_idx_with_group)>0) {
      mat_perf_race_category_target_horses[vec_idx_without_epreuve & vec_idx_with_group,"Epreuve"] <- mat_perf_race_category_target_horses[vec_idx_without_epreuve & vec_idx_with_group,"Group"]
    }
    
    vec_idx_without_epreuve <- is.na(mat_perf_race_category_target_horses$Epreuve)
    if(sum(vec_idx_without_epreuve)>0) {
      idx_without_epreuve <- which(vec_idx_without_epreuve==TRUE)
      for(id_without_epreuve in idx_without_epreuve)
      {
        closest_with_epreuve <- abs(mat_perf_race_category_target_horses$Prix-mat_perf_race_category_target_horses$Prix[id_without_epreuve])
        closest_with_epreuve [idx_without_epreuve] <- max(closest_with_epreuve)*100
        id_closest_epreuve <- which.min(closest_with_epreuve)
        mat_perf_race_category_target_horses[id_without_epreuve,"Epreuve"] <- mat_perf_race_category_target_horses[id_closest_epreuve,"Epreuve"]
      }
    }

      if(sum(!is.na(mat_perf_race_category_target_horses[,c("Group","Epreuve")]))>0) {
      mat_perf_race_category_target_horses_melt <- melt(mat_perf_race_category_target_horses, id=c("Cheval", "Epreuve"),measure.vars='Gains',na.rm=TRUE)
      mat_perf_race_category_target_horses_cast <- reshape2::acast(mat_perf_race_category_target_horses_melt, Cheval ~ Epreuve ~ variable,mean)
      mat_mean_earning_horses_category <- as.data.frame(mat_perf_race_category_target_horses_cast)
      mat_mean_earning_horses_category <- as.matrix(mat_mean_earning_horses_category)
      mat_mean_earning_horses_category[is.nan(mat_mean_earning_horses_category)] <- 0
      vec_start_columnames <- toupper(unlist(lapply(colnames(mat_mean_earning_horses_category) ,function(x){unlist(strsplit(x,"\\."))[2]})))
      vec_completion_columnames <- toupper(unlist(lapply(colnames(mat_mean_earning_horses_category) ,function(x){unlist(strsplit(x,"\\."))[1]})))
      colnames(mat_mean_earning_horses_category) <- paste(vec_start_columnames,"CATEGORY",vec_completion_columnames,sep="_")
      mat_mean_earning_horses_category <- mat_mean_earning_horses_category[,rev(colnames(mat_mean_earning_horses_category)[apply(mat_mean_earning_horses_category,2,max)>0])]
      mat_mean_earning_horses_category <- as.data.frame(mat_mean_earning_horses_category)
      mat_mean_earning_horses_category$Cheval <- as.vector(rownames(mat_mean_earning_horses_category))
      mat_mean_earning_horses_category <- mat_mean_earning_horses_category[,c("Cheval",setdiff(colnames(mat_mean_earning_horses_category),"Cheval"))]
      
      horse_without_category_data <- setdiff(current_race_horses,as.vector(unique(mat_mean_earning_horses_category$Cheval)))
      if(length(horse_without_category_data)>0) {
        mat_mean_earning_horses_category_comp <- mat_mean_earning_horses_category[1:length(horse_without_category_data),]
        mat_mean_earning_horses_category_comp$Cheval <- horse_without_category_data
        mat_mean_earning_horses_category_comp [,-1] <- 0
        mat_mean_earning_horses_category <- rbind(mat_mean_earning_horses_category,mat_mean_earning_horses_category_comp)
      }
      
      mat_mean_earning_horses_category[,-1][mat_mean_earning_horses_category[,-1]==0] <- NA
    }
  }

  list_features_context <- list(lieu=mat_percent_podium_location,depart=mat_percent_podium_depart,corde=mat_percent_podium_corde,ferrage=mat_percent_podium_ferrage,recul=mat_percent_podium_recul,numero=mat_scale_numero,engagement=mat_average_engagement,category=mat_mean_earning_horses_category,team=mat_percent_podium_team,weight=mat_infos_heaviest_weight)
  list_features_context <- list_features_context[lapply(list_features_context,length)>0]
      
  if(length(list_features_context)>0) {
    mat_features_context <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_features_context)
  }
      
  # mat_features_context[is.na(mat_features_context)] <- 0
  
  vec_files_driver_score <- dir(path_driver_score,"mat_ranking_driver")
  vec_files_driver_score_current_discipline  <- grep(tolower(gsub(" ","-",gsub("é","e",current_race_discipline))),vec_files_driver_score,value = TRUE)
  path_files_driver_score_current_discipline <- paste(path_driver_score,vec_files_driver_score_current_discipline,sep="/")
  mat_score_drivers <- readRDS(path_files_driver_score_current_discipline)
  colnames(mat_score_drivers) [colnames(mat_score_drivers)=="Cheval"] <- "Driver"
  mat_score_drivers_current <- subset(mat_score_drivers,Driver %in% current_race_drivers)
  mat_infos_target_race_add <- mat_infos_target_race_add[,intersect(colnames(mat_infos_target_race_add),c("Numero","Cheval","Driver","Jockey"))]
  colnames(mat_infos_target_race_add) <- gsub("Jockey","Driver",colnames(mat_infos_target_race_add))
  mat_score_drivers_current <- merge(mat_infos_target_race_add[,intersect(colnames(mat_infos_target_race_add),c("Numero","Cheval","Driver","Jockey"))],mat_score_drivers_current[,c("Driver","Glicko","Games","Success")],by.x="Driver",by.y="Driver",all = TRUE)
  mat_score_drivers_current <- mat_score_drivers_current[,c(c("Numero","Cheval","Driver"),setdiff(colnames(mat_score_drivers_current),c("Numero","Cheval","Driver")))]
  colnames(mat_score_drivers_current)[-(1:3)] <- toupper(paste("DRIVER",colnames(mat_score_drivers_current)[-(1:3)],sep="_"))
  mat_features_context <- merge(mat_features_context,mat_score_drivers_current,by.x="Cheval",by.y="Cheval",all=TRUE)
  
  if(length(grep("DELAY_SINCE",colnames(mat_features_context)))>0) {
    mat_features_context[grep("DELAY_SINCE",colnames(mat_features_context))][mat_features_context[grep("DELAY_SINCE",colnames(mat_features_context))]==0] <- NA
  }
  
  mat_pairs_confrontation <-matrix(0,ncol=length(current_race_horses),nrow=length(current_race_horses))
  colnames(mat_pairs_confrontation) <- current_race_horses
  rownames(mat_pairs_confrontation) <- current_race_horses
  vec_races_found <- unique(mat_perf_race_target_horses$Race)
  for(current_race in vec_races_found)
  {
    mat_perf_race_target_horses_tmp <- subset(mat_perf_race_target_horses,Race==current_race )
    if(nrow(mat_perf_race_target_horses_tmp)>1) {
      mat_perf_race_target_horses_tmp <- mat_perf_race_target_horses_tmp[order(mat_perf_race_target_horses_tmp$Place),]
      vec_horses_tmp <- as.vector(mat_perf_race_target_horses_tmp$Cheval)
      for(id_horse in 1:(length(vec_horses_tmp)-1))
      {
        for(id_horse_next in (id_horse+1):length(vec_horses_tmp) )
        {
          val_to_add <- 1
          test_if_advantage <- mat_perf_race_target_horses_tmp[id_horse,"Recul"]-mat_perf_race_target_horses_tmp[id_horse+1,"Recul"]
          if(test_if_advantage>0 & !is.na(test_if_advantage)) {
            val_to_add <- 2
          }
          
          if(test_if_advantage==0 & !is.na(test_if_advantage)) {
            val_to_add <- 1
          }
          
          if(test_if_advantage<0 & !is.na(test_if_advantage)) {
            val_to_add <- 0.5
          }
          mat_pairs_confrontation[vec_horses_tmp[id_horse],vec_horses_tmp[id_horse_next]] <- mat_pairs_confrontation[vec_horses_tmp[id_horse],vec_horses_tmp[id_horse_next]]+val_to_add
        }
      }
    }
  }

  mat_confrontation <- as.data.frame(sort(apply(mat_pairs_confrontation,1,sum),decreasing = TRUE))
  colnames(mat_confrontation) <- "GLOBAL_CONFRONTATION_ISSUE"
  mat_confrontation$Cheval <- rownames(mat_confrontation)
  mat_confrontation <- mat_confrontation[,rev(colnames(mat_confrontation))]
  mat_features_context <- merge(mat_features_context,mat_confrontation,by.x="Cheval",by.y="Cheval",all=TRUE)
  return(mat_features_context)
}








