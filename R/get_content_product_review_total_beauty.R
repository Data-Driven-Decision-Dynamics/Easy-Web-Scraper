#' @return mat.offres a matrix with innfos on offers
#' @export
#' @importFrom xml2 read_html
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_links_news_avionics_international("https://www.aviationtoday.com/news-archive/page/2/")
#' @export

get_content_product_review_total_beauty <- function( url_path_content_review ='https://www.totalbeauty.com/reviews/product/6423702/arbonne-makeup-primer/reviews-p2')
{
  infos_product_reviews <- NULL

  page_content_review <- tryCatch(read_html(url_path_content_review),error=function(e){cat("ERROR :",conditionMessage(e), "\n")})

  if(!is.null(page_content_review)){

    xpath_member_name  <- as.character("//*[@class=\"memberReview\"]")
    xpath_product_name  <- as.character("//*[@id=\"breadcrumbTitle\"]")
    xpath_rating_review  <- as.character("//*[@class=\"ratingStarSmall\"]")
    xpath_user_review  <- as.character("//*[@class=\"userReview\"]")
    xpath_review_title <- as.character("//*[@class=\"reviewTitle\"]")
    xpath_review_text <- as.character("//*[@class=\"reviewText\"]")

    product_names <- page_content_review %>%
      html_nodes("div") %>%
      html_nodes(xpath=xpath_product_name) %>%
      html_text()
    product_names <- str_trim(gsub("\n","",product_names))
    product_names <- str_trim(gsub("Reviews /","",product_names))

    titles_reviews <- page_content_review %>%
      html_nodes(xpath=xpath_user_review) %>%
      html_nodes("p") %>%
      html_nodes(xpath=xpath_review_title) %>%
      html_text()

    rating_reviews <- page_content_review %>%
      html_nodes("li") %>%
      html_nodes(xpath=xpath_member_name) %>%
      html_nodes(xpath=xpath_rating_review) %>%
      html_text() %>%
      as.numeric()
    rating_reviews <- rating_reviews[1:length(titles_reviews)]

    content_reviews <- page_content_review %>%
      html_nodes("div") %>%
      html_nodes(xpath=xpath_review_text) %>%
      html_text()
    content_reviews <- str_trim(gsub("\n","",content_reviews))

    infos_product_reviews <- data.frame(Source = "Total Beauty", Product = product_names , Title = titles_reviews , Rating = rating_reviews , Content = content_reviews )

  }

  return(infos_product_reviews)
}
