get_bounding_box_speed_rank_distance_geny <- function(url_target_race = "http://www.geny.com/partants-pmu/2019-07-12-cabourg-pmu-prix-du-sap_c1074875",
                                                      path_cluster_distance_range = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/input/mat_ranges_cluster_distance.rds",
                                                      path_historical_races = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_historical_races.rds",
                                                      save_graph = FALSE) {

  if(!exists("mat_historical_races")) {
    mat_historical_races <- readRDS(path_historical_races)
    vec_date_rebuilt <- unlist(lapply(mat_historical_races$Race,function(x){return(substr(x,1,10))}))
    mat_historical_races$Date <- vec_date_rebuilt
    mat_historical_races$Date <- as.Date(mat_historical_races$Date)
    mat_historical_races$Cheval <- as.vector(mat_historical_races$Cheval)
    mat_historical_races$Race <- as.vector(mat_historical_races$Race)
    mat_historical_races$Prix <- as.numeric(mat_historical_races$Prix)
    mat_historical_races$Gains <- as.numeric(mat_historical_races$Gains)
  }
  
  mat_infos_target_race <- get_details_race_geny(url_path_race_details = url_target_race)
  vec_horses <- unique(mat_infos_target_race$Cheval)
  
  mat_cluster_distance_range <- readRDS(path_cluster_distance_range)
  mat_cluster_distance_range_discipline <- mat_cluster_distance_range[mat_cluster_distance_range$Discipline==unique(mat_infos_target_race$Discipline),]
  
  infos_candidate_races <- get_historical_performance_horse_race_geny(url_target_race,number_days_back_in_time=55000)
  mat_historical_races_focus <- infos_candidate_races$mat_perf_race_target_horses
  
  if(unique(mat_infos_target_race$Discipline) %in% c("Plat")) {
    vec_availables_races <- unique(mat_historical_races_focus$Race)
    mat_historical_races_completed <- mat_historical_races[mat_historical_races$Race %in%vec_availables_races, ]
    list_historical_races_completed <- split.data.frame (mat_historical_races_completed,mat_historical_races_completed$Race)
    list_historical_races_focus <- lapply(list_historical_races_completed,get_reduction_based_ecart_geny)
    mat_historical_races_focus <- bind_rows (list_historical_races_focus)
    mat_historical_races_focus <- mat_historical_races_focus[mat_historical_races_focus$Cheval %in% vec_horses,]
  }
  
  mat_historical_races_focus <- mat_historical_races_focus[!is.na(mat_historical_races_focus$Reduction),]
  
  mat_historical_races_focus$Speed <- as.numeric(mat_historical_races_focus$Distance)/(mat_historical_races_focus$Reduction* (as.numeric(mat_historical_races_focus$Distance)/1000))
  mat_historical_races_focus$Rank <- 1:nrow(mat_historical_races_focus)
  mat_historical_races_focus$Date <- unlist(lapply(mat_historical_races_focus$Race,function(x){substr(x,1,10)}))
  mat_historical_races_focus$Date <- as.Date(mat_historical_races_focus$Date)
  mat_historical_races_focus <- mat_historical_races_focus[nchar(as.character(mat_historical_races_focus$Date))==10,]
  mat_historical_races_focus$Date <- as.Date(mat_historical_races_focus$Date)
  mat_historical_races_focus$Delay <- as.numeric(Sys.Date()- mat_historical_races_focus$Date)
  
  vec_speed_values <- mat_historical_races_focus$Speed
  vec_speed_values <- vec_speed_values[!is.na(vec_speed_values)]
  vec_speed_color <- colorRampPalette(brewer.pal(9,"RdYlBu"))(length(vec_speed_values))[rank(vec_speed_values)]
  names(vec_speed_color) <- vec_speed_values
  
  vec_delay_values <- mat_historical_races_focus$Delay
  vec_delay_values <- vec_delay_values[!is.na(vec_delay_values)]
  vec_delay_color <- colorRampPalette(rev(brewer.pal(9,"RdYlBu")))(length(vec_delay_values))[rank(vec_delay_values)]
  names(vec_delay_color) <- vec_delay_values
  
  distance_ref <-  as.numeric(max(mat_infos_target_race[,"Distance"]))

  mat_record_distance <- mat_historical_races_focus %>% group_by(Distance) %>% 
    dplyr::summarise(Worst=min(Speed),
                     Record=max(Speed)) %>%  
    as.data.frame()
  
  file_name_output <- url_target_race
  file_name_output <- gsub("http://www.geny.com/partants-pmu/","",file_name_output)
  file_name_output <- gsub("é","e",file_name_output)
  file_name_output <- gsub("è","e",file_name_output)
  file_name_output <- gsub(" ","-",file_name_output)
  file_name_output <- gsub("\\(","",file_name_output)
  file_name_output <- gsub("\\)","",file_name_output)
  file_name_output <- paste("bounding-box-",file_name_output,sep="")
  file_name_output <- paste(file_name_output,"tiff",sep=".")
  
  
  tiff(file_name_output, width = 750, height = 500)
  par(mar=c(4,3,2,2))
  if(length(vec_horses)==6)
  {
    vec.mfrow = c(2,3)
  } else if (length(vec_horses)==8) {
    vec.mfrow = c(2,4)
  } else if(length(vec_horses)==9) {
    vec.mfrow = c(3,3)
  } else if (length(vec_horses)==10) {
    vec.mfrow = c(2,5)
  } else if (length(vec_horses)==12) {
    vec.mfrow = c(3,4)
  } else if (length(vec_horses)==16) {
    vec.mfrow = c(4,4)
  } else if (length(vec_horses)==17) {
    vec.mfrow = c(3,6)
  } else if (length(vec_horses)==18) {
    vec.mfrow = c(3,6)
  } else if (length(vec_horses)==19) {
    vec.mfrow = c(4,5)
  } else if (length(vec_horses)==20) {
    vec.mfrow = c(4,5)
  } else if (length(vec_horses)==13) {
    vec.mfrow = c(3,5)
  } else if (length(vec_horses)==14) {
    vec.mfrow = c(3,5)
  } else if (length(vec_horses)==15) {
    vec.mfrow = c(3,5)
  } else if (length(vec_horses)==11) {
    vec.mfrow = c(3,4)
  } else if (length(vec_horses)==4) {
    vec.mfrow = c(2,2)
  } else if (length(vec_horses)==5) {
    vec.mfrow = c(2,3)
  } else if(length(vec_horses)==7) {
    vec.mfrow = c(2,4)
  } else if(length(vec_horses)==3) {
    vec.mfrow = c(2,2)
  }
  
  par(mfrow=vec.mfrow)
  
  mat_delta_compile <- NULL
  for(horse in vec_horses)
  {
    mat_historical_races_focus_current <- mat_historical_races_focus[mat_historical_races_focus$Cheval %in% horse,]
    
    if(nrow(mat_historical_races_focus_current)>0) {
      
      mat_historical_races_focus_current <- mat_historical_races_focus_current[,c("Date","Distance","Speed","Delay","Place")]
      mat_delta_current <- merge(mat_historical_races_focus_current,mat_record_distance,by.x="Distance",by.y="Distance")
      mat_delta_current <- transform(mat_delta_current,Delta=(Speed/Record)*100)
      mat_delta_current <- transform(mat_delta_current,Cheval=horse)
      mat_delta_compile <- rbind(mat_delta_compile,mat_delta_current)
      
      if(save_graph){
        xx <- c(1:nrow(mat_record_distance),nrow(mat_record_distance):1)
        yy <- c(mat_record_distance$Record,rev(mat_record_distance$Worst))
        vec_lab_xaxis <- sort(unique(mat_record_distance$Distance))
        col_lab_xaxis <- rep("black",length(vec_lab_xaxis))
        col_lab_xaxis[vec_lab_xaxis==distance_ref] <- "red"
        
        title_plot <- paste(mat_infos_target_race[mat_infos_target_race$Cheval==horse,"Numero"],mat_infos_target_race[mat_infos_target_race$Cheval==horse,"Cheval"],sep="-")
        plot(0,0,xlim=c(0,length(vec_lab_xaxis)),ylim=range(yy),xaxt="n",type="n",xlab="",ylab="",main=title_plot)
        # rect(par("usr")[1],par("usr")[3],par("usr")[2],par("usr")[4],col = brewer.pal(11,"RdGy")[11])
        polygon(xx,yy,col="grey2",border="red",lty="dashed")
        # grid(10,10)
        axis(1, 1:length(vec_lab_xaxis),labels=FALSE)
        text(1:length(vec_lab_xaxis), par("usr")[3] - 0.2, labels = vec_lab_xaxis, srt = 90, pos = 1, xpd = TRUE,offset = 0,cex=0.75,col=col_lab_xaxis)
        if(nrow(mat_historical_races_focus_current)>0) {
          for (id_race  in 1:nrow(mat_historical_races_focus_current)) {
            current_x <- mat_historical_races_focus_current[id_race,"Distance"]
            current_y <- mat_historical_races_focus_current[id_race,"Speed"]
            current_delay <- mat_historical_races_focus_current[id_race,"Delay"]
            current_speed <- mat_historical_races_focus_current[id_race,"Speed"]
            current_place <- mat_historical_races_focus_current[id_race,"Place"]
            bd_color  <- unique(vec_delay_color[vec_delay_values==current_delay])
            bg_color  <- unique(vec_speed_color[vec_speed_values==current_speed])
            if(current_place<4) {
              points(grep(current_x,vec_lab_xaxis),current_y,pch=0,cex=2,col=bd_color,lwd=1)
            }
            points(grep(current_x,vec_lab_xaxis),current_y,pch=21,cex=1.3,col=bd_color,bg=bg_color,lwd=2)
            # points(grep(current_x,vec_lab_xaxis),current_y,pch=10,cex=1.5,col="orange")
          }
        }
      }
    }
  }
  dev.off()
  
  mat_delta_compile$Cluster_Distance <-NULL
  for(i in 1:nrow(mat_delta_compile))
  {
    idx_cluster_distance_group <- mat_cluster_distance_range_discipline$Lower <= unlist(mat_delta_compile[i,"Distance"]) & mat_cluster_distance_range_discipline$Upper >= unlist(mat_delta_compile[i,"Distance"])
    mat_delta_compile[i,"Cluster_Distance"] <- paste(mat_cluster_distance_range_discipline[idx_cluster_distance_group,c("Lower","Upper")],collapse = "-")
  }
  
  mat_score_global_delta_record <- mat_delta_compile %>% group_by(Cheval)%>% dplyr::summarise(Score=mean(Delta)) %>% arrange(Score) %>% as.data.frame()
  mat_delta_compile_melt <- melt(mat_delta_compile,id.vars=c("Cheval","Cluster_Distance"),measure.vars="Delta")
  mat_delta_compile_cast <- reshape::cast(mat_delta_compile_melt,Cheval ~ Cluster_Distance,mean)
  mat_score_delta_record <- as.data.frame(mat_delta_compile_cast)
  mat_score_delta_record <- merge(mat_score_global_delta_record,mat_score_delta_record,by.x="Cheval",by.y="Cheval",all=TRUE)
  mat_score_delta_record <- merge(mat_infos_target_race[,c("Numero","Cheval")],mat_score_delta_record,by.x="Cheval",by.y="Cheval",all=TRUE)
  mat_score_delta_record <- mat_score_delta_record[,c(c("Numero","Cheval"),setdiff(colnames(mat_score_delta_record),c("Numero","Cheval")))]
  colnames(mat_score_delta_record) <- gsub("Score","GLOBAL_SPEED",colnames(mat_score_delta_record))
  colnames(mat_score_delta_record)[4:ncol(mat_score_delta_record)] <- paste("SPEED",colnames(mat_score_delta_record)[4:ncol(mat_score_delta_record)],sep="_")
  return(mat_score_delta_record)
}






