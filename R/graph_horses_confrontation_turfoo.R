graph_horses_confrontation_turfoo <- function(mat_line_by_line=NULL,lab.cex=0.75,vert.size=12)
{

  df_edges_all <- NULL
  vec_winners <- as.vector(unique(mat_line_by_line$Winner))
  for(horse in vec_winners)
  {
    mat_line_by_line_horse <-subset(mat_line_by_line,Winner==horse)
    vec_looser_current <- as.vector(unique(mat_line_by_line_horse$Looser))
    df_edges <- NULL
    for(looser in vec_looser_current )
    {
      mat_line_by_line_horse_current <-subset(mat_line_by_line_horse,Looser==looser)
      mat_line_by_line_horse_current <- mat_line_by_line_horse_current[order(mat_line_by_line_horse_current$Date),]
      edge_label_current <- paste(paste(mat_line_by_line_horse_current$DistanceWinner/1000,mat_line_by_line_horse_current$DeltaPoids,sep="_"),collapse = "|")
      df_edges_current <- data.frame(from=horse,to=looser,label=edge_label_current)
      df_edges <- rbind(df_edges,df_edges_current)
    }
    df_edges_all <- rbind(df_edges_all,df_edges)
  }
  
  vector_nodes <- as.vector(unique(unlist(df_edges_all[,c('from','to')])))
  df_nodes_all <- data.frame (id=vector_nodes,label=vector_nodes)
  network_graph <- graph.edgelist(as.matrix(df_edges_all[,c("from","to")]),directed=TRUE)
  network_graph <- simplify(network_graph)
  
  V(network_graph)$label <-df_edges_nodes$label
  V(network_graph)$color <- "blue"
  V(network_graph)$label.cex=lab.cex
  V(network_graph)$frame.color <-'white'

  # betweenness_edge <- edge.betweenness(network_graph)
  # degree_edge <- degree(network_graph)
  # df_nodes_all <- transform(df_nodes_all,value=degree_edge)
  nodes_edges <- list(nodes=df_nodes_all,edges=df_edges_all)
  
  
  

  
  
  plot.igraph(network_graph,layout=layout.fruchterman.reingold(network_graph, niter=10000),edge.arrow.size=0.2)
  
  
  
  network.data[is.na(network.data)] <-0
  values.cels <- network.data[!is.na(network.data)]
  column.to   <- rep(colnames(network.data),nrow(network.data))
  column.from  <- NULL
  for(node in rownames(network.data))
  {
    column.from.current <- rep(node,ncol(network.data))
    column.from <-c(column.from,column.from.current)
  }
  df.edges <- data.frame(from=column.from,to=column.to,value=values.cels)
  df.edges <- df.edges[df.edges[,"value"]!=0,]
  df.edges[,'from'] <-as.vector(df.edges[,'from'])
  df.edges[,'to'] <-as.vector(df.edges[,'to'])
  df.edges <- df.edges[df.edges[,'from']!=df.edges[,'to'],]
  vector.nodes <- as.vector(unique(unlist(df.edges[,c('from','to')])))
  df.nodes <- data.frame (id=vector.nodes,label=vector.nodes)
  network.graph <- graph.edgelist(as.matrix(df.edges[,c("from","to")]),directed=FALSE)
  network.graph <- simplify(network.graph)
  fast.greedy.com.detect <- fastgreedy.community(network.graph)
  node.memberchip.class <- membership(fast.greedy.com.detect)
  node.memberchip.class <- node.memberchip.class[vector.nodes]
  df.nodes <- transform(df.nodes,group=node.memberchip.class)
  betweenness.edge <- edge.betweenness(network.graph)
  degree.edge <- degree(network.graph)
  df.nodes <- transform(df.nodes,value=degree.edge)
  nodes.edges <- list(nodes=df.nodes,edges=df.edges)
  return(nodes.edges)
}
