require(fastTextR)
require(wordVectors)

mat.historical.races.word.embedding = mat.historical.races

mat.historical.races.word.embedding$RACE_TYPE <- NA
mat.historical.races.word.embedding$RACE_TYPE[as.numeric(mat.historical.races.word.embedding$Distance)<=1200] <-'Sprint' 
mat.historical.races.word.embedding$RACE_TYPE[as.numeric(mat.historical.races.word.embedding$Distance)>1200 & as.numeric(mat.historical.races.word.embedding$Distance)<=1600 ] <-'Flyer'
mat.historical.races.word.embedding$RACE_TYPE[as.numeric(mat.historical.races.word.embedding$Distance)>1600 & as.numeric(mat.historical.races.word.embedding$Distance)<=2000 ] <-'Miler'
mat.historical.races.word.embedding$RACE_TYPE[as.numeric(mat.historical.races.word.embedding$Distance)>2000 & as.numeric(mat.historical.races.word.embedding$Distance)<=2400 ] <-'Classique'
mat.historical.races.word.embedding$RACE_TYPE[as.numeric(mat.historical.races.word.embedding$Distance)>2400 & as.numeric(mat.historical.races.word.embedding$Distance)<=2800 ] <-'Fond'
mat.historical.races.word.embedding$RACE_TYPE[as.numeric(mat.historical.races.word.embedding$Distance)>2800] <-'Marathon'

mat.historical.races.word.embedding$Lieu <- getTextWithoutSpecialChar(mat.historical.races.word.embedding$Lieu)

mat.historical.races.word.embedding$Discipline[grep("Trot Attel",mat.historical.races.word.embedding$Discipline)] = "Trot Attelé"
mat.historical.races.word.embedding$Discipline[grep("Trot Mont",mat.historical.races.word.embedding$Discipline)]  = "Trot Monté"
mat.historical.races.word.embedding$Discipline <- getTextWithoutSpecialChar(mat.historical.races.word.embedding$Discipline)

mat.historical.races.word.embedding$Terrain[is.na(mat.historical.races.word.embedding$Terrain)] = ""
mat.historical.races.word.embedding$Terrain[mat.historical.races.word.embedding$Terrain=="-"]   = ""
mat.historical.races.word.embedding$Terrain <- getTextWithoutSpecialChar(mat.historical.races.word.embedding$Terrain)

mat.historical.races.word.embedding$Cheval <- getTextWithoutSpecialChar(mat.historical.races.word.embedding$Cheval)
mat.historical.races.word.embedding$Driver <- getTextWithoutSpecialChar(mat.historical.races.word.embedding$Driver)
mat.historical.races.word.embedding$Jockey <- getTextWithoutSpecialChar(mat.historical.races.word.embedding$Jockey)
mat.historical.races.word.embedding$Entraineur <- getTextWithoutSpecialChar(mat.historical.races.word.embedding$Entraineur)

mat.historical.races.word.embedding$Prix[mat.historical.races.word.embedding$Prix==0] = NA
mat.historical.races.word.embedding$PrixClass <- NA
mat.historical.races.word.embedding$PrixClass[as.numeric(mat.historical.races.word.embedding$Prix)<=20000] <-'Cuivre' 
mat.historical.races.word.embedding$PrixClass[as.numeric(mat.historical.races.word.embedding$Prix)>20000 & as.numeric(mat.historical.races.word.embedding$Prix)<=30000 ]  <-'Bronze'
mat.historical.races.word.embedding$PrixClass[as.numeric(mat.historical.races.word.embedding$Prix)>30000 & as.numeric(mat.historical.races.word.embedding$Prix)<=50000 ]  <-'Argent'
mat.historical.races.word.embedding$PrixClass[as.numeric(mat.historical.races.word.embedding$Prix)>50000 & as.numeric(mat.historical.races.word.embedding$Prix)<=100000 ] <-'Or'
mat.historical.races.word.embedding$PrixClass[as.numeric(mat.historical.races.word.embedding$Prix)>100000] <-'Titane'

column.embedding <- c('Lieu','Course','Discipline','RACE_TYPE','PrixClass','Terrain','Cheval','Driver','Jockey','Entraineur')
mat.historical.races.word.embedding <- mat.historical.races.word.embedding[,column.embedding]
for(i in 1:ncol(mat.historical.races.word.embedding))
{
  mat.historical.races.word.embedding[,i] [is.na(mat.historical.races.word.embedding[,i])] =''
}

for(i in 1:ncol(mat.historical.races.word.embedding))
{
  mat.historical.races.word.embedding[,i] = tolower(mat.historical.races.word.embedding[,i])
  
}

for(i in 1:ncol(mat.historical.races.word.embedding))
{
  mat.historical.races.word.embedding[,i] = gsub(' &','',mat.historical.races.word.embedding[,i])
}

mat.historical.races.word.embedding$Cheval = gsub("\\$","",mat.historical.races.word.embedding$Cheval)
setwd('/home/mpaye/Personal/Futanke-Mbayar/output/w2c/')
for(i in 1:nrow(mat.historical.races.word.embedding))
{
  vec.text <- as.vector(unlist(mat.historical.races.word.embedding[i,]))
  vec.text <- paste(vec.text,collapse = " ")
  write(vec.text,paste(i,'.txt',sep=""))
}

setwd('/home/mpaye/Personal/Futanke-Mbayar/output')
prep_word2vec(origin="w2c",destination="futanke.txt",lowercase=T,bundle_ngrams=1)
model.futanke <- train_word2vec("futanke.txt","cookbook_vectors.bin",vectors=200,threads=4,window=25,iter=10)






