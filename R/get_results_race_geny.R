get_results_race_geny <- function (url_path_race_results ="http://www.geny.com/arrivee-et-rapports-pmu/2018-07-04-vichy-pmu-prix-jean-michel-bazire_c984641")
{
  candidate_table = NULL
  page_result <- read_html(curl(url_path_race_results, handle = curl::new_handle("useragent" = "Mozilla/5.0")))
  tables_page_results <- html_table(page_result, fill=TRUE)
  if(length(tables_page_results)>0) {
    res_candidate_table <- unlist(lapply(tables_page_results,get_table_results_geny))
    if(sum(res_candidate_table,na.rm = TRUE)>0) {
      candidate_table <- tables_page_results[[which(res_candidate_table==TRUE)[1]]]
      candidate_table <- candidate_table[candidate_table[,1]!="",]
      candidate_table <- candidate_table [,1:5]
      colnames(candidate_table) <- c("Place","Numero","Cheval","Jockey","Ecart")
      candidate_table <- candidate_table[-1,]
      vec_name_horses <- unlist(lapply(candidate_table[,'Cheval'],function(x){unlist(strsplit(x,"idsChevaux"))[1]}))
      vec_name_horses <- unlist(lapply(candidate_table[,'Cheval'],function(x){unlist(strsplit(x,"\n"))[1]}))
      candidate_table[,'Cheval'] <- vec_name_horses
      id_row_remove <- grep("Portent des oeill|DA|DP|D4",candidate_table$Place)
      if(length(id_row_remove)>0) {
        candidate_table <- candidate_table [-id_row_remove,]
      }
      
    }
  }
  candidate_table$Cheval <- sub('\\(.*', '', candidate_table$Cheval)
  candidate_table$Cheval <- sub('\r.*', '', candidate_table$Cheval)
  candidate_table$Cheval <- stringr::str_trim(candidate_table$Cheval)
  
  return(candidate_table) 
}
