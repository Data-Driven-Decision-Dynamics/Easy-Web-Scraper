getNumericReducKilomEcartLeTrot <- function(reduc.km="1'15\"9")
{
  if(sum(grep("'",reduc.km))<1)
  {
    res <- NA
  } else {
    min <-unlist(strsplit(reduc.km,"'"))[1]
    sec.tier <- sub(min,'',reduc.km)
    sec.tier <- unlist(strsplit(sec.tier,"'"))[2]
    sec.tier <- sub("\"","",sec.tier,fixed=TRUE)
    sec <- substr(sec.tier,1,2)
    tier <- sub(sec,'',sec.tier)
    tier <- substr(tier,1,nchar(tier))
    res <- as.numeric(min)*60 + as.numeric(sec)
    res <- as.numeric(paste(res,'.',tier,sep=''))
  }
  return(res)
}