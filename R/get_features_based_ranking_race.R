get_features_based_ranking_race <- function( mat_infos_target_race_add = NULL, path_res_ranking = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed") {
  
  # extract horses of the target race
  current_race_horses <- as.vector(unique(mat_infos_target_race_add$Cheval))
  # extract date of the target race
  current_race_date <- as.vector(mat_infos_target_race_add$Date)[1]
  current_race_date <- as.Date(as.vector(current_race_date),origin = "1970-01-01")
  # extract distance of the target race
  current_race_distance <- min(mat_infos_target_race_add$Distance)
  # extract discipline of the target race
  current_race_discipline <- as.vector(unique(mat_infos_target_race_add$Discipline))
  current_race_discipline <- tolower(current_race_discipline)
  current_race_discipline <- gsub("é","e",current_race_discipline)
  current_race_discipline <- tolower(gsub(" ","-",current_race_discipline))
  
  vec_files_res_ranking <- dir(path_res_ranking,pattern="mat_ranking_monthly")
  vec_files_res_ranking_current_discipline <- grep(sub("-","_",current_race_discipline),vec_files_res_ranking,value = TRUE)
  vec_files_res_ranking_current_discipline <- grep("mat_ranking_monthly_temporal_glicko_global_|mat_ranking_monthly_temporal_glicko_specific_cluster_distance|mat_ranking_monthly_temporal_glicko_specific_corde|mat_ranking_monthly_temporal_glicko_specific_depart_",vec_files_res_ranking_current_discipline,value=TRUE)
  
  lab_files_res_ranking_current_discipline <- gsub("mat_ranking_monthly_temporal_glicko_global_|mat_ranking_monthly_temporal_glicko_specific_cluster_distance|mat_ranking_monthly_temporal_glicko_specific_corde|mat_ranking_monthly_temporal_glicko_specific_depart_","",vec_files_res_ranking_current_discipline)
  lab_files_res_ranking_current_discipline <- gsub(sub("-","_",current_race_discipline),"",lab_files_res_ranking_current_discipline)
  lab_files_res_ranking_current_discipline <- gsub(".rds","",lab_files_res_ranking_current_discipline)
  lab_files_res_ranking_current_discipline <- gsub("_","",lab_files_res_ranking_current_discipline)
  lab_files_res_ranking_current_discipline[lab_files_res_ranking_current_discipline==""] <- "Global"
  
  list_res_rating_all_together <- vector("list",length(lab_files_res_ranking_current_discipline))
  names(list_res_rating_all_together) <- lab_files_res_ranking_current_discipline
  
  for(id_current_file_res_ranking in 1:length(lab_files_res_ranking_current_discipline))
  {
    mat_ranking_current_gather <- NULL
    mat_ranking_current <- readRDS(paste(path_res_ranking,vec_files_res_ranking_current_discipline[id_current_file_res_ranking],sep="/"))
    mat_infos_target_race_add_focus <- mat_infos_target_race_add[,intersect(c('Numero','Cheval'),colnames(mat_infos_target_race_add)),drop=FALSE]
    mat_infos_target_race_add_focus$ID <- gsub("'"," ",mat_infos_target_race_add_focus$Cheval)
    mat_ranking_current <- mat_ranking_current[mat_ranking_current$Cheval %in% intersect(mat_infos_target_race_add_focus$Cheval,mat_ranking_current$Cheval), ]
    val_last_date <- intersect(as.character(current_race_date),colnames(mat_ranking_current))
    if(length(val_last_date)==0) {
      val_last_date <- colnames(mat_ranking_current)[ncol(mat_ranking_current)]
    }
    if(length(val_last_date)==1) {
      mat_ranking_current <- mat_ranking_current[,c("Cheval",val_last_date),drop=FALSE]
      colnames(mat_ranking_current)[ncol(mat_ranking_current)] <- lab_files_res_ranking_current_discipline[id_current_file_res_ranking]
      mat_ranking_current <- as.data.frame(mat_ranking_current)
      mat_ranking_current$ID<- rownames(mat_ranking_current)
      mat_ranking_current <- merge(mat_ranking_current,mat_infos_target_race_add_focus,by.x="Cheval",by.y="Cheval")
      mat_ranking_current <- mat_ranking_current[,-grep("ID",colnames(mat_ranking_current))]
      mat_ranking_current_gather <- mat_ranking_current[,c(c("Cheval"),setdiff(colnames(mat_ranking_current),c("Numero","Cheval")))]
      list_res_rating_all_together[[id_current_file_res_ranking]] <- mat_ranking_current_gather
    }
  }
  
  if(length(list_res_rating_all_together)>0) {
    mat_res_rating_specific_compliled <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_res_rating_all_together)
  }
  
  vec_final_colums <- colnames(mat_res_rating_specific_compliled)[apply(mat_res_rating_specific_compliled,2,function(x) {return(sum(!is.na(x)))})>0]
  mat_res_rating_specific_compliled <- mat_res_rating_specific_compliled[,vec_final_colums]
  mat_res_rating_specific_compliled <- unique(mat_res_rating_specific_compliled)
  
  # if(length(grep("-",colnames(mat_res_rating_specific_compliled)))>0) {
  #   colnames(mat_res_rating_specific_compliled)[grep("-",colnames(mat_res_rating_specific_compliled))] <- paste("SPECIFIC_SCORE",colnames(mat_res_rating_specific_compliled)[grep("-",colnames(mat_res_rating_specific_compliled))],sep='_')
  # }

 return(mat_res_rating_specific_compliled)
}
