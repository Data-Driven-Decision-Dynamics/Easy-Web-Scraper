get_feature_importance_predictor_geny <- function(file_name_target="28_infos-races-2018-11-25-pmu-prix-gondolette_c1025266.csv",
                                                  path_output_indiv_data = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/model/")
{
  mat_feat_imp <- NULL
  mat_feat <- read.csv(paste(path_output_indiv_data,file_name_target,sep="/"))
  mat_feat <- transform(mat_feat,Placer=ifelse(Place<6,1,0))
  colnames(mat_feat) <- gsub("\\.","_",colnames(mat_feat))
  if(length(unique(mat_feat$Placer))>1 & nrow(mat_feat)>7){
    distance_current <- names(which.max(table(mat_feat$Distance))) 
    lieu_current <- as.vector(unique(mat_feat$Lieu)) 
    
    mat_feat$AGE_RECODE <- NA
    mat_feat$AGE_RECODE[mat_feat$Age %in% c(1:3)] <- "2_3"
    mat_feat$AGE_RECODE[mat_feat$Age %in% c(4,5)] <- "4_5"
    mat_feat$AGE_RECODE[mat_feat$Age %in% c(6,7)] <- "6_7"
    mat_feat$AGE_RECODE[mat_feat$Age %in% c(8,9)] <- "8_9"
    mat_feat$AGE_RECODE[mat_feat$Age %in% 10:20]   <- "+_10"
    mat_feat$Gender <- gsub('FALSE','F',mat_feat$Gender)
    mat_feat$Recul <- as.vector(mat_feat$Recul)
    
    vec_col_remove <- c("Race","X","Cheval","Numero","Ferrage","Discipline","Depart","Distance","Corde","Lieu","Age","Date","Prix","Corde","Place","Cluster_Distance")
    vec_col_recode <- c("AGE_RECODE","Gender","Recul")
    
    mat_feat[, vec_col_recode] <- lapply(mat_feat[,vec_col_recode ], as.factor)
    mat_feat_col_recode <- amap::matlogic(mat_feat[, vec_col_recode])
    colnames(mat_feat_col_recode) <- toupper(as.vector(gsub("\\.","_",colnames(mat_feat_col_recode))))
    mat_feat_without_to_recode <- mat_feat[,setdiff(colnames(mat_feat),vec_col_recode)]
    mat_feat_final_format <- cbind(mat_feat_without_to_recode,mat_feat_col_recode)
    rownames(mat_feat_final_format) <- as.vector(mat_feat_final_format$Cheval)
    mat_feat_final_format <- mat_feat_final_format[,setdiff(colnames(mat_feat_final_format),vec_col_remove)]
    mat_feat_final_format <- mat_feat_final_format[,c(setdiff(colnames(mat_feat_final_format),"Placer"),"Placer")]
    num_useful_columns <- colnames(mat_feat_final_format)[apply(mat_feat_final_format,2,function(x){sum(!is.na(x))})>=round(nrow(mat_feat_final_format)/2)]
    if(length(num_useful_columns>2)){
      mat_feat_final_format <- mat_feat_final_format[,num_useful_columns]
      mat_feat_imp <- t(caTools::colAUC(mat_feat_final_format[,-ncol(mat_feat_final_format)],mat_feat_final_format[,ncol(mat_feat_final_format)]))
      colnames(mat_feat_imp) <-"Score"
      mat_feat_imp <- as.data.frame(mat_feat_imp)
      mat_feat_imp$Feature <- rownames(mat_feat_imp)
      mat_feat_imp$Distance <- distance_current
      mat_feat_imp$Lieu <- lieu_current
      mat_feat_imp$RACE <- file_name_target
      mat_feat_imp <- mat_feat_imp[,c("Feature","Score","Distance","Lieu","RACE")]
    }
  }
  return(mat_feat_imp)
}
