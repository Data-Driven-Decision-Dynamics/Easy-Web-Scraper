#' Extract news from one page of the site http://www.teamstainless.org/news
#' @param  url.path URL of the page to scrape
#' @return news.infos a data frame with the following columns, Source, Date, Title, Summary, Content, Topic, Page, Identifier
#' @export
#' @importFrom xml2 read_html
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' getNewsContentTeamStainless('http://www.teamstainless.org/news/2013-03/From_dream_to_reality')
#' @export

getNewsContentTeamStainless <- function(url.path)
{
  page  <- read_html(url.path)
  xpath.summary <- "//*[@id=\"leftColumnContainer\"]/p/text()"
  summary <-str_trim(page %>% html_nodes(xpath=xpath.summary) %>% html_text())
  summary <- paste0(summary,collapse = '')
  summary <- gsub('For more information, contact the Nickel Institute ()','',summary)
  xpath.date <- "//*[@id=\"leftColumnContainer\"]/div"
  date <- str_trim(page %>% html_nodes(xpath=xpath.date) %>% html_text())
  date <- gsub("Published ",'',date)
  date <- substr(date,1,10)
  xpath.title <- "//*[@id=\"leftColumnContainer\"]/h2"
  title <-str_trim(page %>% html_nodes(xpath=xpath.title) %>% html_text())
  news.infos <- data.frame(Source='Team Stainless',Date=date,Title=title,Summary=summary,Page=url.path)
  news.infos <- transform (news.infos, Content = paste(news.infos[,'Title'],news.infos[,'Summary'],sep='.'))
  news.infos <- transform(news.infos,Topic='')
  news.infos <- news.infos[,c('Source','Date','Title','Summary','Content','Topic','Page')]
  news.infos <- transform(news.infos,Identifier=paste(news.infos[,'Page'],1:nrow(news.infos),sep='-'))
  return(news.infos)
}





