getDetailsOilGasJournal <- function(string)
{
  res <- unlist(strsplit(string,'\n'))
  res <- res [-2]
  title <- res [1]
  date <- res[2]
  content <- paste(res[3:length(res)],collapse =' ')
  mat.res <- data.frame(Source="Oil & Gas Journal",Date=date, Title=title,Summary=content)
  mat.res <- transform(mat.res, Content = paste(mat.res[,'Title'],mat.res[,'Summary'],sep="."))
  return(mat.res)
}
