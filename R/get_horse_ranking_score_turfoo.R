# path_line_by_line_processed = "/data/home/mpaye/Fass/line_by_line/output/processed/mat_line_by_line_rating.rds"
# url_path_new_race = "http://www.turfoo.fr/programmes-courses/180713/reunion3-saint-malo/course9-prix-de-cancale"
get_horse_ranking_score_turfoo <- function (path_line_by_line_processed = "/userhomes/mpaye/Futanke-Mbayar/01-db/tf/output/02-process/mat_line_by_line_rating_Horse.rds",
                                            url_path_new_race = "http://www.turfoo.fr/programmes-courses/180825/reunion1-deauville/course3-prix-de-la-reconversion-des-chevaux-de-courses---gd-handicap",
                                            path_mat_historical_races="/userhomes/mpaye/Futanke-Mbayar/01-db/tf/output/02-process/mat.historical.races.rds",
                                            date_min=as.Date("2017-01-01"))
{
  
  if(!exists("mat_historical_races")) {
    mat_historical_races = readRDS(path_mat_historical_races)
    mat_historical_races$Date <- as.vector(mat_historical_races$Date)
    mat_historical_races$Cheval <- as.vector(mat_historical_races$Cheval)
    mat_historical_races$Race <- as.vector(mat_historical_races$Race)
    
    mat_historical_races$Discipline[mat_historical_races$Discipline== "Trot AttelÃ©"] <- "Trot Attelé" 
    mat_historical_races$Discipline[mat_historical_races$Discipline== "Trot MontÃ©"]  <- "Trot Monté"
    
    mat_historical_races <- transform(mat_historical_races, IdentifierHorseRace=paste(mat_historical_races$Race,mat_historical_races$Cheval,sep="|"))
    vec.freq.horse.race  <- table(mat_historical_races$IdentifierHorseRace)
    mat_historical_races <- subset(mat_historical_races,IdentifierHorseRace %in% names(vec.freq.horse.race)[vec.freq.horse.race<2])
  }
  
  infos_horse_new_race <- getDetailsGivenRacingTurfoo(url.path = url_path_new_race)
  if('S.Age' %in% colnames(infos_horse_new_race)){
    infos_horse_new_race$Gender <- substr(infos_horse_new_race$`S.Age`,1,1)
    infos_horse_new_race$Age <- infos_horse_new_race$`S.Age`
    genders.races <- unique(infos_horse_new_race$Gender)
    for(gender in genders.races)
    {
      infos_horse_new_race$Age <- gsub(gender,'',infos_horse_new_race$Age)
    }
  }
  column.new.race.keep <- c("Numero","Cheval","Age","Gender","Distance","Driver","Jockey","Poids","Entraineur","Musique",grep("Cote",colnames(infos_horse_new_race),value=TRUE),"Musique","Reference" ,"Discipline","Course","Allocation","Date","Heure", "Terrain","Lieu","Conditions","Gender","Age")
  infos_horse_new_race <- infos_horse_new_race[,intersect(column.new.race.keep,colnames(infos_horse_new_race))]
  infos_horse_new_race <- infos_horse_new_race[,apply(infos_horse_new_race,2,function(x){sum(!is.na(x))})>0]
  vec_horses_race <- as.vector(infos_horse_new_race$Cheval)
  race_discipline <- unique(as.vector(infos_horse_new_race$Discipline))
  
  # mat_historical_races_focus <- subset(mat_historical_races,Discipline %in% race_discipline & Cheval %in% vec_horses_race & Date>=as.Date(date_min))
  # mat_historical_races_focus <- unique(mat_historical_races_focus)
  # mat_historical_races_focus <- mat_historical_races_focus[!is.na(mat_historical_races_focus$Reduction),] 
  # 
  # mat_historical_races_focus_melt <- melt(mat_historical_races_focus, id.vars=c("Cheval", "Distance"), measure.vars='Score_Candidat_Record',na.rm=TRUE)
  # mat_historical_races_focus_cast <- dcast(mat_historical_races_focus_melt, Cheval ~ Distance,max,na.rm=FALSE)
  # mat_historical_races_focus_cast[is.na(mat_historical_races_focus_cast)] <- 0
  # mat_historical_races_focus_cast <- replace(mat_historical_races_focus_cast, is.na(mat_historical_races_focus_cast), 0)
  
  mat_line_by_line <- readRDS(path_line_by_line_processed)
  mat_line_by_line <- as.data.frame(mat_line_by_line)
  mat_line_by_line <- subset(mat_line_by_line,Date>=date_min)
  
  vec_distance_useful <- unique(unlist(mat_line_by_line[mat_line_by_line[,"Winner"] %in% vec_horses_race | mat_line_by_line[,"Looser"] %in% vec_horses_race,c("Winner_Distance","Looser_Distance")]))
  res_kmeans_distances <- kmeans(vec_distance_useful,4)
  mat_cluster_distance <- data.frame(Distance=as.character(vec_distance_useful),Speciality=res_kmeans_distances$cluster)
  
  list_distances <- vector('list',length(unique(res_kmeans_distances$cluster)))
  names(list_distances) <- unique(res_kmeans_distances$cluster)
  list_res_rating_specific_compliled <- vector("list",length(sort(unique(res_kmeans_distances$cluster))))
  names(list_res_rating_specific_compliled) <- sort(unique(res_kmeans_distances$cluster))
  
  mat_line_by_line_rating <- subset(mat_line_by_line,mat_line_by_line[,"Winner_Distance"] %in% vec_distance_useful & mat_line_by_line[,"Looser_Distance"] %in% vec_distance_useful & Discipline %in% race_discipline)
  mat_line_by_line_rating <- subset(mat_line_by_line_rating, mat_line_by_line_rating[,"Winner"] %in% vec_horses_race | mat_line_by_line_rating[,"Looser"] %in% vec_horses_race)
  
  mat_line_by_line_rating$Date <- as.Date(mat_line_by_line_rating$Date)
  mat_line_by_line_rating  <- transform(mat_line_by_line_rating,Delay=Sys.Date()-Date)
  mat_line_by_line_rating <-  transform(mat_line_by_line_rating,WeekTempory=mat_line_by_line_rating$Delay/7)
  vec.values.times <- as.numeric(as.vector(mat_line_by_line_rating$WeekTempory))
  mat_line_by_line_rating$Week <- vec.values.times
  mat_line_by_line_rating <-  transform(mat_line_by_line_rating,Score=1)
  mat_line_by_line_rating  <- mat_line_by_line_rating[order(mat_line_by_line_rating$Date),]
  mat_line_by_line_rating  <- unique(mat_line_by_line_rating)
  
  mat_line_by_line_rating <- mat_line_by_line_rating [,c("Week","Winner","Looser","Score")]
  mat_line_by_line_rating <- mat_line_by_line_rating[complete.cases(mat_line_by_line_rating),]
  
  infos_res_glicko_golbal <-  glicko(mat_line_by_line_rating,history=TRUE)
  mat_res_glicko_golbal <- infos_res_glicko_golbal$ratings
  rm(infos_res_glicko_golbal)
  gc()
  mat_res_glicko_golbal <- mat_res_glicko_golbal[,c("Player","Rating")]
  colnames(mat_res_glicko_golbal) <- sub("Player","Cheval",colnames(mat_res_glicko_golbal))
  colnames(mat_res_glicko_golbal) <- sub("Rating","Glicko_Global",colnames(mat_res_glicko_golbal))
  mat_res_glicko_golbal <- merge(mat_res_glicko_golbal,infos_horse_new_race[,c('Cheval','Numero')],by.x="Cheval",by.y="Cheval",all.y = TRUE)
  mat_res_glicko_golbal <- mat_res_glicko_golbal[,-ncol(mat_res_glicko_golbal)]
  
  infos_res_steph_golbal <- steph(mat_line_by_line_rating,history=TRUE)
  mat_res_steph_golbal <- infos_res_steph_golbal$ratings
  rm(infos_res_steph_golbal )
  gc()
  mat_res_steph_golbal <- mat_res_steph_golbal[,c("Player","Rating")]
  colnames(mat_res_steph_golbal) <- sub("Player","Cheval",colnames(mat_res_steph_golbal))
  colnames(mat_res_steph_golbal) <- sub("Rating","Steph_Global",colnames(mat_res_steph_golbal))
  mat_res_steph_golbal <- merge(mat_res_steph_golbal,infos_horse_new_race[,c('Cheval','Numero')],by.x="Cheval",by.y="Cheval",all.y = TRUE)
  mat_res_steph_golbal <- mat_res_steph_golbal[,-ncol(mat_res_steph_golbal)]
  
  infos_res_elo_golbal <- elo(mat_line_by_line_rating,history=TRUE)
  mat_res_elo_golbal <- infos_res_elo_golbal$ratings
  rm(infos_res_elo_golbal)
  gc()
  mat_res_elo_golbal <- mat_res_elo_golbal[,c("Player","Rating")]
  colnames(mat_res_elo_golbal) <- sub("Player","Cheval",colnames(mat_res_elo_golbal))
  colnames(mat_res_elo_golbal) <- sub("Rating","Elo_Global",colnames(mat_res_elo_golbal))
  mat_res_elo_golbal <- merge(mat_res_elo_golbal,infos_horse_new_race[,c('Cheval','Numero')],by.x="Cheval",by.y="Cheval",all.y = TRUE)
  mat_res_elo_golbal <- mat_res_elo_golbal[,-ncol(mat_res_elo_golbal)]
  mat_res_rating_golbal <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list(mat_res_elo_golbal,mat_res_glicko_golbal,mat_res_steph_golbal))
  mat_res_rating_golbal <- transform(mat_res_rating_golbal,EGS_GLOBAL=(Elo_Global+Glicko_Global+Steph_Global)/3)
  mat_res_rating_golbal <- mat_res_rating_golbal [,c("Cheval","EGS_GLOBAL")]
  
  for (clus in sort(unique(res_kmeans_distances$cluster)))
  {
    ranges_distance <- sort(range(as.numeric(as.vector(subset(mat_cluster_distance,Speciality==clus)[,'Distance']))))
    list_distances [[clus]] <- ranges_distance
    id_distance_cluster <- mat_line_by_line[,'Winner_Distance']>=ranges_distance[1] & mat_line_by_line[,'Looser_Distance']>=ranges_distance[1] & mat_line_by_line[,'Winner_Distance']>=ranges_distance[2] & mat_line_by_line[,'Looser_Distance']>=ranges_distance[2] & mat_line_by_line[,'Discipline']==race_discipline
    mat_line_by_line_subset <- mat_line_by_line[id_distance_cluster,]
    
    mat_line_by_line_subset$Date <- as.Date(mat_line_by_line_subset$Date)
    mat_line_by_line_subset  <- transform(mat_line_by_line_subset,Delay=Sys.Date()-Date)
    mat_line_by_line_subset <-  transform(mat_line_by_line_subset,WeekTempory=mat_line_by_line_subset$Delay/7)
    vec.values.times <- as.numeric(as.vector(mat_line_by_line_subset$WeekTempory))
    mat_line_by_line_subset$Week <- vec.values.times
    mat_line_by_line_subset <-  transform(mat_line_by_line_subset,Score=1)
    mat_line_by_line_subset  <- mat_line_by_line_subset[order(mat_line_by_line_subset$Date),]
    mat_line_by_line_subset  <- unique(mat_line_by_line_subset)
    # 
    # mat_number_race_distance_range <- melt(mat_line_by_line_subset , id.vars=c("Cheval", "Race"), na.rm=TRUE)
    # mat_number_race_distance_range <- dcast(mat_number_race_distance_range, Cheval ~ Race,length,na.rm=TRUE)
    # rownames(mat_number_race_distance_range) <- as.vector(mat_number_race_distance_range[,1])
    # 
    # 

    if(length(mat_line_by_line_subset$Winner)>1 & length(mat_line_by_line_subset$Looser)>1 & length(unique(mat_line_by_line_subset$Week))>1)
    {
      mat_line_by_line_subset_rating <-  mat_line_by_line_subset[, c("Week","Winner","Looser","Score")]
      mat_line_by_line_subset_rating <- mat_line_by_line_subset_rating[!is.na(mat_line_by_line_subset_rating$Score),]
      mat_line_by_line_subset_rating <- mat_line_by_line_subset_rating[complete.cases(mat_line_by_line_subset_rating),]
      infos_res_glicko_specific <- glicko(mat_line_by_line_subset_rating,history=TRUE)
      # infos_res_glicko_specific <- glicko(mat.horses.direct.ranking.rating.subset,gamma=mat.horses.direct.ranking[id.distance.cluster,'gamma'],history=TRUE)
      mat_res_glicko_specific <- infos_res_glicko_specific$ratings
      rm(infos_res_glicko_specific)
      gc()
      mat_res_glicko_specific <- mat_res_glicko_specific[,c("Player","Rating")]
      colnames(mat_res_glicko_specific) <- sub("Player","Cheval",colnames(mat_res_glicko_specific))
      colnames(mat_res_glicko_specific) <- sub("Rating","Glicko",colnames(mat_res_glicko_specific))
      colnames(mat_res_glicko_specific) <- c(colnames(mat_res_glicko_specific)[1],paste(paste(ranges_distance,collapse ='-'),colnames(mat_res_glicko_specific)[2],sep="_"))
      mat_res_glicko_specific <- merge(mat_res_glicko_specific,infos_horse_new_race[,c('Cheval','Numero')],by.x="Cheval",by.y="Cheval",all.y = TRUE)
      mat_res_glicko_specific <- mat_res_glicko_specific[,-ncol(mat_res_glicko_specific)]
      
      infos_res_steph_specific <- steph(mat_line_by_line_subset_rating,history=TRUE)
      mat_res_steph_specific <- infos_res_steph_specific$ratings
      rm(infos_res_steph_specific)
      gc()
      mat_res_steph_specific <- mat_res_steph_specific[,c("Player","Rating")]
      colnames(mat_res_steph_specific) <- sub("Player","Cheval",colnames(mat_res_steph_specific))
      colnames(mat_res_steph_specific) <- sub("Rating","Steph",colnames(mat_res_steph_specific))
      colnames(mat_res_steph_specific) <- c(colnames(mat_res_steph_specific)[1],paste(paste(ranges_distance,collapse ='-'),colnames(mat_res_steph_specific)[2],sep="_"))
      mat_res_steph_specific <- merge(mat_res_steph_specific,infos_horse_new_race[,c('Cheval','Numero')],by.x="Cheval",by.y="Cheval",all.y = TRUE)
      mat_res_steph_specific <- mat_res_steph_specific[,-ncol(mat_res_steph_specific)]
      
      infos_res_elo_specific <- elo(mat_line_by_line_subset_rating,history=TRUE)
      mat_res_elo_specific <- infos_res_elo_specific$ratings
      rm(infos_res_elo_specific)
      gc()
      mat_res_elo_specific <- mat_res_elo_specific[,c("Player","Rating")]
      colnames(mat_res_elo_specific) <- sub("Player","Cheval",colnames(mat_res_elo_specific))
      colnames(mat_res_elo_specific) <- sub("Rating","Elo",colnames(mat_res_elo_specific))
      colnames(mat_res_elo_specific) <- c(colnames(mat_res_elo_specific)[1],paste(paste(ranges_distance,collapse ='-'),colnames(mat_res_elo_specific)[2],sep="_"))
      mat_res_elo_specific <- merge(mat_res_elo_specific,infos_horse_new_race[,c('Cheval','Numero')],by.x="Cheval",by.y="Cheval",all.y = TRUE)
      mat_res_elo_specific <- mat_res_elo_specific[,-ncol(mat_res_elo_specific)]
      mat_res_rating_specific <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list(mat_res_elo_specific,mat_res_glicko_specific,mat_res_steph_specific))
      mat_res_rating_specific <- transform(mat_res_rating_specific,EGS=(mat_res_rating_specific[,2]+mat_res_rating_specific[,3]+mat_res_rating_specific[,4])/3)
      colnames(mat_res_rating_specific) [ncol(mat_res_rating_specific)] <- paste("EGS",paste(ranges_distance,collapse ='-'),sep="_")
      mat_res_rating_specific <- mat_res_rating_specific[,c("Cheval",paste("EGS",paste(ranges_distance,collapse ='-'),sep="_"))]
      list_res_rating_specific_compliled [[clus]] <- mat_res_rating_specific
    }
  }
  list_res_rating_specific_compliled <- list_res_rating_specific_compliled[lapply(list_res_rating_specific_compliled,length)>0]
  mat_res_rating_specific_compliled <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_res_rating_specific_compliled)
  mat_res_rating_all_together <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list(A=mat_res_rating_golbal,B=mat_res_rating_specific_compliled))
  mat_res_rating_all_together <- merge(mat_res_rating_all_together,infos_horse_new_race,by.x="Cheval",by.y="Cheval",all.y = TRUE)
  vec_order_column <- c(c("Numero","Cheval","Cote.1","Cote.2","Cote.3","Distance","Musique"),setdiff(colnames(mat_res_rating_all_together),c("Numero","Cheval","Cote.1","Cote.2","Cote.3","Distance","Musique")))
  vec_order_column <- intersect(vec_order_column,colnames(mat_res_rating_all_together))
  mat_res_rating_all_together <- mat_res_rating_all_together[,vec_order_column]
  colnames(mat_res_rating_all_together) <- gsub("\\.","_",colnames(mat_res_rating_all_together))
return(mat_res_rating_all_together)
}


# unlist(lapply(grep('EGS',colnames(mat_res_rating_all_together),value=TRUE)[-1],function(x){unlist(strsplit(x,"-"))[2]})) [order(as.numeric(unlist(lapply(grep('EGS',colnames(mat_res_rating_all_together),value=TRUE)[-1],function(x){unlist(strsplit(x,"-"))[2]}))))]

