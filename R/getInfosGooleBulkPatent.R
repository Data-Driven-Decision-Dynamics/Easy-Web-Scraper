#' Extract patent detailed informations
#' @param  file2process patent file to parse
#' @return infos.current.patent a data frame with the following title,abstract,assignee,year
#' @export
#' @importFrom xml2 read_xml
#' @importFrom xml2 xml_find_all
#' @importFrom xml2 xml_ns
#' @importFrom xml2 xml_text
#' @importFrom magrittr %>%
#' @examples
#' getInfosGooleBulkPatent('/home/rd.loreal/payem/work/met/20-Patent-Retrieval/output/data/ipg050308')
#' @export

getInfosGooleBulkPatent <- function(file2process)
{
  # file.content <- read_xml(file2process)
  
  tryCatch(file.content <-read_xml(file2process),error=function(e){cat("ERROR :",conditionMessage(e), "\n")})
  assignees.orgnames <- xml_find_all(file.content, ".//assignee",xml_ns(file.content)) %>% xml_text()
  assignees.orgnames <- unlist(lapply(assignees.orgnames,function(x){return(gsub("\n\n","",gsub("\n0.*","",x)))}))
  assignees.orgnames <- ifelse(length(assignees.orgnames)==0,NA,paste(assignees.orgnames,collapse ='|'))
  
  countries <- xml_find_all(file.content, ".//country",xml_ns(file.content)) %>% xml_text() 
  countries <- ifelse (length(countries)==0,NA,countries[1])
  
  dates <- xml_find_all(file.content, ".//date",xml_ns(file.content)) %>% xml_text() 
  dates <- ifelse(length(dates)==0,NA, dates[1])
  
  classification.ipc <- xml_find_all(file.content, ".//main-classification",xml_ns(file.content)) %>% xml_text()
  classification.ipc <- ifelse (length(classification.ipc)==0,NA,paste(classification.ipc,collapse ='|'))
  
  titles <- xml_find_all(file.content, ".//invention-title",xml_ns(file.content)) %>% xml_text() 
  titles <- ifelse(length(titles)==0,NA, titles[1])
  
  abstracts <- xml_find_all(file.content, ".//abstract",xml_ns(file.content)) %>% xml_text() 
  abstracts <- ifelse(length(abstracts)==0,NA, abstracts[1])
  
  infos.patent <- data.frame(Assignee=assignees.orgnames,Country=countries,Date=dates,Classification=classification.ipc,
                             Title = titles, Abstract=abstracts)
  
  infos.patent <- transform(infos.patent,Content= paste(infos.patent[,'Title'],infos.patent[,'Abstract'],sep='.'))
  infos.patent [,'Content'] <- gsub('\n','',infos.patent [,'Content'])
  
  return(infos.patent)
}

# getInfosGooleBulkPatent <- function(file2process)
# {
#   if ( sum(grep('^pg',file2process))>0) {
#     year <- substr(file2process,3,4)
#     year  <- paste0('20',year)
#   } else if (sum(grep('^ipg',file2process))>0) {
#     year <- substr(file2process,4,5)
#     year  <- paste0('20',year)
#   } else if ( sum(grep('^pftaps',file2process))>0) {
#     year <- substr(file2process,7,10)
#   }
#   xml.read.again <-  readLines(file2process)
#   patent.xml.current.file.assignee <- rev(unlist(strsplit(grep('orgname',xml.read.again,value=TRUE),";")))[1]
#   patent.xml.current.file.assignee <- gsub('<orgname>','',patent.xml.current.file.assignee)
#   patent.xml.current.file.assignee <- gsub('</orgname>','',patent.xml.current.file.assignee)
#   if(length(nchar(patent.xml.current.file.assignee))<1)
#   {
#     patent.xml.current.file.assignee =NA
#     patent.xml.current.file <- xmlParse(file2process)
#     root.patent.xml.current.file <- xmlRoot(patent.xml.current.file)
#     names.patent.xml.current.file <- xmlSApply(root.patent.xml.current.file, names)
#     patent.xml.current.file.title <- as.vector(xmlSApply(root.patent.xml.current.file[[1]], xmlValue)['invention-title'])
#     if(length(class(root.patent.xml.current.file[[2]]))==3)
#     {
#       patent.xml.current.file.abstract <- as.vector(xmlSApply(root.patent.xml.current.file[[2]], xmlValue))
#     } else {
#       patent.xml.current.file.abstract =NA
#     }
#     infos.current.patent <- data.frame(title=patent.xml.current.file.title,
#                                        abstract=patent.xml.current.file.abstract,
#                                        assignee= patent.xml.current.file.assignee,
#                                        year=as.numeric(year)
#     )
# 
#   } else {
#     patent.xml.current.file <- xmlParse(file2process)
#     root.patent.xml.current.file <- xmlRoot(patent.xml.current.file)
#     names.patent.xml.current.file <- xmlSApply(root.patent.xml.current.file, names)
#     patent.xml.current.file.title <- as.vector(xmlSApply(root.patent.xml.current.file[[1]], xmlValue)['invention-title'])
#     if(class(root.patent.xml.current.file[[2]])!="NULL")
#     {
#       patent.xml.current.file.abstract <- as.vector(xmlSApply(root.patent.xml.current.file[[2]], xmlValue))
#     } else {
#       patent.xml.current.file.abstract =NA
#     }
#     infos.current.patent <- data.frame(title=patent.xml.current.file.title,
#                                        abstract=patent.xml.current.file.abstract,
#                                        assignee= patent.xml.current.file.assignee,
#                                        year=as.numeric(year)
#     )
#   }
# 
#   return(infos.current.patent)
# }
