get_delay_last_race_distance_geny <- function(url_target_race = "http://www.geny.com/partants-pmu/2019-05-01-saint-cloud-pmu-prix-de-beaumesnil_c1054945") {
  
  if(!exists("mat_historical_races")) {
    mat_historical_races <- readRDS(path_historical_races)
    mat_historical_races$Cheval <- as.vector(mat_historical_races$Cheval)
    mat_historical_races$Race <- as.vector(mat_historical_races$Race)
    mat_historical_races$Date <- as.Date(mat_historical_races$Date)
    mat_historical_races$Prix <- as.numeric(mat_historical_races$Prix)
    mat_historical_races$Gains <- as.numeric(mat_historical_races$Gains)
  }
  
  if(!is.null(url_target_race)) {
    mat_infos_target_race <- get_details_race_geny(url_target_race)
  }

  infos_candidate_races <- get_historical_performance_horse_race_geny(url_target_race,number_days_back_in_time=365)
  mat_perf_race_target_horses <- infos_candidate_races$mat_perf_race_target_horses
  mat_infos_target_race_add <- infos_candidate_races$mat_infos_target_race_add[,c("Cheval","Numero")]

  mat_last_race_distance_horses <- mat_perf_race_target_horses %>% group_by(Cheval,Distance) %>% top_n(1,Date)
  mat_last_race_distance_horses <- mat_last_race_distance_horses[,c("Cheval","Distance","Date")]
  
  mat_last_race_distance_melt <- reshape2::melt(mat_perf_race_target_horses,id.vars=c("Cheval","Distance"),measure.vars="Date")
  mat_last_race_distance_cast <- dcast(mat_last_race_distance_melt,Cheval ~ Distance ,fun.aggregate=max)
  
  
  mat_earning_distance_cast[is.na(mat_earning_distance_cast)] <- 0
  
  mat_earning_distance_cast <- merge(mat_earning_distance_cast,mat_infos_target_race_add[,c("Numero","Cheval")],by.x="Cheval",by.y="Cheval",all=TRUE)
  mat_earning_distance_cast <- mat_earning_distance_cast[,c("Numero","Cheval",setdiff(colnames(mat_earning_distance_cast),c("Numero","Cheval")))]

  return(mat_earning_distance_cast)
}

