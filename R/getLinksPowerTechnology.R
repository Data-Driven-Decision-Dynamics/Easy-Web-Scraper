#' Extract news from one page of the site http://www.power-technology.com
#' @param  url.path URL of the page to scrape
#' @return links.news a vector of link to scrape
#' @export
#' @importFrom xml2 read_html
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' getLinksPowerTechnology('http://www.power-technology.com/news/industry_news_archive.html')
#' @export
getLinksPowerTechnology <- function( url.path='http://www.power-technology.com/news/industry_news_archive.html')
{
  page  <- read_html(url.path)
  links.news <- str_trim(page %>% html_nodes('a') %>% html_attr("href"))
  links.news <- links.news[grep('^/news/news',links.news)]
  links.news <- paste0('http://www.power-technology.com',links.news)
  return(links.news)
}
