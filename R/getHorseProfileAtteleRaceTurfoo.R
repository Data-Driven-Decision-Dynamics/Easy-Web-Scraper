getHorseProfileAtteleRaceTurfoo <- function (path.race.data = "/home/rd.loreal/payem/Futanke-Mbayar/input/matrix-performances-all-races.csv",
                                             url.path.new.race='http://www.turfoo.fr/programmes-courses/170909/reunion1-vincennes/course4-pxrmc-finale-european-trotting-masters/',
                                             race.reference.select.type = 'mixed',
                                             number.distances.frequent  = 3,
                                             number.distances.similar   = 2data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAWElEQVR42mNgGPTAxsZmJsVqQApgmGw1yApwKcQiT7phRBuCzzCSDSHGMKINIeDNmWQlA2IigKJwIssQkHdINgxfmBBtGDEBS3KCxBc7pMQgMYE5c/AXPwAwSX4lV3pTWwAAAABJRU5ErkJggg==,
                                             date.race.min ="01/01/2014",
                                             delta.acceptable.distance = 100
)
{
  mat.perf.all.races <- as.data.frame(read_csv(path.race.data))
  mat.perf.all.races <- mat.perf.all.races [!is.na(mat.perf.all.races$Distance),]
  mat.perf.all.races$RACE_TYPE <- NA
  mat.perf.all.races$RACE_TYPE[as.numeric(mat.perf.all.races$Distance)<=1200] <-'Sprint' 
  mat.perf.all.races$RACE_TYPE[as.numeric(mat.perf.all.races$Distance)>1200 & as.numeric(mat.perf.all.races$Distance)<=1600 ] <-'Flyer'
  mat.perf.all.races$RACE_TYPE[as.numeric(mat.perf.all.races$Distance)>1600 & as.numeric(mat.perf.all.races$Distance)<=2000 ] <-'Miler'
  mat.perf.all.races$RACE_TYPE[as.numeric(mat.perf.all.races$Distance)>2000 & as.numeric(mat.perf.all.races$Distance)<=2400 ] <-'Classique'
  mat.perf.all.races$RACE_TYPE[as.numeric(mat.perf.all.races$Distance)>2400] <-'Marathon'
  
  infos.horse.new.race <- getDetailsGivenRacingTurfo(url.path = url.path.new.race)
  race.horses <- unique(as.vector(infos.horse.new.race$Cheval))
  if(length(race.horses)<2)
  {
    stop('This function requires at 2 least 2 horses')
  }
  nb.horses.unknow <- length(setdiff(race.horses,unique(mat.perf.all.races$Cheval)))
  if(nb.horses.unknow>0)
  {
    horses.unknow <- setdiff(race.horses,unique(mat.perf.all.races$Cheval))
    message(paste(paste(setdiff(race.horses,unique(mat.perf.all.races$Cheval)),collapse = ",")," not present in knowledgebase"))
  }
  
  race.distance <- unique(infos.horse.new.race[,'Distance'])
  race.distance <- str_trim(gsub('mètres','',race.distance))
  race.distance <- as.numeric(race.distance)
  race.distance <- min(race.distance)
  
  if(class(race.distance)!='numeric')
  {
    stop('This function requires that the race distace must be given and should be numeric')
  }
  
  race.discipline <- as.vector(unique(infos.horse.new.race[,'Discipline']))
  
  mat.perf.races.current.horse <- subset(mat.perf.all.races,Cheval %in% race.horses)
  mat.perf.races.current.horse <- subset(mat.perf.races.current.horse,Discipline==race.discipline)
  mat.perf.races.current.horse <- subset(mat.perf.races.current.horse, Date >=as.Date(date.race.min,format = "%d/%m/%Y"))
  mat.perf.races.current.horse <- subset(mat.perf.races.current.horse, Distance <=race.distance+delta.acceptable.distance & Distance >=race.distance-delta.acceptable.distance)
  
  list.mat.perf.races.current.horse$`Midnight Hour`
  ########## Vitesse Estimation
  
  mat.perf.races.current.horse <- transform(mat.perf.races.current.horse, Time  = ReductionFormat*(Distance/1000))
  mat.perf.races.current.horse <- transform(mat.perf.races.current.horse, Speed = Distance/Time)
   
  list.mat.perf.races.current.horse <- lapply(split.data.frame(mat.perf.races.current.horse,mat.perf.races.current.horse$Cheval),getAverageSpeedHorse)
  mat.average.speed.horse <- rbindlist(list.mat.perf.races.current.horse,fill = TRUE)
  mat.average.speed.horse <- as.data.frame(mat.average.speed.horse)
  infos.horse.new.race.joint <- infos.horse.new.race[,c('Numero','Cheval','Distance')]
  mat.average.speed.horse <- merge(mat.average.speed.horse,infos.horse.new.race.joint,by.x='Cheval',by.y='Cheval',all.x=TRUE ,all.y=TRUE)
  colnames(mat.average.speed.horse) <- sub('Distance.x','MeanDistance',colnames(mat.average.speed.horse))
  colnames(mat.average.speed.horse) <- sub('Distance.y','Distance',colnames(mat.average.speed.horse))
  mat.average.speed.horse <- transform(mat.average.speed.horse,Time= Distance/AverageSpeed)
  mat.average.speed.horse$Cheval <- paste(mat.average.speed.horse$Numero,mat.average.speed.horse$Cheval,sep="-")
  mat.average.speed.horse <- mat.average.speed.horse[order(mat.average.speed.horse$Time),]
  
  mat.average.speed.horse <- mat.average.speed.horse[rownames(unique(mat.average.speed.horse[,c('Cheval','Time','Distance')])),]
  
  xlim=c(min(mat.average.speed.horse$MeanDistance)-50,max(mat.average.speed.horse$MeanDistance)+50)
  par(las=1)
  plot(mat.average.speed.horse[,'MeanDistance'],mat.average.speed.horse[,'Speed'],ylab='Average Speed',xlab='Average Distance',type='n',xlim=xlim)
  text(mat.average.speed.horse[,'MeanDistance'],mat.average.speed.horse[,'Speed'],mat.average.speed.horse$Cheval,cex=0.75)
  abline(v=race.distance,col="orange",lty=2)
  par(las=1)
  plot(mat.average.speed.horse[,'MeanDistance'],mat.average.speed.horse[,'Time'],ylab='Estimated Time',xlab='Average Distance',type='n',xlim=xlim)
  text(mat.average.speed.horse[,'MeanDistance'],mat.average.speed.horse[,'Time'],mat.average.speed.horse$Cheval,cex=0.75)
  abline(v=race.distance,col="orange",lty=2)
  par(las=1)
  plot(mat.average.speed.horse[,'Speed'],mat.average.speed.horse[,'Time'],ylab='Estimated Time',xlab='Average Speed',type='n')
  text(mat.average.speed.horse[,'Speed'],mat.average.speed.horse[,'Time'],mat.average.speed.horse$Cheval,cex=0.75)
  abline(v=race.distance,col="orange",lty=2)
  ########## Vitesse Estimation
  
  
  ########## Tentative Ranking Horses based on previous races 
  mat.horses.direct.ranking <- NULL
  mat.perf.races.complete.current.horse <- mat.perf.races.current.horse
  vec.num.horses.race <- table(mat.perf.races.current.horse$Race)
  vec.candidates.races.ranking <- names(vec.num.horses.race)[vec.num.horses.race>1] 
  for(race in vec.candidates.races.ranking)
  {
    mat.horses.direct.ranking.current.race <- NULL
    mat.perf.races.complete.current.horse.candidate.race <- subset(mat.perf.races.complete.current.horse,Race == race & Cheval %in% race.horses)
    mat.perf.races.complete.current.horse.candidate.race <- mat.perf.races.complete.current.horse.candidate.race[order(mat.perf.races.complete.current.horse.candidate.race[,'Reduction']),]
    for(id.horse in 1:(nrow(mat.perf.races.complete.current.horse.candidate.race)-1))
    {
      mat.horses.direct.ranking.current.race.current.horse <- NULL
      id.horse.compare <- (id.horse+1): nrow(mat.perf.races.complete.current.horse.candidate.race) 
      for(id.horse.next in id.horse.compare)
      {
        if(!is.na(mat.perf.races.complete.current.horse.candidate.race[id.horse,'Reduction']) & !is.na(mat.perf.races.complete.current.horse.candidate.race[id.horse.next,'Reduction']))
        {
          delta.time.compared.horses   <- mat.perf.races.complete.current.horse.candidate.race[id.horse.next,'Reduction']-mat.perf.races.complete.current.horse.candidate.race[id.horse,'Reduction']
          if(delta.time.compared.horses>=0)
          {
            race.name <- race
            date.name <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Date']
            distance <-  min(mat.perf.races.complete.current.horse.candidate.race[id.horse:id.horse.next,'Distance'])
            prix <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Prix']
            terrain <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Terrain']
            corde <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Corde']
            winner <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Cheval']
            looser <- mat.perf.races.complete.current.horse.candidate.race[id.horse.next,'Cheval']
            distance.winner <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Distance']
            distance.looser <- mat.perf.races.complete.current.horse.candidate.race[id.horse.next,'Distance']
            temps.winner <- mat.perf.races.complete.current.horse.candidate.race[id.horse,'Reduction']
            temps.looser <- mat.perf.races.complete.current.horse.candidate.race[id.horse.next,'Reduction']
            mat.horses.direct.ranking.current.race.current.horse.item <- data.frame(Race=race.name,Date=date.name,Distance=distance,Prix=prix,Terrain=terrain,Corde=corde,Winner=winner,Looser=looser,
                                                                                    DistanceWinner=distance.winner,DistanceLooser=distance.looser,TempsWinner=temps.winner,TempsLooser=temps.looser)
            print(mat.horses.direct.ranking.current.race.current.horse.item)
          }
          mat.horses.direct.ranking.current.race.current.horse <- rbind(mat.horses.direct.ranking.current.race.current.horse,mat.horses.direct.ranking.current.race.current.horse.item)
        }
        mat.horses.direct.ranking.current.race <- rbind(mat.horses.direct.ranking.current.race.current.horse,mat.horses.direct.ranking.current.race) 
      }
    }
    
    mat.horses.direct.ranking <- rbind(mat.horses.direct.ranking,mat.horses.direct.ranking.current.race)
  }
  mat.horses.direct.ranking  <- mat.horses.direct.ranking[as.vector(mat.horses.direct.ranking$Winner)!=as.vector(mat.horses.direct.ranking$Looser),] 
  mat.horses.direct.ranking  <- transform(mat.horses.direct.ranking,DeltaTemps=TempsWinner-TempsLooser)
  mat.horses.direct.ranking  <- transform(mat.horses.direct.ranking,DeltaDistance=DistanceWinner-DistanceLooser)
  mat.horses.direct.ranking  <- unique(mat.horses.direct.ranking)
  mat.horses.direct.ranking  <- transform(mat.horses.direct.ranking,Delay=Sys.Date()-Date)
}


mat.winner.looser.direct.ranking <- table(mat.horses.direct.ranking$Winner,mat.horses.direct.ranking$Looser)
col.names<- colnames(mat.winner.looser.direct.ranking)
row.names <- rownames(mat.winner.looser.direct.ranking)
mat.winner.looser.direct.ranking <- as.matrix.data.frame(mat.winner.looser.direct.ranking)
colnames(mat.winner.looser.direct.ranking) <- col.names
rownames(mat.winner.looser.direct.ranking) <- row.names

getNodesEdgesListFromMatrix (network.data=mat.winner.looser.direct.ranking)

########## Tentative Ranking Horses based on previous races

  
  
  ########## Speciality Analysis 
  mat.perf.current.horse.race.type.group <- group_by(mat.perf.races.current.horse ,Cheval,RACE_TYPE)
  mat.perf.current.horse.gains.race.type.melt <- melt(mat.perf.current.horse.race.type.group, id=c("Cheval", "RACE_TYPE"),measure.vars='Gains',na.rm=TRUE)
  mat.perf.current.horse.gains.race.type <- cast(mat.perf.current.horse.gains.race.type.melt, Cheval ~ RACE_TYPE ~ variable,sum)
  mat.perf.current.horse.gains.race.type <- as.data.frame(mat.perf.current.horse.gains.race.type)
  colnames(mat.perf.current.horse.gains.race.type) <- gsub('.Gains','',colnames(mat.perf.current.horse.gains.race.type))
  if(ncol(mat.perf.current.horse.gains.race.type)>1)
  {
    mat.perf.current.horse.gains.race.type <- mat.perf.current.horse.gains.race.type/apply(mat.perf.current.horse.gains.race.type,1,sum)
    mds <- cmdscale(1-cor(t(mat.perf.current.horse.gains.race.type)))
    plot(mds,type='n',xlab='',ylab='')
    text(mds,rownames(mds),cex=0.75)
    grid(20,20)
  }
  ########## Speciality Analysis 
  
  
  ########## Profile Exploration 
  
  mat.perf.races.current.horse <- transform(mat.perf.races.current.horse,Vitesse=Distance/(Reduction*(Distance/1000)))
  mat.perf.races.current.horse.group <- group_by(mat.perf.races.current.horse,Distance)
  matrix.records.discipline <- summarise(mat.perf.races.current.horse.group,
                                         Record=min(Reduction,na.rm = TRUE)
  )
  matrix.records.discipline <- as.data.frame(matrix.records.discipline)
  matrix.records.discipline <- transform(matrix.records.discipline,Vitesse=Distance/(Record*(Distance/1000)))
  
  mat.profile.perf.all.distances.infos <- NULL
  par(mar=c(5,3,4,2))
  par(mfrow=c(3,6))
  for(horse in race.horses)
  {
    mat.profile.perf.all.distances.infos.current.horse <- subset(mat.perf.races.current.horse,Cheval==horse)[,c('Cheval','Date','Distance','Reduction')]
    mat.profile.perf.all.distances.infos.current.horse <- mat.profile.perf.all.distances.infos.current.horse[!is.na(mat.profile.perf.all.distances.infos.current.horse$Reduction),]
    mat.profile.perf.all.distances.infos.current.horse <- mat.profile.perf.all.distances.infos.current.horse[order(mat.profile.perf.all.distances.infos.current.horse$Distance,mat.profile.perf.all.distances.infos.current.horse$Date),]
    mat.profile.perf.all.distances.infos.current.horse <- transform(mat.profile.perf.all.distances.infos.current.horse,Vitesse=Distance/(Reduction*(Distance/1000)))
    mat.profile.perf.all.distances.infos.current.horse$Order <- 1:nrow(mat.profile.perf.all.distances.infos.current.horse)
    vec.label.axis <- unique(mat.profile.perf.all.distances.infos.current.horse$Distance)
    vec.label.axis <- vec.label.axis[!is.na(vec.label.axis)]
    vec.val.axis.infos <- NULL
    for(label in vec.label.axis )
    {
      val.max.axis <- max(subset(mat.profile.perf.all.distances.infos.current.horse,Distance==label)[,'Order'])
      vec.val.axis.infos <- c(vec.val.axis.infos,val.max.axis)
    }
    vec.val.axis.infos <- c(0,vec.val.axis.infos)
    vec.val.axis <- NULL
    for(id.label in 1:length(vec.label.axis) )
    {
      vec.val.axis.current <- vec.val.axis.infos[id.label]+(vec.val.axis.infos[id.label+1]-vec.val.axis.infos[id.label])/2
      vec.val.axis <- c(vec.val.axis,vec.val.axis.current)
    }
    par(las=2)
    names(vec.val.axis) <- vec.label.axis
    col.points <- as.numeric(mat.profile.perf.all.distances.infos.current.horse$Distance)
    if(length(unique(col.points))>12)
    {
      col.availables <- brewer.pal(12,'Paired')[1:length(vec.label.axis[1:12])]
      col.availables <- c(col.availables,col.availables,col.availables,col.availables)
    } else {
      col.availables <- brewer.pal(12,'Paired')[1:length(vec.label.axis)]
    }
    #names(col.availables) <- unique(col.points)
    ylim.max <- max(matrix.records.discipline[,'Vitesse'],na.rm = TRUE)
    ylim.min <- min(mat.perf.races.current.horse[,'Vitesse'],na.rm = TRUE)
    
    xlim.min <- -1
    xlim.max <- max(vec.val.axis)+2
    
    # vec.val.plot <- mat.profile.perf.all.distances.infos.current.horse[,'Vitesse']
    # plot(vec.val.axis[as.factor(mat.profile.perf.all.distances.infos.current.horse$Distance)],mat.profile.perf.all.distances.infos.current.horse[,'Vitesse'],ylab='Vitesse',xlab='',axes = FALSE,col=col.availables[as.factor(col.points)],pch=20,cex=0.75,ylim=c(ylim.min,ylim.max),main=horse,xlim=c(xlim.min,xlim.max),type='n')
    # for( i in names(vec.val.axis))
    # {
    #   mat.point.current <- subset(mat.profile.perf.all.distances.infos.current.horse,Distance==as.numeric(i))
    #   if(nrow(mat.point.current)==1)
    #   {
    #     points(vec.val.axis[i],mat.point.current$Vitesse,col=col.availables[i],pch=20,cex=0.75)
    #   } else {
    #     
    #   }
      
      # abs.point.plot <- as.vector(vec.val.axis[as.character(mat.profile.perf.all.distances.infos.current.horse$Distance[i])])
    #}
    
    
    plot(vec.val.axis[as.factor(mat.profile.perf.all.distances.infos.current.horse$Distance)],mat.profile.perf.all.distances.infos.current.horse[,'Vitesse'],ylab='Vitesse',xlab='',axes = FALSE,col=col.availables[as.factor(col.points)],pch=20,cex=0.75,ylim=c(ylim.min,ylim.max),main=horse,xlim=c(xlim.min,xlim.max))
    axis(1,at=vec.val.axis,labels=vec.label.axis,cex=0.75)
    axis(2)
    box(,col='blue')
    #grid(10,10)
    for(i in vec.val.axis.infos[-1])
    {
      abline(v=i,lwd=0.5,lty=2,col='grey')
    }

    for(i in 1:length(vec.label.axis))
    {
      x0 <- vec.val.axis.infos[i]
      x1 <- vec.val.axis.infos[i+1]
      y0 <- subset(matrix.records.discipline,Distance==vec.label.axis[i])[,'Record']
      if(is.finite(y0))
      {
        y0 <- vec.label.axis[i]/(y0*(vec.label.axis[i]/1000))
        y1 <- y0
        segments(x0,y0,x1,y1,col='red')
      }

    }
  }
  
  mat.profile.perf.all.distances <- table(mat.perf.races.current.horse$Cheval,mat.perf.races.current.horse$Distance)
  col.names<- colnames(mat.profile.perf.all.distances)
  row.names <- rownames(mat.profile.perf.all.distances)
  mat.profile.perf.all.distances <- as.matrix.data.frame(mat.profile.perf.all.distances)
  colnames(mat.profile.perf.all.distances) <- col.names
  rownames(mat.profile.perf.all.distances) <- row.names
  
  mat.perf.races.current.horse.group <- group_by(mat.perf.races.current.horse,Cheval,Distance)
  mat.perf.current.horse.gains.distances.melt <- melt(mat.perf.races.current.horse.group, id=c("Cheval", "Distance"),measure.vars='Gains',na.rm=TRUE)
  mat.perf.current.horse.gains.distances <- cast(mat.perf.current.horse.gains.distances.melt, Cheval ~ Distance ~ variable,sum)
  mat.perf.current.horse.gains.distances <- as.data.frame(mat.perf.current.horse.gains.distances)
  colnames(mat.perf.current.horse.gains.distances) <- gsub('.Gains','',colnames(mat.perf.current.horse.gains.distances))
  mat.perf.current.horse.gains.distances <- mat.perf.current.horse.gains.distances[apply(mat.perf.current.horse.gains.distances,1,max)>0,apply(mat.perf.current.horse.gains.distances,2,max)>0]
  ########## Profile Exploration
  
  
  
  ########## Speciality Analysis 
  # mat.perf.current.horse.race.type.group <- group_by(mat.perf.races.current.horse ,Cheval,RACE_TYPE)
  # mat.perf.current.horse.gains.race.type.melt <- melt(mat.perf.current.horse.race.type.group, id=c("Cheval", "RACE_TYPE"),measure.vars='Gains',na.rm=TRUE)
  # mat.perf.current.horse.gains.race.type <- cast(mat.perf.current.horse.gains.race.type.melt, Cheval ~ RACE_TYPE ~ variable,sum)
  # mat.perf.current.horse.gains.race.type <- as.data.frame(mat.perf.current.horse.gains.race.type)
  # colnames(mat.perf.current.horse.gains.race.type) <- gsub('.Gains','',colnames(mat.perf.current.horse.gains.race.type))
  # mat.perf.current.horse.gains.race.type <- mat.perf.current.horse.gains.race.type/apply(mat.perf.current.horse.gains.race.type,1,sum)
  # mds <- cmdscale(1-cor(t(mat.perf.current.horse.gains.race.type)))
  # plot(mds,type='n',xlab='',ylab='')
  # text(mds,rownames(mds),cex=0.75)
  # grid(20,20)
  ########## Speciality Analysis 
  
  ########## Tentative Distance Affinity Estimation 
  mat.perf.current.horse.distances.group <- group_by(mat.perf.races.current.horse ,Cheval,Distance)
  mat.perf.current.horse.gains.distances.melt <- melt(mat.perf.current.horse.distances.group, id=c("Cheval", "Distance"),measure.vars='Gains',na.rm=TRUE)
  mat.perf.current.horse.gains.distances <- cast(mat.perf.current.horse.gains.distances.melt, Cheval ~ Distance ~ variable,sum)
  mat.perf.current.horse.gains.distances <- as.data.frame(mat.perf.current.horse.gains.distances)
  colnames(mat.perf.current.horse.gains.distances) <- gsub('.Gains','',colnames(mat.perf.current.horse.gains.distances))
  mat.perf.current.horse.gains.distances <- mat.perf.current.horse.gains.distances[apply(mat.perf.current.horse.gains.distances,1,max)>0,apply(mat.perf.current.horse.gains.distances,2,max)>0]
  mat.perf.current.horse.gains.distances <- t(mat.perf.current.horse.gains.distances/ apply(mat.perf.current.horse.gains.distances,1,sum))
  if(race.reference.select.type=='mixed')
  {
    vec.distances.most.frequent <- as.numeric(names(sort(table(mat.perf.races.current.horse$Distance),decreasing = TRUE)))[1:number.distances.frequent]
    vec.distances.available <-sort(as.numeric(names(sort(table(mat.perf.races.current.horse$Distance),decreasing = TRUE))))
    id.distance.race <- grep(race.distance,vec.distances.available)
    id.distance.race.range <- (id.distance.race-number.distances.similar):(id.distance.race+number.distances.similar)
    id.distance.race.range <- intersect(id.distance.race.range,1:length(vec.distances.available))
    vec.distances.most.similar <- vec.distances.available[id.distance.race.range]
    vec.distances.most.similar <- intersect(vec.distances.most.similar,rownames(mat.perf.current.horse.gains.distances))
    vec.distances.selected <- unique(intersect(as.character(vec.distances.most.similar),as.character(vec.distances.most.frequent)))
    if(length(vec.distances.selected)>1) {
      weight.selected.distances <- apply(mat.perf.current.horse.gains.distances[vec.distances.selected,],2,sum)
      affinity.selected.distance.horses <- weight.selected.distances[sort(names(weight.selected.distances))]
    } else if(length(vec.distances.selected)==1) {
      affinity.selected.distance.horses <- mat.perf.current.horse.gains.distances[vec.distances.selected,]
      affinity.selected.distance.horses <- affinity.selected.distance.horses[sort(names(affinity.selected.distance.horses))]
    } else {
      
      vec.candidates.distances <- unique(c(as.character(vec.distances.most.similar),as.character(vec.distances.most.frequent)))
      vec.delta.distances <- abs(race.distance-as.numeric(vec.candidates.distances))
      vec.distances.selected <- vec.candidates.distances[vec.delta.distances<=delta.acceptable.distance]
      affinity.selected.distance.horses <- mat.perf.current.horse.gains.distances[vec.distances.selected,]
      affinity.selected.distance.horses <- affinity.selected.distance.horses[sort(names(affinity.selected.distance.horses))]
    }
  }
  
  
  ########## Tentative Distance Affinity Estimation
  
 



# mat.perf.races.current.horse.dt <- as.data.table(mat.perf.races.current.horse)
# mat.infos.best.ecart.current.horse  <- mat.perf.races.current.horse.dt[,.SD[which.min(Reduction)],by=Cheval]
# mat.infos.best.ecart.current.horse  <- as.data.frame(mat.infos.best.ecart.current.horse[,c('Cheval','Distance','Reduction')])
# colnames(mat.infos.best.ecart.current.horse) <- c('Cheval','DistanceBest','ReductionBest')
# mat.infos.worst.ecart.current.horse <- mat.perf.races.current.horse.dt[,.SD[which.max(Reduction)],by=Cheval]
# mat.infos.worst.ecart.current.horse  <- as.data.frame(mat.infos.worst.ecart.current.horse[,c('Cheval','Distance','Reduction')])
# colnames(mat.infos.worst.ecart.current.horse) <- c('Cheval','DistanceWorst','ReductionWorst')
# mat.reference.ecart.current.horse  <- merge(mat.infos.best.ecart.current.horse,mat.infos.worst.ecart.current.horse,by.x='Cheval',by.y='Cheval')


# mat.reference.ecart.current.horse <-merge(mat.reference.ecart.current.horse,mat.infos.mean.ecart.current.horse,by.x='Cheval',by.y='Cheval',all.x = TRUE, all.y = TRUE)
# mat.reference.ecart.current.horse <- mat.reference.ecart.current.horse[!is.na(mat.reference.ecart.current.horse$DistanceBest),]
# 
# plot(mat.reference.ecart.current.horse$DistanceMean,mat.reference.ecart.current.horse$ReductionMean,ylab='',xlab='',type='n')
# text(mat.reference.ecart.current.horse$DistanceMean,mat.reference.ecart.current.horse$ReductionMean,mat.reference.ecart.current.horse$Cheval,cex=0.75)
# 
# par(mar=c(7,3,3,3))
# par(las=2)
# 
# mat.reference.ecart.current.horse <- mat.reference.ecart.current.horse[order(mat.reference.ecart.current.horse$ReductionBest),]
# mat.reference.ecart.current.horse <- transform(mat.reference.ecart.current.horse,ReductionMedian=(ReductionBest+ReductionWorst)/2)
# errbar(1:nrow(mat.reference.ecart.current.horse), mat.reference.ecart.current.horse$ReductionMedian,mat.reference.ecart.current.horse$ReductionBest, mat.reference.ecart.current.horse$ReductionWorst, axes = FALSE,ylab='Reduction Kilometrique',xlab='',errbar.col='orange')
# axis(1, 1:nrow(mat.reference.ecart.current.horse),mat.reference.ecart.current.horse$Cheval)
# axis(2)
# box(,col='blue')
# grid(10,10)
# title('Intervalle de confiance pour la RK',cex.main=0.9)
# 
# infos.horse.new.race.joint = infos.horse.new.race[,c('Cheval','Distance')]
# mat.reference.ecart.current.horse  <- merge(mat.reference.ecart.current.horse,infos.horse.new.race.joint,by.x='Cheval',by.y='Cheval',all.x = TRUE, all.y = TRUE)
# mat.reference.ecart.current.horse  <- transform(mat.reference.ecart.current.horse,Temps = ReductionMean*(Distance/1000))
# 
# mat.infos.most.recent.ecart.current.horse <- mat.perf.races.complete.current.horse.dt[,.SD[which.max(Date)],by=Cheval]

# mat.perf.races.current.horse.group <- group_by(mat.perf.races.current.horse,Cheval)
# mat.indicators.current.horse <- summarise(mat.perf.races.current.horse.group,
#                                           Reduction=min(Reduction,na.rm = TRUE),
#                                           Distance =mean(Distance,na.rm = TRUE),
#                                           Vitesse =Distance/Reduction
# )
# 
# plot(mat.indicators.current.horse$Distance,mat.indicators.current.horse$Reduction,type='n',xlab='Distance',ylab='Affinity')
# text(mat.indicators.current.horse$Distance,mat.indicators.current.horse$Reduction,mat.indicators.current.horse$Cheval)
# for(distance in as.character(vec.distances.most.frequent))
# {
#   abline(v=as.numeric(distance),col='orange',lty=2)
# }
########## Vitesse Estimation





mat.perf.races.complete.current.horse.dt <- as.data.table(mat.perf.races.complete.current.horse)
mat.infos.best.ecart.current.horse  <- mat.perf.races.complete.current.horse.dt[,.SD[which.min(EcartFormat)],by=Cheval]
mat.infos.worst.ecart.current.horse <- mat.perf.races.complete.current.horse.dt[,.SD[which.max(EcartFormat)],by=Cheval]
mat.infos.most.recent..ecart.current.horse <- mat.perf.races.complete.current.horse.dt[,.SD[which.max(Date)],by=Cheval]
mat.infos.heaviest.current.horse <- mat.perf.races.complete.current.horse.dt[,.SD[which.max(Poids)],by=Cheval]
infos.horse.new.race.joint <- infos.horse.new.race[,c('Cheval','Poids','Distance')]
colnames(infos.horse.new.race.joint) <- c('Cheval','PoidsCurrent','DistanceCurrent')
mat.infos.best.ecart.current.horse.joint <- mat.infos.best.ecart.current.horse[,c('Cheval','Distance','Poids','EcartFormat')]
mat.infos.best.ecart.current.horse.joint <- merge(infos.horse.new.race.joint,mat.infos.best.ecart.current.horse.joint,by.x='Cheval',by.y='Cheval')
mat.infos.best.ecart.current.horse.joint <- transform(mat.infos.best.ecart.current.horse.joint,DeltaPoids=Poids-PoidsCurrent)
mat.infos.best.ecart.current.horse.joint <- transform(mat.infos.best.ecart.current.horse.joint,Vitesse=Distance/EcartFormat)
xlimit.abs <- max(abs(range(mat.infos.best.ecart.current.horse.joint$DeltaPoids)))
plot(mat.infos.best.ecart.current.horse.joint$DeltaPoids,mat.infos.best.ecart.current.horse.joint$Vitesse,xlab='Weight Advatange',ylab='Best Race Speed',xlim=c(-xlimit.abs,xlimit.abs),type='n')
text(mat.infos.best.ecart.current.horse.joint$DeltaPoids,mat.infos.best.ecart.current.horse.joint$Vitesse,mat.infos.best.ecart.current.horse.joint$Cheval,cex=0.75)
grid(10,10)












###
mat.perf.all.races.current.discipline <- subset(mat.perf.all.races,Discipline==race.discipline)
mat.perf.all.races.current.discipline.group <- group_by(mat.perf.all.races.current.discipline,Distance)
matrix.records.discipline <- summarise(mat.perf.all.races.current.discipline.group,
                                       Record=min(ReductionFormat,na.rm = TRUE)
)
matrix.records.discipline <- as.data.frame(matrix.records.discipline)
matrix.records.discipline <- matrix.records.discipline[complete.cases(matrix.records.discipline),]
matrix.records.discipline$Distance <- as.character(matrix.records.discipline$Distance)
rownames(matrix.records.discipline) <- as.character(matrix.records.discipline$Distance)
mat.score.gap.record.each.distance <- matrix(NA,ncol=ncol(mat.best.perf.distance.horses),nrow=nrow(mat.best.perf.distance.horses)) 
colnames(mat.score.gap.record.each.distance) <- colnames(mat.best.perf.distance.horses)
rownames(mat.score.gap.record.each.distance) <- rownames(mat.best.perf.distance.horses)

mat.perf.races.current.horse <- merge(mat.perf.races.current.horse,matrix.records.discipline,by.x='Distance',by.y='Distance')
mat.perf.races.current.horse <- transform(mat.perf.races.current.horse,Gap=Record-ReductionFormat)

mat.horse.distance.infos <- table(mat.perf.races.current.horse$Cheval,mat.perf.races.current.horse$Distance)
mat.horse.distance <- as.matrix.data.frame(mat.horse.distance.infos)
colnames(mat.horse.distance) <- colnames(mat.horse.distance.infos)
rownames(mat.horse.distance) <- rownames(mat.horse.distance.infos)
mat.horse.distance.score <- matrix(NA,nrow=nrow(mat.horse.distance),ncol=1)
colnames(mat.horse.distance.score) <- 'Affinity'
rownames(mat.horse.distance.score) <- rownames(mat.horse.distance)

for(horse in rownames(mat.score.gap.record.each.distance))
{
  distance.found <- colnames(mat.horse.distance)[mat.horse.distance[horse,]>0]
  distance.found <- distance.found[!is.na(distance.found)]
  number.races.distance <- mat.horse.distance[horse,distance.found]
  normalizer.distance <- as.numeric(distance.found)/race.distance
  normalizer.distance <- 1/(normalizer.distance/sum(normalizer.distance))
  mat.horse.distance.score[horse,1] <- weighted.mean(as.numeric(distance.found),normalizer.distance)
  distance.found <- colnames(mat.best.perf.distance.horses)[!is.infinite(t(mat.best.perf.distance.horses[horse,])[,1])]
  distance.found <- distance.found[!is.na(distance.found)]
  for(distance in distance.found)
  {
    reduction.horse.distance <- mat.best.perf.distance.horses[horse,distance]
    record.distance <- matrix.records.discipline[distance,'Record']
    delta.reduction.distance <- reduction.horse.distance-record.distance
    mat.score.gap.record.each.distance[horse,distance] <- delta.reduction.distance
  }
}

###





# 
# mat.infos.best.ecart.current.horse$Vitesse <- mat.infos.best.ecart.current.horse$Distance/mat.infos.best.ecart.current.horse$EcartFormat
# mat.infos.heaviest.current.horse$Vitesse   <- mat.infos.heaviest.current.horse$Distance/mat.infos.heaviest.current.horse$EcartFormat


mat.perf.current.horse.races.group <- group_by(mat.perf.races.current.horse,Cheval)
mat.global.indicators.horse.races <- summarise(mat.perf.current.horse.races.group,
                                               GainMean = mean(Gains,na.rm = TRUE),
                                               DistanceMean = mean(Distance,na.rm = TRUE),
                                               ReductionMean = mean(Reduction,na.rm = TRUE),
                                               MostRecentRace = max(Date,na.rm = TRUE)
)
mat.global.indicators.horse.races <- as.data.frame(mat.global.indicators.horse.races)
rownames(mat.global.indicators.horse.races) <-as.vector(mat.global.indicators.horse.races$Cheval)
mat.global.indicators.horse.races <- transform(mat.global.indicators.horse.races,DelayLastRace=Sys.Date()-MostRecentRace)
mat.global.indicators.horse.races$DelayLastRace <- as.numeric(mat.global.indicators.horse.races$DelayLastRace)
mat.global.indicators.horse.races <- mat.global.indicators.horse.races[,-c(1,5)]

plot(ReductionMean~DistanceMean, data=mat.global.indicators.horse.races,type='n',xlim=c(2700,2900))
text(mat.global.indicators.horse.races$DistanceMean,mat.global.indicators.horse.races$ReductionMean,mat.global.indicators.horse.races$Cheval,cex=0.75)
#abline(v=mean(mat.global.indicators.horse.races$Distance),col='orange',lty=2)
abline(v=2775,col='orange',lty=2,lwd=1)
abline(v=2800,col='orange',lty=2,lwd=1)
abline(v=2700,col='lightblue',lty=2,lwd=1)
abline(v=2850,col='lightblue',lty=2,lwd=1)


getProjectiondisplay(mat.global.indicators.horse.races)




write.xlsx(mat.infos.best.ecart.current.horse,file="mat.infos.best.ecart.current.horse.xlsx")
write.xlsx(mat.infos.heaviest.current.horse,file="mat.infos.heaviest.current.horse.xlsx")



colnames(mat.best.perf.distance.horses) <- as.numeric(colnames(mat.best.perf.distance.horses))
mat.best.perf.distance.horses[is.infinite(as.matrix(mat.best.perf.distance.horses))] = 0
mat.best.perf.distance.horses[is.nan(as.matrix(mat.best.perf.distance.horses))] = 0












# mat.perf.races.current.horse <- transform(mat.perf.races.current.horse,Vitesse = Distance/Reduction)
# colnames(infos.horse.new.race) <- sub('Distance.x','Distance',colnames(infos.horse.new.race))

mat.perf.races.current.horse <- transform(mat.perf.races.current.horse,Vitesse = Distance/Reduction)


race.number.horses <- unique(infos.horse.new.race[,'Chevaux'])
if(race.number.horses <0)
{
  stop('This function requires that at least one horse name to be filled')
}
vec.known.horses <- as.vector(unique(mat.perf.all.races$Cheval))
number.race.horses.known <- intersect(race.horses,vec.known.horses)
if(length(number.race.horses.known)!=length(race.horses))
{
  message (paste('No information found for these horses',setdiff(race.horses,vec.known.horses)))
}
if(length(number.race.horses.known)<1)
{
  stop('This function requires that at least one horse for which there is historical data')
}

if(race.discipline =="Trot Attele")
{
}


mat.final.choix <- merge(mat.perf.races.current.horse,matrix.records.discipline,by.x = 'Distance',by.y='Distance')
mat.final.choix <- mat.final.choix[!is.na(mat.final.choix$Reduction),]
mat.final.choix <- transform(mat.final.choix,Gap=Record-Reduction)
mat.final.choix.group <- group_by(mat.final.choix,Cheval)
mat.final.choix.score <- summarise(mat.final.choix.group,
                                   ReductionMean = mean(Reduction,na.rm = TRUE)
)
mat.final.choix.score <- merge(infos.horse.new.race[,c('Position','Cheval','DistanceReal')],mat.final.choix.score,by.x='Cheval',by.y='Cheval')
mat.final.choix.score <- transform(mat.final.choix.score,Vitesse=ReductionMean/1000)
mat.final.choix.score <- transform(mat.final.choix.score,Arrivee=Vitesse*DistanceReal)






mds.global.indicators = cmdscale(1-cor(t(mat.global.indicators.horse.races)))
plot(mds.global.indicators,type='n',xlab='',ylab='')
text(mds.global.indicators,rownames(mds.global.indicators),cex=0.8)

getProjectiondisplay <- function(mat.data=mat.score.gap.record.each.distance)
{
  cor.values <- cor(t(mat.data),use="pairwise.complete.obs") 
  id.remove.keep <- apply(cor.values,1,function(x){sum(is.na(x))}) != nrow(cor.values)
  cor.values <- cor.values[id.remove.keep,id.remove.keep]
  pca.res <- pca(cor.values,method='svdImpute')
  cor.values <- pca.res@completeObs
  mds.values = cmdscale(1-cor.values)
  xlimits <- max(abs(range(mds.values[,1])))
  ylimits <- max(abs(range(mds.values[,2])))
  
  plot(mds.values,type='n',xlab='',ylab='',xlim=c(-xlimits,xlimits),ylim=c(-ylimits,ylimits),cex.axis=0.75)
  text(mds.values,rownames(mds.values),cex=0.8,col=brewer.pal(8,'Set1')[1:4][as.factor(kmeans(mds.values,4)$cluster)])
  abline(v=0,lty=2,col='black')
  abline(h=0,lty=2,col='black')
  grid(15,15)
}


minimum = apply(mat.score.gap.record.each.distance,1,function(x){return(min(x,na.rm = TRUE))})
moyenne = apply(mat.score.gap.record.each.distance,1,function(x){return(mean(x,na.rm = TRUE))})
mediane = apply(mat.score.gap.record.each.distance,1,function(x){return(median(x,na.rm = TRUE))})
mat.score.gap.record.each.distance.average <- data.frame(Minimum=minimum,Moyenne=moyenne,Mediane=mediane)
mat.score.gap.record.each.distance.average$Cheval <- as.vector(rownames(mat.score.gap.record.each.distance.average))
mat.score.gap.record.each.distance.average <- merge(infos.horse.new.race[,c('Position','Cheval')],mat.score.gap.record.each.distance.average,by.x='Cheval',by.y='Cheval')


mat.perf.races.current.horse.year.melt <- melt(mat.perf.races.current.horse, id=c("Cheval", "Year"),measure.vars='Reduction',na.rm=TRUE)
mat.best.perf.distance.horses.year <- cast(mat.perf.races.current.horse.year.melt, Cheval ~ Year ~ variable,min)
mat.best.perf.distance.horses.year <- as.data.frame(mat.best.perf.distance.horses.year)
colnames(mat.best.perf.distance.horses.year) <- gsub('.Reduction','',colnames(mat.best.perf.distance.horses.year))
mat.best.perf.distance.horses.year[mat.best.perf.distance.horses.year==Inf] <-NA
number.missing.values <- apply(aa,1,function(x){return(sum(is.na(x)))})
aa <- aa[number.missing.values<10,number.missing.values<10]
mds <- cmdscale(1-aa)



getProjectiondisplay(mat.score.gap.record.each.distance)
getProjectiondisplay(mat.global.indicators.horse.races)




# a = apply(mat.score.gap.record.each.distance,1,function(x){mean(x,na.rm = TRUE)})
# b =mat.horse.distance.score[,1]
# plot(a,b)
# text(a,b,names(b))
# mat.horse.distance.group <- group_by(mat.perf.races.current.horse,Cheval,Distance)
# matrix.records.discipline <- summarise(mat.horse.distance.group,
#                                        Record =min(Reduction,na.rm = TRUE)
# sort(apply(mat.score.gap.record.each.distance,1,function(x){sum(x,na.rm =TRUE)}))


getDistanceAffinity <- function(mat.aff=mat.score.gap.record.each.distance,dist.race=2850)
{
  horse.label <- rownames(mat.aff)
  mat.res <- matrix(0,ncol=1,nrow=length(horse.label))
  colnames(mat.res) <- c("Affinity")
  rownames(mat.res) <- horse.label
  for(horse in horse.label )
  {
    vec.val.gap.horse   <- NULL
    distance.found <- as.numeric(colnames(mat.aff)[!is.na(mat.aff[horse,])])
    distance.found <- distance.found[distance.found >= dist.race-200 & distance.found <= dist.race+200] 
    distance.found <- as.character(distance.found)
    if(length(distance.found)>0)
    {
      for(distance in distance.found )
      {
        if( sum(!is.na(mat.aff[,distance]))>4)
        {
          vec.val.gap.horse   <- c(vec.val.gap.horse,mat.aff[horse,distance])
        }
      }
      mat.res[horse,'Affinity'] <- mean(vec.val.gap.horse)
    }
  }
}


# mat.wins.races.current.horse.melt <- melt(mat.perf.races.current.horse, id=c("Cheval", "Distance"),measure.vars='Gains',na.rm=TRUE)
# mat.wins.distance.horses <- cast(mat.wins.races.current.horse.melt, Cheval ~ Distance ~ variable,sum)
# mat.wins.distance.horses <- as.data.frame(mat.wins.distance.horses)
# colnames(mat.wins.distance.horses) <- gsub('.Gains','',colnames(mat.wins.distance.horses))
# mat.percent.wins.distance <- mat.wins.distance.horses
# for(row in rownames(mat.wins.distance.horses))
# {
#   mat.percent.wins.distance[row,] <- mat.wins.distance.horses[row,]/sum(mat.wins.distance.horses[row,])
# }



table.distances.horses <- table(mat.perf.races.current.horse$Distance,mat.perf.races.current.horse$Cheval)
mat.distances.horses <- as.matrix.data.frame(table.distances.horses)
colnames(mat.distances.horses) <- colnames(table.distances.horses)
rownames(mat.distances.horses) <- rownames(table.distances.horses)
num.distances.horses <- sort(apply(mat.distances.horses,1,function(x) {sum(x>0)}),decreasing = TRUE)
if((num.distances.horses[1]/length(race.horses))>0.79)
{
  first.candidate.distance <- names(num.distances.horses)[1]
  mat.perf.races.complete.first.candidate.distance.current.horse<-subset(mat.perf.races.complete.current.horse,Distance==first.candidate.distance)
  mat.perf.races.complete.first.candidate.distance.current.horse.dt <- as.data.table(mat.perf.races.complete.first.candidate.distance.current.horse)
  mat.best.perf.first.candidate.distance.horse  <- mat.perf.races.complete.first.candidate.distance.current.horse.dt[,.SD[which.min(EcartFormat)],by=Cheval]
  
}

