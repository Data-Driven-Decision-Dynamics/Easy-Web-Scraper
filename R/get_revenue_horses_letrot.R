#' @return mat.offres a matrix with innfos on offers
#' @export
#' @importFrom xml2 read_html
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_details_races_reunion_letrot(url.path='http://www.letrot.com/stats/courses/programme/2018-04-15/6206')
#' @export
get_details_races_reunion_letrot <- function(url.path='http://www.letrot.com/stats/fiche-course/2018-04-15/6206')
{
  require(rvest)
  require(xml2)
  require(stringr)
  require(magrittr)
  
  page <- read_html(paste0(url.path,"/1/partants/tableau"))
  races <- page %>%
    html_nodes("a") %>%
    html_attr("href")
  races <- unique(grep("tableau$",races,value=TRUE))
  races <- paste0("http://www.letrot.com",races)
  return(races)
}











#' @return mat.offres a matrix with innfos on offers
#' @export
#' @importFrom xml2 read_html
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' get_details_races_reunion_letrot(url.path='http://www.letrot.com/stats/courses/programme/2018-04-15/6206')
#' @export
get_revenue_horses_letrot <- function(url.path='http://www.letrot.com/stats/fiche-course/2018-04-15/6206/',target.date="2018-04-15")
{
  require(rvest)
  require(xml2)
  require(stringr)
  require(magrittr)

  vec.races.target.day <- get_links_race_schedule_letrot()
  vec.races.target.day <- grep(target.dat,vec.races.target.day,value=TRUE)
  vec.races.target.day <- gsub("courses/programme","fiche-course",vec.races.target.day)
  vec.races.target.day <- unlist(lapply(vec.races.target.day,get_details_races_reunion_letrot))
  
  aa = readHTMLTable(vec.races.target.day[1])[[1]]
  colnames(aa) = str_trim(gsub("\n","",colnames(aa)))
  aa$`Gains (€)` = as.matrix(aa$`Gains (€)`)
  aa$`Gains (€)` =  gsub("^[0]+", "",aa$`Gains (€)`) 
  aa$`Gains (€)` =  gsub("€", "",aa$`Gains (€)`) 
  aa$`Gains (€)` =  gsub(" ", "",aa$`Gains (€)`) 
  aa$`Gains (€)` = unlist(lapply(aa$`Gains (€)`,get_correct_revenue_letrot))

  
  
  
  
  page <- read_html(url.path)
  xpath.horse.page <- as.character("//*[@class=\"nowrap other-fixed-colomn\"]")
  horses.page <- page %>%
    html_nodes(xpath=xpath.horse.page) %>%
    html_nodes("a") %>%
    html_attr("href")
  return(horses.page)
}

