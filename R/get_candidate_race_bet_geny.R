get_candidate_race_bet_geny <- function (path_historical_races = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_historical_races.rds",
                                         url_target_race_start ="http://www.geny.com/reunions-courses-pmu?date=",
                                         number_distances_frequent  = 3,
                                         number_distances_similar   = 2,
                                         date_race_min ="2018-01-01",
                                         delta_acceptable_distance = 200,
                                         path_output_file = "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/output/candidates/"){

  target_date <- as.character(Sys.Date())
  url_path_current_date <- paste(url_target_race_start,target_date,"?",sep="")
  vec_races_current_date <- rev(get_race_current_date_geny(url_path_date=url_path_current_date))

  mat_perf_all_races <- readRDS(path_historical_races)
  mat_perf_all_races$Date <- unlist(lapply(mat_perf_all_races$Race,function(x){substr(x,1,10)}))
  mat_perf_all_races$Date <- as.Date(mat_perf_all_races$Date)
  mat_perf_all_races <- mat_perf_all_races[nchar(as.character(mat_perf_all_races$Date))==10,]
  
  mat_indicators_candidate_races_compilation <- NULL
  
  for(url_target_race in vec_races_current_date)
  {
    infos_horse_new_race <- get_details_race_geny(url_target_race)
    race_horses <- unique(as.vector(infos_horse_new_race$Cheval))
    if(length(race_horses)<2)
    {
      message('This function requires at 2 least 2 horses')
    }
    
  nb_horses_unknow <- length(setdiff(race_horses,base::unique(mat_perf_all_races[,'Cheval'])))
  if(nb_horses_unknow>0)
  {
    horses_unknow <- setdiff(race_horses,base::unique(mat_perf_all_races[,'Cheval']))
    message(paste(paste(setdiff(race_horses,base::unique(mat_perf_all_races[,'Cheval'])),collapse = ",")," not present in knowledgebase"))
  }
    
  if(nb_horses_unknow!=length(race_horses))
  {
    race_distance <- min(as.numeric(infos_horse_new_race[,'Distance']))
    if(class(race_distance)!='numeric')
    {
      stop('This function requires that the race distace must be given and should be numeric')
    }
      
    race_discipline <- as.vector(unique(infos_horse_new_race[,'Discipline']))
    race_date  <- as.Date(unique(as.vector(infos_horse_new_race$Date)),origin = "1970-01-01")
    race_heure <- unique(as.vector(infos_horse_new_race$Heure))
    race_lieu  <- unique(as.vector(infos_horse_new_race$Lieu))
    race_name  <- unique(as.vector(infos_horse_new_race$Course))
    race_name  <- gsub("\n","",race_name)
    race_prix  <- unique(as.vector(infos_horse_new_race$Prix))
      
    mat_perf_races_current_horse <- subset(mat_perf_all_races,Discipline==race_discipline)
    mat_perf_races_current_horse <- subset(mat_perf_races_current_horse, Date>=as.Date(date_race_min))
    number_location_overall   <- length(names(sort(table(mat_perf_races_current_horse$Lieu))))
    mat_perf_races_current_horse <- subset(mat_perf_races_current_horse,Cheval %in% race_horses)
    number_location_current   <- length(names(sort(table(mat_perf_races_current_horse$Lieu))))
    number_races_current   <- length(names(sort(table(mat_perf_races_current_horse$Course))))
    
    vec_cum_sum_locations <- cumsum(sort(table(mat_perf_races_current_horse$Lieu),decreasing = TRUE))/sum(table(mat_perf_races_current_horse$Lieu))
    
    most_frequent_location <- names(sort(table(mat_perf_races_current_horse$Lieu),decreasing = TRUE))[1]
    
    if (nrow(mat_perf_races_current_horse)!=0)
    {
      vec_races_curent_horses <- unique(mat_perf_races_current_horse$Race)
      mat_perf_races_complete_current_horse <- subset(mat_perf_all_races,Race %in% vec_races_curent_horses)
      mat_perf_races_complete_current_horse <- subset(mat_perf_races_complete_current_horse,Cheval %in% race_horses)
      mat_confrontation_horses_current <- base::table(as.vector(mat_perf_races_complete_current_horse$Race),as.vector(mat_perf_races_complete_current_horse$Cheval))
      col_names <- colnames(mat_confrontation_horses_current)
      row_names <- rownames(mat_confrontation_horses_current)
      mat_confrontation_horses_current <- as.matrix.data.frame(mat_confrontation_horses_current)
      colnames(mat_confrontation_horses_current) <- col_names
      rownames(mat_confrontation_horses_current) <- row_names
      mat_confrontation_horses_current <- t(mat_confrontation_horses_current) %*% mat_confrontation_horses_current
      diag(mat_confrontation_horses_current) <- 0
      mat_confrontation_horses_current <- mat_confrontation_horses_current[apply(mat_confrontation_horses_current,1,max)>0,apply(mat_confrontation_horses_current,1,max)>0]
      number_direct_comparison   <- NA
      number_possible_comparison <- NA
      percent_direct_comparison  <- NA
      percent_horses_compared <- NA
      number_races_horses <- NA
      if(sum(mat_confrontation_horses_current)>0)
      {
        mat_confrontation_horses_current <- ifelse(mat_confrontation_horses_current>0,1,0)
        number_direct_comparison <- (sum(mat_confrontation_horses_current))/2
        number_possible_comparison <- ((length(race_horses)-1)*length(race_horses))/2
        percent_direct_comparison <- round((number_direct_comparison/number_possible_comparison)*100,digits = 2)
        percent_horses_compared <- round((nrow(mat_confrontation_horses_current)/length(race_horses))*100,digits=2)
      }
      
      median_number_horse_compared <- median(apply(mat_confrontation_horses_current,1,function(x){sum(x,na.rm = TRUE)}),na.rm = TRUE)
      number_races_horses <- table(mat_perf_races_current_horse$Cheval)
      vec_distances_most_frequent <- sort(table(mat_perf_races_current_horse$Distance),decreasing = TRUE)
      freq_distances_race_horses <- sort(table(mat_perf_races_current_horse$Distance),decreasing = TRUE)
      freq_distances_race_horses <- as.numeric(names(freq_distances_race_horses))
      number_horse_frequent_distance <- length(names(table(subset(mat_perf_races_current_horse,Distance==names(vec_distances_most_frequent)[1])[,'Cheval'])))
      mat_indicators_candidate_race <- data.frame(Date=race_date,
                                                  Heure=race_heure,
                                                  Lieu=race_lieu,
                                                  Course=race_name,
                                                  Prix=race_prix,
                                                  Discipline=race_discipline,
                                                  Distance=race_distance,
                                                  Distance_01=freq_distances_race_horses[1],
                                                  Distance_02=freq_distances_race_horses[2],
                                                  Distance_03=freq_distances_race_horses[3],
                                                  Number_Races=nrow(mat_perf_races_current_horse),
                                                  Number_Hippodromes=as.numeric(which(vec_cum_sum_locations>0.8)[1]),
                                                  Ratio_Race_Location = round(number_location_current/number_races_current,digits=2),
                                                  Max_Races=max(number_races_horses),
                                                  Med_Races=median(number_races_horses),
                                                  Min_Races=min(number_races_horses),
                                                  Number_Horse=length(race_horses),Number_Horse_With_Comparison=nrow(mat_confrontation_horses_current),
                                                  Median_Number_Horse_Compared=median_number_horse_compared,
                                                  Number_Horse_Most_Frequent_Distance=number_horse_frequent_distance,
                                                  Percent_Horses_Compared=percent_horses_compared,
                                                  Percent_Pairwise_Comparison=percent_direct_comparison
                                                  )
      mat_indicators_candidate_race$Is_Reference_Hippodrome  <- ifelse(unique(mat_indicators_candidate_race$Lieu)==most_frequent_location,1,0)
      mat_indicators_candidate_race$Is_Reference_Distance    <- ifelse(abs(mat_indicators_candidate_race$Distance-mat_indicators_candidate_race$Distance_01)<=delta_acceptable_distance,1,0)
      mat_indicators_candidate_race$Link <- url_target_race
      mat_indicators_candidate_races_compilation <- rbind(mat_indicators_candidate_races_compilation,mat_indicators_candidate_race)
    }
  }
}
mat_indicators_candidate_races_compilation <- mat_indicators_candidate_races_compilation[order(mat_indicators_candidate_races_compilation$Number_Races,decreasing = TRUE),]
setwd(path_output_file)
xlsx::write.xlsx(mat_indicators_candidate_races_compilation,file=paste(gsub("/","-",race_date),".xlsx",sep=""),row.names=FALSE)     
return(mat_indicators_candidate_races_compilation) 
}


