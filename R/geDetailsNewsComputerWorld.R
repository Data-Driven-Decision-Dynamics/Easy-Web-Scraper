#' @return mat.offres a matrix with innfos on offers
#' @export
#' @importFrom xml2 read_html
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom rvest html_attr
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#' @examples
#' geDetailsNewsComputerWorld(url.path='http://www.computerworld.com/news/?start=1000')
#' @export
geDetailsNewsComputerWorld <- function(url.path='http://www.computerworld.com/news/?start=1000')
{
  mat.res <- NULL
  
  page <- read_html(url.path)
  links <- page %>%
    html_nodes('h3') %>%
    html_nodes('a') %>%
    html_attr("href")
  links <- paste('http://www.computerworld.com',links,sep='')

  text <- page %>%
    html_nodes('h3') %>%
    html_nodes('a') %>%
    html_text()

  page <- read_html(url.path)
  content <- page %>%
    html_nodes('h4') %>%
    html_text()
  
  dates <- unlist(lapply(links,getDateNewsComputerWorld))
  
  if(length(links)==length(text) & length(links)==length(content) & length(links)==length(dates))
  {
    mat.res <- data.frame(Link=links,Title=text,Summary=content)
    mat.res <- transform(mat.res,Date=unlist(lapply(as.vector(mat.res[,1]),getDateNewsComputerWorld)))
  }

  return(mat.res)
}








