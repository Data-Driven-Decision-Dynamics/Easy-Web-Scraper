#' Extract news details for a given page of the site https://www.infoworld.com
#' @param  url.path URL of the page to scrape
#' @return matrix.infos.news matrix with the detailed information of the news of the given page
#'
#' @export
#' @importFrom xml2 read_html
#' @importFrom rvest html_nodes
#' @importFrom rvest html_text
#' @importFrom stringr str_trim
#' @importFrom magrittr %>%
#'
#' @examples
#' getNewsContentTheGuardian('https://www.theguardian.com/global-development/2017/oct/24/the-terrible-truth-about-your-tin-of-italian-tomatoes')
#' @export
getNewsContentTheGuardian <- function(url.path='https://www.theguardian.com/business/2017/oct/10/deliveroo-workers-rights-uber')
{
  matrix.infos.news <- data.frame(Source='https://www.theguardian.com',Date=NA,Title=NA,Summary=NA)
  page <- tryCatch(read_html(url.path),error = function(e) {print(paste("URL does not seem to exist:", url.path));NULL})
  if(!is.null(page))
  {
    xpath.title <- "//*[@itemprop=\"headline\"]"
    news.title <- page %>% 
      html_nodes(xpath=xpath.title) %>% 
      html_text() %>% 
      unique()
    news.title <- gsub('\n','',news.title)
    if(length(news.title)==0) {
      news.title <- NA
    }
    if(length(news.title)>1) {
      news.title <- news.title [1]
    }
    
    xpath.summary <- "//*[@itemprop=\"description\"]"
    news.summary <- page %>%
      html_nodes(xpath=xpath.summary) %>% 
      html_attr("content") 
    if(length(news.summary)>1) {
      news.summary <- news.summary [1]
    }
    if(length(news.summary)==0) {
      news.summary <- NA
    }
    
    xpath.date <- "//*[@itemprop=\"datePublished\"]"
    news.date <- page %>%
      html_nodes(xpath=xpath.date) %>% 
      html_attr("datetime") 
    if(length(news.date)>1) {
      news.date <- news.date[1]
      news.date <- substr(news.date,1,10)
    } else {
      news.date <- substr(news.date,1,10)
    }
    if(length(news.date)==0) {
      news.date <- NA
    }
    
    matrix.infos.news <- data.frame(Source='https://www.theguardian.com',Date=news.date,Title=news.title,Summary=news.summary)
  }
return(matrix.infos.news)
}
