######################################## Loading required Packages ########################################
require(data.table)
require(plyr)
require(dplyr)
require(PlayerRatings)
require(parallel)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_input_data  <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/ranking_jockey_driver"
path_output_data <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.75))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Setting Output Path ############################
files_line_by_line <- dir(path=path_input_data)
setwd(path_input_data)
# mat_line_by_line  <- mclapply (X=files_line_by_line, FUN=fread, mc.cores=ceiling(host_nb_cores*0.75))
# mat_line_by_line  <- mclapply (X=files_line_by_line, FUN=fread, mc.cores=ceiling(host_nb_cores*0.75))
mat_line_by_line  <- lapply (files_line_by_line,read.csv)
names(mat_line_by_line) <- 1:length(mat_line_by_line)
mat_line_by_line <- rbindlist(mat_line_by_line,fill = TRUE)
mat_line_by_line <- as.data.frame(mat_line_by_line)
mat_line_by_line$Winner <- as.vector(mat_line_by_line$Winner)
mat_line_by_line$Looser <- as.vector(mat_line_by_line$Looser)
file_name <- "mat_line_by_line_jockey_driver.rating"
file_name <-  paste(file_name,".rds",sep="")
file_name <- paste(path_output_data,file_name,sep="/")
saveRDS(mat_line_by_line,file_name)
################################ Setting Output Path ############################
