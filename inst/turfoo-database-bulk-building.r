require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(data.table)
require(XML)
require(readr)
require(dplyr)
require(readxl)
require(xml2)

output.path <- "C:/Users/malick.paye/OneDrive - SODEXO/Personal/Futanke-Mbayar/output/data"

host.os  <- as.vector(Sys.info()['sysname'])
host.nb.cores <- parallel::detectCores()

if(host.os == 'Windows')
{
  cl.nb.cores <- parallel::makeCluster(host.nb.cores-1)
  doParallel::registerDoParallel(cl.nb.cores)
  
}

source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getLinksPageRacingTurfoo.R')
source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getLinksLandingPageRacingTurfoo.R')
source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getDetailsPageRacingTurfo.R')
source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getNumericReducKilomEcartTurfoo.R')
source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getDetailsGivenRacingTurfo.R')
source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getGoodTableTurfoo.R')
source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getResultsGivenRacingTurfo.R')
source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getCandidateRaceBetTurfoo.R')

range.years <- 2017
landing.pages.years <- paste0('http://www.turfoo.fr/programmes-courses/archives/',range.years)
vector.pages.scrape <- unlist(lapply(landing.pages.years,getLinksLandingPageRacingTurfoo))
vector.pages.scrape <- rev(vector.pages.scrape)

if(host.os == 'Windows')
{
  vector.pages.program.results.scrape <- unlist(lapply(vector.pages.scrape,getLinksPageRacingTurfoo))
} else {
  vector.pages.program.results.scrape <- foreach(i=1:length(vector.pages.scrape),.combine=c) %dopar%
  {
    getLinksPageRacingTurfoo(vector.pages.scrape[i])
  }
}
vector.pages.program.results.scrape <- rev(unique(vector.pages.program.results.scrape))
vector.pages.program.results.scrape <- rev(vector.pages.program.results.scrape)

setwd(output.path)

for(i in 1015:length(vector.pages.program.results.scrape)) 
{
  # url.path.res.current  <- vector.pages.program.results.scrape[i] 
  # url.path.prog.current <- sub('resultats-courses','programmes-courses',url.path.res.current)
  
  url.path.prog.current  <- vector.pages.program.results.scrape[i] 
  url.path.res.current <- sub('programmes-courses','resultats-courses',url.path.prog.current)
  
  
  infos.page.prog <- getDetailsGivenRacingTurfo(url.path.prog.current) 
  infos.page.res  <- getResultsGivenRacingTurfo(url.path.res.current) 
  infos.race.all  <- merge(infos.page.prog,infos.page.res,all.x=TRUE,all.y=TRUE,by.x="Cheval",by.y='Cheval')
  val.gains <- unlist(strsplit(as.vector(infos.page.prog[1,'Allocation']),','))
  val.gains <- unlist(strsplit(val.gains,' '))
  val.gains <- val.gains[val.gains!=""]
  if(length(grep('\\.',val.gains))>0)
  {
    val.gains <- as.numeric(str_trim(gsub('\\.','',val.gains)))
  }
  val.gains <- as.numeric(val.gains)
  infos.race.all <- infos.race.all[order(infos.race.all$Place),]
  infos.race.all$Gains <- 0
  if(length(val.gains)>0)
  {
    val.gains <- val.gains[1:min(nrow(infos.race.all),length(val.gains))]
    infos.race.all[1:length(val.gains),'Gains'] <- val.gains
  }
  write.csv(infos.race.all,file=paste(i,Sys.Date(),'matrix-historical.csv',sep='-'),sep=';')
  print(i)
}




files.results <- list.files(path =getwd(),pattern =  ".csv")
temp <- lapply(files.results, fread, sep=",")
matrix.performances.all.races <- rbindlist( temp,fill = TRUE)
rm(temp)
gc()
matrix.performances.all.races <- as.data.frame(matrix.performances.all.races)
matrix.performances.all.races <- matrix.performances.all.races[,-1]
rm(list=setdiff(ls(),"matrix.performances.all.races"))
matrix.performances.all.races$Date<- as.Date(matrix.performances.all.races$Date,format = "%d/%m/%Y")
label.column.keep <- c('Race','Date','Heure','Lieu','Course', 'Discipline','Distance','Terrain','Corde','Prix','Numero','Cheval','Musique','S.Age','Driver', 'Jockey','Entraineur','Poids','Reference','Place','Ecart','Reduction','Gains')
matrix.performances.all.races <- matrix.performances.all.races[,label.column.keep]
matrix.performances.all.races$Driver[matrix.performances.all.races$Discipline %in% c("Haies","Steeple","Cross Trot")] <- matrix.performances.all.races$Jockey[matrix.performances.all.races$Discipline %in% c("Haies","Steeple","Cross Trot")]
matrix.performances.all.races$ReductionFormat <- unlist(lapply(matrix.performances.all.races$Reduction,getNumericReducKilomEcartTurfoo))
matrix.performances.all.races$Reduction <- matrix.performances.all.races$ReductionFormat
setwd("C:/Users/malick.paye/OneDrive - SODEXO/Personal/Futanke-Mbayar/input")
write_csv(matrix.performances.all.races,"matrix-performances-all-races.csv")


# matrix.performances.all.races$EcartFormat <- unlist(lapply(matrix.performances.all.races$Ecart,getNumericReducKilomEcartTurfoo))
# matrix.performances.all.races$Ecart <- matrix.performances.all.races$EcartFormat





# for(i in 1:length(vector.pages.program.results.scrape[1:100])) 
# {
#   url.path.res.current <- vector.pages.program.results.scrape[i] 
#   url.path.prog.current <- sub('resultats-courses','programmes-courses',url.path.res.current)
#   
#   infos.page.prog <- getDetailsGivenRacingTurfo(url.path.prog.current) 
#   infos.page.res  <- getResultsGivenRacingTurfo(url.path.res.current) 
#   infos.race.all  <- merge(infos.page.prog,infos.page.res,all.x=TRUE,all.y=TRUE)
#   val.gains <- unlist(strsplit(as.vector(infos.page.prog[1,'Allocation']),','))
#   val.gains <- unlist(strsplit(val.gains,' '))
#   val.gains <- val.gains[val.gains!=""]
#   if(length(grep('\\.',val.gains))>0)
#   {
#     val.gains <- as.numeric(str_trim(gsub('\\.','',val.gains)))
#   }
#   val.gains <- as.numeric(val.gains)
#   infos.race.all <- infos.race.all[order(infos.race.all$Place),]
#   infos.race.all$Gains <- 0
#   
#   if(length(val.gains)>0)
#   {
#     infos.race.all$Gains[1:length(val.gains)] <- val.gains
#   }
#   write.csv(infos.race.all,file=paste(i,Sys.Date(),'matrix-historical.csv',sep='-'),sep=';')
# }





# setwd(output.path)
# files.results <- list.files(path =getwd(),pattern =  ".csv")
# temp <- lapply(files.results, fread, sep=",")
# matrix.performances.all.races <- rbindlist( temp,fill = TRUE )
# rm(temp)
# gc()
# matrix.performances.all.races <- matrix.performances.all.races[,1:100]
# matrix.performances.all.races <- as.data.frame(matrix.performances.all.races)
# matrix.performances.all.races <- unique(matrix.performances.all.races)
# matrix.performances.all.races$Date <- str_trim(matrix.performances.all.races$Date)
# matrix.performances.all.races$Date<- as.Date(matrix.performances.all.races$Date,format = "%d/%m/%Y")
# matrix.performances.all.races$Poids <- as.numeric(as.vector(matrix.performances.all.races$Poids))
# matrix.performances.all.races$EcartCorrected <- matrix.performances.all.races$Ecart
# matrix.performances.all.races$Year <- year(matrix.performances.all.races$Date)
# matrix.performances.all.races$Distance <- as.numeric(matrix.performances.all.races$Distance)
# matrix.performances.all.races$Placer <- ifelse(matrix.performances.all.races$Place<6,1,0)
# matrix.performances.all.races <- transform(matrix.performances.all.races,Race =paste(matrix.performances.all.races[,'Course'],matrix.performances.all.races[,'Lieu'],matrix.performances.all.races[,'Date'],sep='-'))
# colnames(matrix.performances.all.races)
# 
# matrix.performances.all.races$Reduction <- matrix.performances.all.races$Reduc
# matrix.performances.all.races <- matrix.performances.all.races[,c('Cheval','Poids','Race','Date', 'Year','Lieu','Prix','Discipline','Distance','Place','Ecart','Reduction','Gains')]
# 
# 
# 
# matrix.cotes.all.races <- matrix.performances.all.races[,grep('^X....',colnames(matrix.performances.all.races))]
# matrix.cotes.all.races <- matrix.cotes.all.races[,-1]
# max.nb.val.cotes <- max(apply(matrix.cotes.all.races,1,function(x){sum(!is.na(x))}))
# 
# getCotesMatrixMissingTurfoo <- function(x)
# {
#   val.cotes <- rep(NA,3)
#   id.cotes <- !is.na(x)
#   if(sum(id.cotes)>0)
#   {
#     val.cotes <- x[id.cotes]
#     if(length(val.cotes)<3)
#     {
#       val.cotes <- c(val.cotes,rep(NA,3-length(val.cotes)))
#     }
#   }
# 
# return(val.cotes)
# }
# 
# matrix.cotes.all.races.format <- t(apply(matrix.cotes.all.races,1,getCotesMatrixMissingTurfoo))
# colnames(matrix.cotes.all.races.format) <- c('Cote-1','Cote-2','Cote-3')
# matrix.performances.all.races <- matrix.performances.all.races[,-grep('^X....',colnames(matrix.performances.all.races))]
# matrix.performances.all.races <- cbind(matrix.performances.all.races,matrix.cotes.all.races.format)
# A FAIRE APRES LE FILTRE reduction <- unlist(lapply(matrix.performances.all.races$Reduc, getNumericReducKilomEcartTurfoo))
# matrix.performances.all.races <- transform(matrix.performances.all.races,Reduction=reduction)
# ecart.values.remove <- unique(as.vector(matrix.infos.ecarts[!is.na(matrix.infos.ecarts$Remove),"Remove"]))
# ecart.values.modify <- unique(as.vector(matrix.infos.ecarts[!is.na(matrix.infos.ecarts$Modify),"Ecart"]))
# matrix.performances.all.races$EcartCorrected <- gsub("\\(","",matrix.performances.all.races$EcartCorrected)
# matrix.performances.all.races$EcartCorrected <- gsub("\\)","",matrix.performances.all.races$EcartCorrected)
# matrix.performances.all.races$EcartCorrected[grep(',$',matrix.performances.all.races$EcartCorrected)] <- gsub(",","",matrix.performances.all.races$EcartCorrected[grep(',$',matrix.performances.all.races$EcartCorrected)])
# 
# for(ecart.item in ecart.values.remove)
# {
#   matrix.performances.all.races$EcartCorrected <- gsub(ecart.item,"",matrix.performances.all.races$EcartCorrected)
# }
# 
# for(ecart.item in ecart.values.modify)
# {
#   ecart.modified <- matrix.infos.ecarts[matrix.infos.ecarts$Ecart==ecart.item,"Modify"]
#   ecart.modified <- as.vector(ecart.modified[!is.na(ecart.modified)])
#   matrix.performances.all.races$EcartCorrected[matrix.performances.all.races$EcartCorrected==ecart.item] <- ecart.modified
# }
# 
# matrix.performances.all.races$EcartCorrected[grep("1/2",matrix.performances.all.races$EcartCorrected)] <- unlist(lapply(gsub("1/2","0.5",matrix.performances.all.races$EcartCorrected[grep("1/2",matrix.performances.all.races$EcartCorrected)]),getEcartValuesTurfoo))
# matrix.performances.all.races$EcartCorrected[grep("1/4",matrix.performances.all.races$EcartCorrected)] <- unlist(lapply(gsub("1/4","0.25",matrix.performances.all.races$EcartCorrected[grep("1/4",matrix.performances.all.races$EcartCorrected)]),getEcartValuesTurfoo))
# matrix.performances.all.races$EcartCorrected[grep("3/4",matrix.performances.all.races$EcartCorrected)] <- unlist(lapply(gsub("3/4","0.75",matrix.performances.all.races$EcartCorrected[grep("3/4",matrix.performances.all.races$EcartCorrected)]),getEcartValuesTurfoo))
# matrix.performances.all.races$EcartCorrected <- as.numeric(matrix.performances.all.races$EcartCorrected)
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getHarmonicMeanMusiqueTurfoo.R')



# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getMeanMostRecentValues.R')
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getAgeBasedTimeValues.R')
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getMeanDistanceHorseDriver.R')
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getNumberPodiumTurfoo.R')
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getNumberFirstTurfoo.R')
# 
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getReductionBasedEcartPlatTurfoo.R')
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getEcartFormatPlatTurfoo.R')
# 
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/graphHorseWeightProfileTurfoo.R')
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/EasyMachineLearner/R/getNodesEdgesListFromMatrix.R')
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getAverageSpeedHorse.R')
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getGoodTableCanalTurf.R')
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getRealDistanceBasedWeightTurfoo.R')
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getDetailsGivenRacingCanalTurf.R')
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getResultsGivenRacingCanalTurf.R')
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/Easy-Web-Scraper/R/getHorseProfilePlateRaceTurfoo.R')
# source('C:/Users/malick.paye/OneDrive - SODEXO/Personal/DataDrivenDecisionDx/R/getMeanPairwiseDelta.R')


