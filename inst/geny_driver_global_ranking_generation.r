######################################## Loading required Packages ########################################
require(data.table)
require(plyr)
require(dplyr)
require(PlayerRatings)
require(EloRating)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_input_data <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed"
path_output_data <- path_input_data
################################ Setting Output Path ############################

################################ Setting Output Path ############################
file_line_by_line <- dir(path=path_input_data,pattern = "line_by_line_jockey")
mat_line_by_line <- readRDS(paste(path_input_data,file_line_by_line,sep="/"))
mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Discipline),]
vec_disciplines <- as.vector(unique(mat_line_by_line$Discipline))
for(discipline in rev(vec_disciplines))
{
  mat_line_by_line_rating <- subset(mat_line_by_line,Discipline %in% discipline)
  mat_line_by_line_rating$Date <- as.Date(mat_line_by_line_rating$Date)
  mat_line_by_line_rating  <- transform(mat_line_by_line_rating,Delay=Sys.Date()-Date)
  mat_line_by_line_rating <-  transform(mat_line_by_line_rating,WeekTempory=mat_line_by_line_rating$Delay/7)
  vec.values.times <- as.numeric(as.vector(mat_line_by_line_rating$WeekTempory))
  mat_line_by_line_rating$Week <- vec.values.times
  mat_line_by_line_rating <-  transform(mat_line_by_line_rating,Score=1)
  mat_line_by_line_rating  <- mat_line_by_line_rating[order(mat_line_by_line_rating$Date),]
  mat_line_by_line_rating  <- unique(mat_line_by_line_rating)
  mat_line_by_line_rating <- mat_line_by_line_rating [,c("Week","Winner","Looser","Score")]
  mat_line_by_line_rating <- mat_line_by_line_rating[complete.cases(mat_line_by_line_rating),]
  mat_line_by_line_rating$Winner <- as.vector(mat_line_by_line_rating$Winner)
  mat_line_by_line_rating$Looser <- as.vector(mat_line_by_line_rating$Looser)
  infos_res_glicko_golbal <-  PlayerRatings::glicko(mat_line_by_line_rating,history=TRUE)
  mat_res_glicko_golbal <- infos_res_glicko_golbal$ratings
  rm(infos_res_glicko_golbal)
  gc()
  mat_res_glicko_golbal <- mat_res_glicko_golbal[,c("Player","Rating","Deviation","Games","Win","Loss")]
  colnames(mat_res_glicko_golbal) <- sub("Player","Cheval",colnames(mat_res_glicko_golbal))
  colnames(mat_res_glicko_golbal) <- sub("Rating","Glicko",colnames(mat_res_glicko_golbal))
  mat_res_glicko_golbal <- transform(mat_res_glicko_golbal,Success=(Win/Games)*100)
  setwd(path_output_data)
  saveRDS(mat_res_glicko_golbal,paste(paste("mat_ranking_driver_",tolower(gsub(" ","-",gsub("é","e",discipline))),sep=""),".rds",sep=""))
  paste(discipline,"processed")
}
