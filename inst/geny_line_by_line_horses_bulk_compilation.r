######################################## Loading required Packages ########################################
require(data.table)
require(plyr)
require(dplyr)
require(PlayerRatings)
require(parallel)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_input_data  <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/ranking_horses"
path_output_data <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.75))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Setting Output Path ############################
files_line_by_line <- dir(path=path_input_data)
setwd(path_input_data)
# mat_line_by_line  <- mclapply (X=files_line_by_line, FUN=fread, mc.cores=ceiling(host_nb_cores*0.75))
# mat_line_by_line  <- mclapply (X=files_line_by_line, FUN=fread, mc.cores=ceiling(host_nb_cores*0.75))
mat_infos_files  <- lapply (files_line_by_line,file.info)
names(mat_infos_files) <- 1:length(mat_infos_files)
mat_infos_files <- rbindlist(mat_infos_files,fill = TRUE)
mat_infos_files <- as.data.frame(mat_infos_files)

my_read_files <- function(x) {
  res <- NULL
  infos_file <- file.info(x)
  if(infos_file[,'size']>0) {
    res <- read.csv(x)
  }
  return(res)
}

mat_line_by_line  <- lapply (files_line_by_line,my_read_files)
names(mat_line_by_line) <- 1:length(mat_line_by_line)
mat_line_by_line <- rbindlist(mat_line_by_line,fill = TRUE)
mat_line_by_line <- as.data.frame(mat_line_by_line)
mat_line_by_line$Winner <- as.vector(mat_line_by_line$Winner)
mat_line_by_line$Looser <- as.vector(mat_line_by_line$Looser)
mat_line_by_line$Date <- as.Date(mat_line_by_line$Date)
file_name <- "mat_line_by_line_rating"
file_name <-  paste(file_name,".rds",sep="")
file_name <- paste(path_output_data,file_name,sep="/")
saveRDS(mat_line_by_line,file_name)
################################ Setting Output Path ############################
