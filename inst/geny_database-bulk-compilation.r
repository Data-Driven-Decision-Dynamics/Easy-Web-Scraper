################################ Loading required packages ################################
require(readr)
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(plyr)
require(dplyr)
require(xml2)
require(doSNOW)
require(doParallel)
require(curl)
require(data.table)
require(parallel)
require(magrittr)
################################ Loading required packages ################################

################################ Setting Output Path ############################
path_input_data  <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/performances"
path_output_data <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.75))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Parallelization Management ############################
vec_availables_files <- dir(path_input_data)
setwd(path_input_data)
list_historical_races  <- lapply (vec_availables_files,function(x) {return(fread(x,sep=","))})
names(list_historical_races) <- 1:length(list_historical_races)
mat_historical_races <- rbindlist(list_historical_races,fill = TRUE)
mat_historical_races <- as.data.frame(mat_historical_races)
vec_name_races <- paste(mat_historical_races$Date,mat_historical_races$Heure,mat_historical_races$Lieu,mat_historical_races$Course,sep="-")
mat_historical_races <- transform(mat_historical_races,Race=vec_name_races)
mat_historical_races <- unique(mat_historical_races)
mat_historical_races <- mat_historical_races[,-grep("\\.y",colnames(mat_historical_races))]
colnames(mat_historical_races) <- gsub("\\.x","",colnames(mat_historical_races))
for(i in 1:nrow(mat_historical_races))
{
  if(!is.na(mat_historical_races[i,"Gains"]) & !is.na(mat_historical_races[i,"Prix"]) & mat_historical_races[i,"Gains"]>mat_historical_races[i,"Prix"]) {
    mat_historical_races[i,"Gains"] <- mat_historical_races[i,"Gains"]/1000
    print(i)
  }
}
mat_historical_races <- mat_historical_races[!is.na(mat_historical_races$Prix),]
setwd(path_output_data)
saveRDS(mat_historical_races,"mat_historical_races.rds")
################################ Parallelization Management ############################
