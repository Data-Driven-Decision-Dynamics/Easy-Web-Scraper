################################ Loading required packages ################################
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(plyr)
require(dplyr)
require(xml2)
require(doSNOW)
require(doParallel)
require(curl)
require(doMC)
require(data.table)
################################ Loading required packages ################################

################################ Loading some needed functions ################################
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_results_race_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_table_results_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_race_given_date_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_details_race_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_table_details_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/convert_numeric_values_reduc_ecart_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_amount_win_horse_given_race_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_race_allocation_from_details.R')
################################ Loading some needed functions ############################

################################ Setting Output Path ############################
path_output_data <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/performances"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.75))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Generate and store data ############################
seq_date <- seq(as.Date("1998-01-01"), Sys.Date(), by = "day")
vec_url_date_scrape   <- paste("http://www.geny.com/reunions-courses-pmu?date=",seq_date,sep="")
vec_url_date_scrape <- rev(vec_url_date_scrape )
res_races_scrape <- foreach(i=1:length(vec_url_date_scrape),.combine=rbind.fill) %dopar% {
  
  require(rvest)
  require(stringr)
  require(curl)
  
  vec_race_current_date <- get_race_given_date_geny(vec_url_date_scrape[i])
  for(j in 1:length(vec_race_current_date))
  {
    url_path_res_current <- vec_race_current_date[j]
    url_path_pro_current <- sub('arrivee-et-rapports-pmu','partants-pmu',url_path_res_current)
    
    mat_details_race <- get_details_race_geny(url_path_pro_current)
    mat_results_race <- get_results_race_geny(url_path_res_current)
    vec_horses_common <- intersect(mat_details_race$Cheval,mat_results_race$Cheval)
    if(length(vec_horses_common)>0) {
      mat_infos_race <- merge(mat_details_race,mat_results_race,by.x="Cheval",by.y="Cheval")
      mat_infos_race$Reduction <- NA
      if("Ecart" %in% colnames(mat_infos_race)) {
        mat_infos_race$Reduction <- unlist(lapply(mat_infos_race$Ecart, convert_numeric_values_reduc_ecart_geny))
      }
    }
    
    mat_infos_race$Prix <- get_race_allocation_from_details(mat_infos_race$Details)
    mat_infos_race <- get_amount_win_horse_given_race_geny(mat_perf_race_complete=mat_infos_race)
    
    mat_infos_race$Prix[mat_infos_race$Prix==1451164763] <- NA
    mat_infos_race$Prix[mat_infos_race$Prix==1630544958] <- NA
    mat_infos_race$Prix[mat_infos_race$Prix==193250002] <- 52000
    mat_infos_race$Prix[mat_infos_race$Prix==175000000] <- 175000
    mat_infos_race$Prix[mat_infos_race$Prix==255000000] <- 255000
    mat_infos_race$Prix[mat_infos_race$Prix==340000000] <- 340000
    mat_infos_race$Prix[mat_infos_race$Prix==425000000] <- 425000
    mat_infos_race$Prix[mat_infos_race$Prix==495000000] <- 495000
    mat_infos_race$Prix[mat_infos_race$Prix==528000000] <- 528000
    mat_infos_race$Prix[mat_infos_race$Prix==550000000] <- 550000
    mat_infos_race$Prix[mat_infos_race$Prix==440000000] <- 440000
    
    setwd(path_output_data)
    file_name_output <- gsub("http://www.geny.com/partants-pmu/","infos-races-",url_path_pro_current)
    file_name_output <- paste(i,file_name_output,sep="_")
    file_name_output <- paste(file_name_output,".csv",sep="")
    file_name_output <- paste(path_output_data,file_name_output,sep="/")
    write.csv(mat_infos_race,file=file_name_output,row.names = FALSE)
  }
}
################################ Generate and store data ############################

