######################################## Loading required Packages ########################################
require(doSNOW)
require(doParallel)
require(foreach)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_input_data <-"/userhomes/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/performances"
path_output_data <- "/userhomes/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/perfs-gains/"
################################### Setting Output Path ############################

################################ Loading some needed functions ################################
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_amount_win_horse_given_race_geny.R')
################################ Loading some needed functions ################################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.75))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Get list of files to process ############################
vec_files_to_process <- dir(path_input_data)
################################ Get list of files to process ############################

################################ Generate and store data ############################
mat_res_perfs_earnings <- foreach(i=1:length(vec_files_to_process),.combine=rbind.fill) %dopar% {
  mat_perf_current_race <- read.csv(paste(path_input_data,vec_files_to_process[i],sep="/"))

  depart = NA
  corde = NA
  
  mat_perf_current_race$Depart <- NA
  mat_perf_current_race$Corde  <- NA
  mat_perf_current_race$Recul <- 0
  
  current_discipline <- as.vector(unique(mat_perf_current_race$Discipline))
  vec_details  <- unlist(strsplit(unique(as.vector(mat_perf_current_race$Details)),"_"))
  infos_corde  <- grep("corde",vec_details,ignore.case = TRUE,value=TRUE)
  infos_depart <- grep("Départ",vec_details,ignore.case = TRUE,value=TRUE)
  test_corde_gauche <- grep("gauche",infos_corde)
  test_corde_droite <- grep("droite",infos_corde)
  test_depart_autostart <- grep("autostart",infos_depart,ignore.case = TRUE)
  test_depart_volt <- grep("volt",infos_depart,ignore.case = TRUE)
  
  if(current_discipline=="Trot Attelé") {
    if(sum(test_corde_gauche)>0) {
      corde <- "Gauche"
    } else {
      corde <- "Droite"
    }
    
    if(sum(test_depart_autostart)>0) {
      depart <- "Auto-Start"
    } else {
      depart <- "Volte"
    }
    
    mat_perf_current_race$Depart <- depart
    mat_perf_current_race$Corde  <- corde
    
    current_distance_min <- min(unique(mat_perf_current_race$Distance))
    idx_horses_recul <- mat_perf_current_race$Distance>current_distance_min
    if(sum(idx_horses_recul)>0) {
      mat_perf_current_race$Recul[idx_horses_recul] <- diff(range(unique(mat_perf_current_race$Distance)))
    }
    
    mat_perf_current_race$Ferrage <- as.vector(mat_perf_current_race$Ferrage)
    idx_horses_fer <- is.na(mat_perf_current_race$Ferrage)
    if(sum(idx_horses_fer)>0) {
      mat_perf_current_race$Ferrage[idx_horses_fer] <- "D0"
    }
  }

  mat_perf_current_race <- get_amount_win_horse_given_race_geny(mat_perf_race_complete=mat_perf_current_race)
  write.csv(mat_perf_current_race,file=paste(path_output_data,vec_files_to_process[i],sep="/"),row.names = FALSE)
}
################################ Generate and store data ############################



