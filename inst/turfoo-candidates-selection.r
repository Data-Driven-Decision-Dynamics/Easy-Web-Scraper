################################ Loading required packages ################################
library("rlang", lib.loc="~/R/x86_64-pc-linux-gnu-library/3.3")
require(plyr)
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(data.table)
require(XML)
require(readr)
require(dplyr)
require(readxl)
require(xml2)
require(doSNOW)
require(doParallel)
require(xlsx)
require(plotly)
require(igraph)
require(RColorBrewer)
require(curl)
require(reshape2)
require(PlayerRatings)
################################ Loading required packages ################################

################################ Loading some needed functions ################################
source('/userhomes/mpaye/Easy-Web-Scraper/R/getLinksPageRacingTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getLinksLandingPageRacingTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getDetailsPageRacingTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getNumericReducKilomEcartTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getDetailsGivenRacingTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getGoodTableTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getResultsGivenRacingTurfo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getCandidateRaceBetTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getNumberPodiumTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getAverageSpeedHorse.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getEcartFormatPlatTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getReductionBasedEcartPlatTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_candidate_race_bet_turfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_horse_score_racing_turfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_line_by_line_analysis_turfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/graph_matrix_bipartite_network.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_line_by_line_race_turfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_horse_ranking_score_turfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_cumul_gains_yearly.R')
# source('/userhomes/mpaye/Easy-Web-Scraper/R/get_horse_record_profile_turfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/graph_horse_profile_turfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_ranking_horse_driver_coach_score_turfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_path_horse_page_given_race_trot.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_horse_identity_card_trot.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_details_races_reunion_letrot.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_links_race_target_day_letrot.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_wins_profile_horse_geny.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/convert_cumulative_wins_individual_geny.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_path_horse_page_given_race_trot.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_details_race_geny.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_results_race_geny.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_table_results_geny.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_race_given_date_geny.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_details_race_geny.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_table_details_geny.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/convert_numeric_values_reduc_ecart_geny.R')
################################ Loading some needed functions ############################

################################ Setting Working Directory ################################
path_output_dir <- "/userhomes/mpaye/Futanke-Mbayar/02-ds/output"
################################ Setting Working Directory ################################

################################ Loading some needed functions ############################
date_current <- as.character(Sys.Date())
date_current <- gsub('-','',substring(date_current,3,nchar(date_current)))
matrix_infos_candidate_races <- get_candidate_race_bet_turfoo (url.path.race.infos.data=paste("http://www.turfoo.fr/programmes-courses",date_current,sep='/'))
matrix_infos_candidate_races <- matrix_infos_candidate_races[order(matrix_infos_candidate_races$NumberRacesFound,decreasing = TRUE),]
matrix_infos_candidate_races$Heure <- lubridate::hm(matrix_infos_candidate_races$Heure)
################################ Loading some needed functions ###############################

################################ Generating race informations ################################
vec_item_type <- c("Horse","Driver_Jockey","Entraineur")
list_res_ranking <- vector("list",length(vec_item_type ))
names(list_res_ranking) <- vec_item_type
# vec_races_selected <- subset(matrix_infos_candidate_races,Discipline=="Trot Attelé" & NumberRacesFound >99 & Lieu %in% c("Cagnes Sur Mer","Graignes","Les Sables D'olonne"))$Link
vec_races_selected <- subset(matrix_infos_candidate_races,Discipline=="Trot Attelé" & Lieu %in% c("Vincennes") & NumberRacesFound >99)$Link
for(race in vec_races_selected[1:length(vec_races_selected)]) {
  infos.horse.new.race <- getDetailsGivenRacingTurfoo(url.path = race)
  race.date <- as.vector(unique(infos.horse.new.race$Date))
  race.heure <- as.vector(unique(infos.horse.new.race$Heure))
  race.lieu <- as.vector(unique(infos.horse.new.race$Lieu))
  race.distance <- as.vector(unique(infos.horse.new.race$Distance))
  race.distance <- min(as.numeric(race.distance))
  race.discipline <- as.vector(unique(infos.horse.new.race[,'Discipline']))
  race.name <- as.vector(unique(infos.horse.new.race$Course))
  file_name <- gsub("/","-",gsub(" ", "-",paste(race.date,race.heure,race.lieu,race.discipline[1],race.distance,race.name,sep="_")))
  file_name <- gsub(":","-",file_name)
  file_name <- gsub("\\(","",file_name)
  file_name <- gsub("\\)","",file_name)
  file_name <- paste(file_name ,"Global",sep="_")
  file_name <- paste(path_output_dir,paste0(file_name,'.xlsx'),sep="/")
  file_name <- gsub("---","-",file_name)
  # for(item in c("Horse","Driver_Jockey","Entraineur"))
  # {
  #   list_res_ranking[[item]] <- get_ranking_horse_driver_coach_score_turfoo (url_path_new_race = race,item_ranker=item)
  #   print(item)
  # }
  # mat_res_rating_horse_driver_coach <- Reduce(function(x, y) merge(x, y, by.x="Numero",by.y="Numero",all=TRUE), list_res_ranking)
  # write.xlsx(mat_res_rating_horse_driver_coach, file_name, sheetName="Global_Ranking",col.names=TRUE, row.names=FALSE, append=FALSE, showNA=TRUE)
  
  # res_profil_geny <- get_wins_profile_horse_geny(url_path_race_geny ="http://www.geny.com/partants-pmu/2018-08-06-dieppe-pmu-prix-du-departement-de-seine-maritime_c992826",
  #                                url_path_race_trot ="https://www.letrot.com/stats/fiche-course/2018-08-06/7608/8/partants/tableau")
  # 
  # write.xlsx(res_profil_geny$performance, file_name, sheetName="Performance",col.names=TRUE, row.names=FALSE, append=TRUE, showNA=TRUE)
  # write.xlsx(res_profil_geny$affinity, file_name, sheetName="Affinity",col.names=TRUE, row.names=TRUE, append=TRUE, showNA=TRUE)

  graph_horse_profile_turfoo(url.path.new.race=race)
  get_horse_score_racing_turfoo(url.path.new.race=race)
  print(race)
}


# aa = get_path_horse_page_given_race_trot("https://www.letrot.com/stats/fiche-course/2018-08-06/5307/6/partants/tableau")$Page
# bb = lapply(as.vector(aa),get_horse_identity_card_trot)
# cc = rbindlist(bb)

