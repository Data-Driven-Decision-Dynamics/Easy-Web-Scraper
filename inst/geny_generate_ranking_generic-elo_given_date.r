######################################## Loading required Packages ########################################
require(data.table)
require(plyr)
require(dplyr)
require(PlayerRatings)
require(stringr)
require(curl)
require(rvest)
require(reshape2)
require(EloRating)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_input_ranking_data = "/userhomes/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed"
path_output_res_ranking = "/userhomes/mpaye/Futanke-Mbayar/02-ds/output/elo"
################################ Setting Output Path ############################

################################ Loading required functions ############################
source("/userhomes/mpaye/Easy-Web-Scraper/R/get_race_current_date_geny.R")
source("/userhomes/mpaye/Easy-Web-Scraper/R/get_details_race_geny.R")
source("/userhomes/mpaye/Easy-Web-Scraper/R/get_table_details_geny.R")
################################ Loading required functions ############################

################################ Extract races current day ############################
url_path_current_date <- paste("http://www.geny.com/reunions-courses-pmu/_d",as.character(Sys.Date()),"?",sep="")
vec_races_current_date <- get_race_current_date_geny(url_path_date=url_path_current_date)
################################ Extract races current day ############################

################################ Extract races current day ############################
list_infos_races_current_date <- lapply(vec_races_current_date, get_details_race_geny)
mat_infos_races_current_date <-  data.table::rbindlist(list_infos_races_current_date,fill = TRUE)
mat_infos_races_current_date <- as.data.frame(mat_infos_races_current_date)
vec_race_identifier <- paste(mat_infos_races_current_date$Date,mat_infos_races_current_date$Heure,mat_infos_races_current_date$Lieu,mat_infos_races_current_date$Course,sep="-")
mat_infos_races_current_date <- transform(mat_infos_races_current_date,Race =vec_race_identifier)
vec_column_add <- c("Numero","Cheval","Age","Gender","Distance","Poids","Jockey","Driver" ,"Ferrage","Musique",grep("cot",colnames(mat_infos_races_current_date),ignore.case = TRUE,value=TRUE))
mat_infos_races_current_date_joint <- mat_infos_races_current_date[,vec_column_add]
vec_races_current_date <- as.vector(unique(mat_infos_races_current_date$Race))
####################################### Extract races current day ############################

####################################### Setting Output Path ###################################
mat_line_by_line <- readRDS(paste(path_input_ranking_data,"mat_line_by_line_rating.rds",sep="/"))
mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Discipline),]

for(current_race in vec_races_current_date)
{
  current_candidates <- subset(mat_infos_races_current_date,Race==current_race)[,"Cheval"]
  current_candidates <- unlist(lapply(current_candidates,function(x){unlist(strsplit(x,"\r"))[1]}))
  current_candidates <- stringr::str_trim(current_candidates)
  
  current_discipline <- as.vector(unique(subset(mat_infos_races_current_date,Race==current_race)[,"Discipline"]))
  mat_line_by_line_rating <- subset(mat_line_by_line,Looser %in% current_candidates | Winner %in% current_candidates )
  
  vec_distance_found <- unique(as.vector(unlist(mat_line_by_line_rating[,c("Winner_Distance","Looser_Distance")])))
  res_clusters_distances <- kmeans(vec_distance_found,4)
  mat_cluster_distance <- data.frame(Distance=vec_distance_found,Cluster=res_clusters_distances$cluster)
  mat_cluster_distance <- mat_cluster_distance[order(mat_cluster_distance$Distance),]
  mat_cluster_distance$Group <- NA
  vec_value_cluster <- unique(mat_cluster_distance$Cluster)
  for(clus in 1:length(vec_value_cluster)){
    mat_cluster_distance$Group[mat_cluster_distance$Cluster==vec_value_cluster[clus]] <- clus
  }
  
  mat_ranges_cluster_distance <- NULL
  for(clus in 1:length(vec_value_cluster)) {
    vec_ranges_distances <- sort(range(mat_cluster_distance[mat_cluster_distance$Cluster==vec_value_cluster[clus],"Distance"]))
    mat_ranges_cluster_distance_current <- data.frame(Speciality=clus,Lower=vec_ranges_distances[1],Upper=vec_ranges_distances[2])
    mat_ranges_cluster_distance <- rbind(mat_ranges_cluster_distance,mat_ranges_cluster_distance_current)
  }
  
  list_distances <- vector('list',length(unique(mat_ranges_cluster_distance$Speciality)))
  names(list_distances) <- unique(mat_ranges_cluster_distance$Speciality)
  
  list_res_rating_specific_compliled <- vector("list",length(sort(unique(mat_ranges_cluster_distance$Speciality))))
  names(list_res_rating_specific_compliled) <- sort(unique(mat_ranges_cluster_distance$Speciality))
  
  for (clus in mat_ranges_cluster_distance$Speciality)
  {
    ranges_distance <- sort(range(as.numeric(unlist(subset(mat_ranges_cluster_distance,Speciality==clus)[,c("Lower","Upper")]))))
    list_distances [[clus]] <- ranges_distance
    id_distance_cluster <- mat_line_by_line_rating[,'Winner_Distance']>=ranges_distance[1] & mat_line_by_line_rating[,'Looser_Distance']>=ranges_distance[1] & mat_line_by_line_rating[,'Winner_Distance']<ranges_distance[2] & mat_line_by_line_rating[,'Looser_Distance']<ranges_distance[2]
    
    mat_line_by_line_subset <- mat_line_by_line_rating[id_distance_cluster,]
    mat_line_by_line_subset$Date <- as.Date(mat_line_by_line_subset$Date)
    mat_line_by_line_subset <- subset(mat_line_by_line_subset,Discipline %in% current_discipline)
    mat_line_by_line_subset <- subset(mat_line_by_line_subset,Looser %in% current_candidates | Winner %in% current_candidates )
    if(nrow(mat_line_by_line_subset)>5){
      
      mat_line_by_line_subset_rating <- mat_line_by_line_subset[mat_line_by_line_subset$Winner!=mat_line_by_line_subset$Looser,]
      infos_res_specific_elo_golbal <- elo.seq(winner=mat_line_by_line_subset_rating$Winner, loser=mat_line_by_line_subset_rating$Looser, Date=mat_line_by_line_subset_rating$Date,runcheck=FALSE)
      
      mat_specific_elo_golbal <- data.frame(Cheval=names(extract.elo(infos_res_specific_elo_golbal)),Elo=as.numeric(extract.elo(infos_res_specific_elo_golbal)))
      mat_specific_elo_golbal <- subset(mat_specific_elo_golbal,Cheval %in% current_candidates)
      
      colnames(mat_specific_elo_golbal) <- c(colnames(mat_specific_elo_golbal)[1],paste(paste(ranges_distance,collapse ='-'),colnames(mat_specific_elo_golbal)[2],sep="_"))
      list_res_rating_specific_compliled[[clus]] <- mat_specific_elo_golbal
      
      mat_specific_elo_temporal_profile_golbal <- infos_res_specific_elo_golbal$mat[,intersect(colnames(infos_res_specific_elo_golbal$mat),current_candidates)]
      mat_specific_elo_temporal_profile_golbal <- as.data.frame(mat_specific_elo_temporal_profile_golbal)
      mat_specific_elo_temporal_profile_golbal$Date <- NA
      mat_specific_elo_temporal_profile_golbal$Date <- infos_res_specific_elo_golbal$truedates
      mat_specific_elo_temporal_profile_golbal$Year <- year(mat_specific_elo_temporal_profile_golbal$Date)
      mat_specific_elo_temporal_profile_golbal <- mat_specific_elo_temporal_profile_golbal[,setdiff(colnames(mat_specific_elo_temporal_profile_golbal),"Date")]
      
      my_median <- function(x){median(x,na.rm = TRUE)}

      mat_specific_elo_temporal_profile_golbal <- mat_specific_elo_temporal_profile_golbal %>%
        group_by(Year) %>% 
        summarise_all(funs(my_median)) %>% 
        as.data.frame()
      rownames(mat_specific_elo_temporal_profile_golbal) <- as.vector(mat_specific_elo_temporal_profile_golbal$Year)
      mat_specific_elo_temporal_profile_golbal <- mat_specific_elo_temporal_profile_golbal[,-1]
      mat_specific_elo_temporal_profile_golbal <- as.data.frame(t(mat_specific_elo_temporal_profile_golbal))
      mat_specific_elo_temporal_profile_golbal$Cheval <- NA
      mat_specific_elo_temporal_profile_golbal$Cheval <- as.vector(rownames(mat_specific_elo_temporal_profile_golbal))
      mat_specific_elo_temporal_profile_golbal <- mat_specific_elo_temporal_profile_golbal[,rev(colnames(mat_specific_elo_temporal_profile_golbal))[1:2]]
      rownames(mat_specific_elo_temporal_profile_golbal) <- NULL
      colnames(mat_specific_elo_temporal_profile_golbal)[2] <- paste(ranges_distance,collapse ="_")
      list_res_rating_specific_compliled[[clus]] <- mat_specific_elo_temporal_profile_golbal
    }
  }
  
  list_res_rating_specific_compliled <- list_res_rating_specific_compliled[lapply(list_res_rating_specific_compliled,length)>0]
  mat_res_rating_specific_compliled <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_res_rating_specific_compliled)
  
  if(nrow(mat_line_by_line_rating)>0)
  {
    mat_line_by_line_rating$Date <- as.Date(mat_line_by_line_rating$Date)
    mat_line_by_line_rating <- subset(mat_line_by_line_rating,Discipline %in% current_discipline)
    mat_line_by_line_rating  <- mat_line_by_line_rating[order(mat_line_by_line_rating$Date),]
    mat_line_by_line_rating  <- unique(mat_line_by_line_rating)
    mat_line_by_line_rating <- mat_line_by_line_rating [,c("Winner","Looser","Date")]
    mat_line_by_line_rating <- mat_line_by_line_rating[complete.cases(mat_line_by_line_rating),]
    mat_line_by_line_rating$Winner <- as.vector(mat_line_by_line_rating$Winner)
    mat_line_by_line_rating$Looser <- as.vector(mat_line_by_line_rating$Looser)
    mat_line_by_line_rating <- mat_line_by_line_rating[complete.cases(mat_line_by_line_rating),]
    mat_line_by_line_rating <- mat_line_by_line_rating[mat_line_by_line_rating$Winner!=mat_line_by_line_rating$Looser,]
    infos_res_generic_elo_golbal <- elo.seq(winner=mat_line_by_line_rating$Winner, loser=mat_line_by_line_rating$Looser, Date=mat_line_by_line_rating$Date,runcheck=FALSE)
    
    mat_generic_elo_golbal <- data.frame(Cheval=names(extract.elo(infos_res_generic_elo_golbal)),Elo=as.numeric(extract.elo(infos_res_generic_elo_golbal)))
    mat_generic_elo_golbal <- subset(mat_generic_elo_golbal,Cheval %in% current_candidates)
    mat_generic_elo_temporal_profile_golbal <- infos_res_generic_elo_golbal$mat[,intersect(colnames(infos_res_generic_elo_golbal$mat),current_candidates)]
    
    if(length(colnames(mat_generic_elo_temporal_profile_golbal))>1){
      mat_generic_elo_temporal_profile_golbal <- as.data.frame(mat_generic_elo_temporal_profile_golbal)
      mat_generic_elo_temporal_profile_golbal$Date <- NA
      mat_generic_elo_temporal_profile_golbal$Date <- infos_res_generic_elo_golbal$truedates
      mat_generic_elo_temporal_profile_golbal$Year <- year(mat_generic_elo_temporal_profile_golbal$Date)
      mat_generic_elo_temporal_profile_golbal <- mat_generic_elo_temporal_profile_golbal[,setdiff(colnames(mat_generic_elo_temporal_profile_golbal),"Date")]
      
      mat_generic_elo_temporal_profile_golbal <- mat_generic_elo_temporal_profile_golbal %>%
        group_by(Year) %>% 
        summarise_all(funs(my_median)) %>% 
        as.data.frame()
      rownames(mat_generic_elo_temporal_profile_golbal) <- as.vector(mat_generic_elo_temporal_profile_golbal$Year)
      mat_generic_elo_temporal_profile_golbal <- mat_generic_elo_temporal_profile_golbal[,-1]
      mat_generic_elo_temporal_profile_golbal <- as.data.frame(t(mat_generic_elo_temporal_profile_golbal))
      mat_generic_elo_temporal_profile_golbal$Cheval <- NA
      mat_generic_elo_temporal_profile_golbal$Cheval <- as.vector(rownames(mat_generic_elo_temporal_profile_golbal))
      mat_generic_elo_temporal_profile_golbal <- mat_generic_elo_temporal_profile_golbal[,rev(colnames(mat_generic_elo_temporal_profile_golbal))[1:2]]
      rownames(mat_generic_elo_temporal_profile_golbal) <- NULL
      colnames(mat_generic_elo_temporal_profile_golbal)[2] <- "Elo_Global"
    }
  }
  
  mat_res_rating_all_together <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list(A=mat_generic_elo_temporal_profile_golbal,B=mat_res_rating_specific_compliled))
}













# axis_limts <- range(mat_generic_elo_temporal_profile_golbal,na.rm = TRUE)
# 
# file_name_output <- gsub("é","e",current_race)
# file_name_output <- gsub("è","e",file_name_output)
# file_name_output <- gsub(" ","-",file_name_output)
# file_name_output <- gsub("\\(","",file_name_output)
# file_name_output <- gsub("\\)","",file_name_output)
# file_name_output <- paste(file_name_output,"Elo",sep="-")
# file_name_output <- paste(file_name_output,".tiff",sep="")
# 
# setwd(path_output_res_ranking)
# tiff(file_name_output,width = 700, height = 500)
# par(mar=c(4,2,2,2))
# if(length(current_candidates)==6){
#   vec.mfrow = c(2,3)
# } else if (length(current_candidates)==8) {
#   vec.mfrow = c(2,4)
# } else if(length(current_candidates)==9) {
#   vec.mfrow = c(3,3)
# } else if (length(current_candidates)==10) {
#   vec.mfrow = c(2,5)
# } else if (length(current_candidates)==12) {
#   vec.mfrow = c(3,4)
# } else if (length(current_candidates)==16) {
#   vec.mfrow = c(4,4)
# } else if (length(current_candidates)==17) {
#   vec.mfrow = c(3,6)
# } else if (length(current_candidates)==18) {
#   vec.mfrow = c(3,6)
# } else if (length(current_candidates)==13) {
#   vec.mfrow = c(3,5)
# } else if (length(current_candidates)==14) {
#   vec.mfrow = c(3,5)
# } else if (length(current_candidates)==15) {
#   vec.mfrow = c(3,5)
# } else if (length(current_candidates)==11) {
#   vec.mfrow = c(3,4)
# } else if (length(current_candidates)==4) {
#   vec.mfrow = c(2,2)
# } else if (length(current_candidates)==5) {
#   vec.mfrow = c(2,3)
# } else if(length(current_candidates)==7) {
#   vec.mfrow = c(2,4)
# } else if(length(current_candidates)==3) {
#   vec.mfrow = c(2,2)
# }
# par(mfrow=vec.mfrow)
# 
# for(horse in intersect(colnames(infos_res_generic_elo_golbal$mat),current_candidates))
# {
#   vec_abcisses_values <- rownames(mat_generic_elo_temporal_profile_golbal)[!is.na(mat_generic_elo_temporal_profile_golbal[,horse])]
#   vec_abcisses_values <- as.Date(as.numeric(vec_abcisses_values))
#   vec_scores_values <- mat_generic_elo_temporal_profile_golbal[,horse][!is.na(mat_generic_elo_temporal_profile_golbal[,horse])]
#   
#   if(length(vec_abcisses_values)>3 & length(vec_scores_values)>3){
#     mat_scores_smooth <- data.frame(x=1:length(vec_scores_values),y=vec_scores_values)
#     mod_loess <- loess(y~x,data=mat_scores_smooth)
#     plot(vec_scores_values,type='l',ylim = axis_limts,main=horse,lwd=0.75,col="blue")
#     grid(10,10)
#     lines(mod_loess$fitted,col="orange")
#     #plot(lowess(vec_scores_values,1:length(vec_scores_values))$y,main=horse)
#   }
# }
# dev.off()
# print(current_race)
# mat_res_rating_all_together <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list(mat_generic_elo_golbal,mat_res_rating_specific_compliled))
# write.csv(mat_res_rating_all_together,file=sub(".tiff",".csv",file_name_output))
# list_res_rating_specific_compliled <- list_res_rating_specific_compliled[lapply(list_res_rating_specific_compliled,length)>0]
# mat_res_rating_specific_compliled <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_res_rating_specific_compliled)
# mat_res_rating_all_together <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list(A=mat_res_rating_global,B=mat_res_rating_specific_compliled))
# mat_res_rating_all_together <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all.y=TRUE), list(A=mat_res_rating_all_together,B=mat_infos_races_current_date_joint[mat_infos_races_current_date_joint$Cheval %in% current_candidates,]))