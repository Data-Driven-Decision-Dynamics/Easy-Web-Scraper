######################################## Loading required Packages ########################################
require(RWeka)
require(randomForest)
require(bnlearn)
require(e1071)
require(foreach)
require(doSNOW)
require(doParallel)
require(data.table)
require(magrittr)
require(WriteXLS)
require(forcats)
require(readr)
require(stringi)
require(stringr)
require(foreach)
require(plyr)
require(dplyr)
require(parallel)
require(magrittr)
require(caret)
require(lime)
require(SuperLearner)
require(xlsx)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_input_data  <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/model-2.0"
path_output_learning_data <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/input"
path_output_model <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/output/models"
path_output_dashboard <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/output/dashboard"
path_cluster_distance_range = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_ranges_cluster_distance.rds"
source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_cluster_distance_race_type.R")
################################### Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.75))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Getting list of files to process ############################
vec_file_train_test   <- dir(path_input_data)
################################ Getting list of files to process ############################

################################ Reading cluster distance information ############################
mat_cluster_distance_range <- readRDS(path_cluster_distance_range)
################################ Reading cluster distance information ############################

################################ Generate training dat for each file ############################
mat_infos_files_compiled <- foreach(i=1:length(vec_file_train_test),.combine=rbind.fill) %dopar% {
  mat_infos_files_current <- file.info(paste(path_input_data,vec_file_train_test[i],sep="/"))
  mat_infos_files_current <- transform(mat_infos_files_current,File=vec_file_train_test[i])
}
vec_file_train_test <- as.vector(mat_infos_files_compiled[mat_infos_files_compiled$size>5,"File"])
setwd(path_input_data)
list_features_compiled  <- lapply (vec_file_train_test,function(x) {return(fread(x,sep=","))})
names(list_features_compiled) <- 1:length(list_features_compiled)
mat_features_compiled <- rbindlist(list_features_compiled,fill = TRUE)
mat_features_compiled <- as.data.frame(mat_features_compiled)
if(length(intersect("Cluster_Distance",colnames(mat_features_compiled)))==0) {
  mat_features_compiled$Cluster_Distance <-NULL
  for(i in 1:nrow(mat_features_compiled))
  {
    mat_cluster_distance_range_discipline <- mat_cluster_distance_range[mat_cluster_distance_range$Discipline==as.vector(unique(mat_features_compiled[i,"Discipline"])),]
    idx_cluster_distance_group <- mat_cluster_distance_range_discipline$Lower <= unlist(mat_features_compiled[i,"Distance"]) & mat_cluster_distance_range_discipline$Upper >= unlist(mat_features_compiled[i,"Distance"])
    mat_features_compiled[i,"Cluster_Distance"] <- paste(mat_cluster_distance_range_discipline[idx_cluster_distance_group,c("Lower","Upper")],collapse = "-")
    print(i)
  }
}
mat_features_compiled <- transform(mat_features_compiled,Class=paste(mat_features_compiled$Lieu,mat_features_compiled$Discipline,mat_features_compiled$Cluster_Distance,sep="-"))
saveRDS(mat_features_compiled,paste(path_output_learning_data,"mat_features_predictive_model.rds",sep="/"))
################################ Generate training dat for each file ############################

################################ Learning Model for each race class ###########################
caret_control <- trainControl(method="repeatedcv", number=10, repeats=3,classProbs = TRUE,summaryFunction = twoClassSummary)
seed <- 7
caret_metric <- "ROC"
set.seed(seed)
caret_mtry <- sqrt(ncol(mat_features_compiled))
caret_tunegrid_rf <- expand.grid(.mtry=caret_mtry)

if(!exists("mat_features_compiled")) {
  mat_features_compiled <- readRDS(paste(path_output_data,"mat_features_predictive_model.rds",sep="/"))
}

vec_number_items_race_class <- sort(table(mat_features_compiled$Class),decreasing = TRUE)
vec_race_class_trainable <- names(vec_number_items_race_class)[vec_number_items_race_class>=500]
mat_perf_compiled <- data.frame(Class=vec_race_class_trainable,ROC=0,SENS=0,SPEC=0)

for(current_race_class in vec_race_class_trainable)
{
  models_names <- c("RF")
  list_models <- vector("list",length(models_names))
  names(list_models) <- models_names
  current_model_name <- tolower(gsub(" ","-",gsub("è","e",gsub("é","e",current_race_class))))
  mat_features_compiled_current <- mat_features_compiled[mat_features_compiled$Class==current_race_class,]
  vec_missing_number <- apply(mat_features_compiled_current,2,function(x){return(sum(is.na(x)))})
  vec_features_remove <- names(vec_missing_number) [vec_missing_number==nrow(mat_features_compiled_current)]
  mat_features_compiled_current <- mat_features_compiled_current[,setdiff(colnames(mat_features_compiled_current),vec_features_remove)]
  mat_features_compiled_current[is.na(mat_features_compiled_current)] <- 0
  vec_features_remove <- c("Date","Lieu","Discipline","Distance","Cluster_Distance","Class","Cheval")
  mat_features_compiled_current <- mat_features_compiled_current[,setdiff(colnames(mat_features_compiled_current),vec_features_remove)]
  
  # mat_features_compiled_current_binary <- ifelse(mat_features_compiled_current=="Yes",1,0)
  mat_features_compiled_current_binary <- mat_features_compiled_current
  mat_features_compiled_current_binary <- as.data.frame(mat_features_compiled_current_binary)
  # mat_features_compiled_current_binary$Place <- ifelse(mat_features_compiled_current_binary$Place==1,"Yes","No")
  mat_features_compiled_current_binary$Place <- as.factor(mat_features_compiled_current_binary$Place)
  model_weights <- ifelse(mat_features_compiled_current_binary$Place == "Yes",
                          (1/table(mat_features_compiled_current_binary$Place)[1]) * 0.5,
                          (1/table(mat_features_compiled_current_binary$Place)[2]) * 0.5)
  nmin <- ceiling(min(table(mat_features_compiled_current_binary$Place))*0.8)
  
  # model_rf_fit <- train(Place ~ ., data = mat_features_compiled_current_binary, method="rf", metric=caret_metric, tuneGrid=caret_tunegrid_rf, trControl=caret_control,ntree=1000)
  model_rf_fit <- train(Place ~ ., data = mat_features_compiled_current_binary, method="rf", metric=caret_metric, tuneGrid=caret_tunegrid_rf, trControl=caret_control,sampsize = c(nmin,nmin),ntree=1000)
  mat_perf_compiled[mat_perf_compiled$Class ==current_race_class,2:4] <- apply(model_rf_fit$resample[,1:3],2,mean)
  
  print(current_race_class)
  print(apply(model_rf_fit$resample[,1:3],2,mean))
        
  list_models[["RF"]]  <- model_rf_fit
  
  for(model in names(list_models))
  {
    saveRDS(list_models[[model]],file=paste(path_output_model,paste(paste(current_model_name,model,sep="_"),".rds",sep=""),sep="/"))
  }
}

write.xlsx(mat_perf_compiled,file=paste(path_output_dashboard,"mat_model_performances_compiled_race_class.xlsx",sep="/"))


#   model_nb_fit <- train(Place ~ ., data = mat_features_compiled_current_binary, method="nb", trControl=caret_control, metric=caret_control)
#   # model_lmt_current <- LMT(Place ~ ., data = mat_features_compiled_current_binary)
#   model_logistic_fit <- train(Place ~ ., data = mat_features_compiled_current_binary, method="glm", family="binomial", metric="Accuracy", trControl=caret_control)
#   model_rf_fit <- train(Place ~ ., data = mat_features_compiled_current_binary, method="rf", metric="Accuracy", tuneGrid=caret_tunegrid_rf, trControl=caret_control,sampsize = c(nmin,nmin),ntree=1000)
#   model_lmt_fit <- train(Place ~ ., data = mat_features_compiled_current_binary, method="LMT", metric=caret_metric,trControl=caret_control)
# for(current_race_class in vec_race_class_trainable)
# {
#   models_names <- c("RF","NB","LMT","LG")
#   list_models <- vector("list",length(models_names))
#   names(list_models) <- models_names
#   current_model_name <- tolower(gsub(" ","-",gsub("è","e",gsub("é","e",current_race_class))))
#   mat_features_compiled_current <- mat_features_compiled[mat_features_compiled$Class==current_race_class,]
#   vec_missing_number <- apply(mat_features_compiled_current,2,function(x){return(sum(is.na(x)))})
#   vec_features_remove <- names(vec_missing_number) [vec_missing_number==nrow(mat_features_compiled_current)]
#   mat_features_compiled_current <- mat_features_compiled_current[,setdiff(colnames(mat_features_compiled_current),vec_features_remove)]
#   mat_features_compiled_current[is.na(mat_features_compiled_current)] <- "No"
#   vec_features_remove <- c("Date","Lieu","Discipline","Distance","Cluster_Distance","Class")
#   mat_features_compiled_current <- mat_features_compiled_current[,setdiff(colnames(mat_features_compiled_current),vec_features_remove)]
# 
#   mat_features_compiled_current_binary <- ifelse(mat_features_compiled_current=="Yes",1,0)
#   mat_features_compiled_current_binary <- as.data.frame(mat_features_compiled_current_binary)
#   mat_features_compiled_current_binary$Place <- ifelse(mat_features_compiled_current_binary$Place==1,"Yes","No")
#   mat_features_compiled_current_binary$Place <- as.factor(mat_features_compiled_current_binary$Place)
#   model_weights <- ifelse(mat_features_compiled_current_binary$Place == "Yes",
#                           (1/table(mat_features_compiled_current_binary$Place)[1]) * 0.5,
#                           (1/table(mat_features_compiled_current_binary$Place)[2]) * 0.5)
#   
#   caret_control <- trainControl(method="repeatedcv", number=10, repeats=1,classProbs = TRUE)
#   nmin <- ceiling(min(table(mat_features_compiled_current_binary$Place))*0.8)
#   
#   model_nb_fit <- train(Place ~ ., data = mat_features_compiled_current_binary, method="nb", trControl=caret_control, metric=caret_control)
#   # model_lmt_current <- LMT(Place ~ ., data = mat_features_compiled_current_binary)
#   model_logistic_fit <- train(Place ~ ., data = mat_features_compiled_current_binary, method="glm", family="binomial", metric="Accuracy", trControl=caret_control)
#   model_rf_fit <- train(Place ~ ., data = mat_features_compiled_current_binary, method="rf", metric="Accuracy", tuneGrid=caret_tunegrid_rf, trControl=caret_control,sampsize = c(nmin,nmin),ntree=1000)
#   model_lmt_fit <- train(Place ~ ., data = mat_features_compiled_current_binary, method="LMT", metric=caret_metric,trControl=caret_control)
#   
#   
#   list_models[["RF"]]  <- model_rf_fit
#   list_models[["NB"]]  <- model_nb_fit
#   list_models[["LMT"]] <- model_lmt_fit
#   list_models[["LG"]]  <- model_logistic_fit
#   for(model in names(list_models))
#   {
#     saveRDS(list_models[[model]],file=paste(path_output_model,paste(paste(current_model_name,model,sep="_"),".rds",sep=""),sep="/"))
#   }
# }
################################ Learning Model for each race class ############################

# idx_train <- sample(nrow(mat_features_compiled_current_binary),ceiling(nrow(mat_features_compiled_current_binary)*0.8))
# idx_test <- setdiff(1:nrow(mat_features_compiled_current_binary),idx_train)
# 
# xx_train = mat_features_compiled_current_binary[idx_train,]
# xx_test = mat_features_compiled_current_binary[idx_test,]
# model_rf_fit <- train(Place ~ ., data = xx_train, method="rf", metric=caret_metric, tuneGrid=caret_tunegrid_rf, trControl=caret_control,sampsize = c(1000,1000),ntree=1000)
# predict_rf_fit <- predict(model_rf_fit,xx_test,type="prob")
# predict_rf_fit$Class = xx_test$Place
# predRF <- predict(modelFitRF, newdata = validation)
# predGBM <- predict(modelFitGBM, newdata = validation)
# prefLDA <- predict(modelFitLDA, newdata = validation)
# predDF <- data.frame(predRF, predGBM, prefLDA, diagnosis = validation$diagnosis, stringsAsFactors = F)

