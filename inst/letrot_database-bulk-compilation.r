################################ Loading required packages ################################
require(readr)
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(plyr)
require(dplyr)
require(xml2)
require(doSNOW)
require(doParallel)
require(curl)
require(data.table)
################################ Loading required packages ################################


################################ Setting Output Path ############################
path_input_data  <- "/userhomes/mpaye/Futanke-Mbayar/01-db/lt/output/01-raw/performances"
# path_output_data <- "/userhomes/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
vec_availables_files <- dir(path_input_data)
setwd(path_input_data)
list_historical_races  <- lapply(vec_availables_files, fread, sep=",")
names(list_historical_races) <- 1:length(list_historical_races)
mat_historical_races <- rbindlist(list_historical_races,fill = TRUE)
mat_historical_races <- as.data.frame(mat_historical_races)



vec_name_races <- paste(mat_historical_races$Date,mat_historical_races$Heure,mat_historical_races$Lieu,mat_historical_races$Course,sep="-")
mat_historical_races <- transform(mat_historical_races,Race=vec_name_races)
mat_historical_races <- unique(mat_historical_races)
setwd(path_output_data)
saveRDS(mat_historical_races,"mat_historical_races.rds")
################################ Parallelization Management ############################
