######################################## Loading required Packages ########################################
require(doSNOW)
require(doParallel)
require(foreach)
require(data.table)
require(plyr)
require(dplyr)
require( magrittr)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_mat_historical_races = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_historical_races.rds"
path_input_itr_per_horse   = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/itr"
path_output_itr = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed"
################################ Setting Output Path ############################

################################ Loading required functions ############################
source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_high_level_global_kpi_geny.R")
################################ Loading required functions ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- ceiling(host_nb_cores*0.75)
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Data Extraction and storage ############################
mat_historical_races = readRDS(path_mat_historical_races)
vec_horses <- as.vector(unique(mat_historical_races$Cheval))
vec_horses <- vec_horses[!is.na(vec_horses)]
################################ Data Extraction and storage ############################

################################ Parallelization Management ############################
vec_availables_files <- list.files(path_input_itr_per_horse)
setwd(path_input_itr_per_horse)
list_itr_horses <- lapply(vec_availables_files, fread, sep=",")
names(list_itr_horses) <- 1:length(list_itr_horses)
mat_compilation_itr <- rbindlist(list_itr_horses,fill = TRUE)
mat_compilation_itr <- as.data.frame(mat_compilation_itr)
vec_ordered_column <- c(c("Cheval","Age","Gender","Year","Discipline"),setdiff(colnames(mat_compilation_itr),c("Cheval","Age","Gender","Year","Discipline")))
mat_compilation_itr <- mat_compilation_itr[,vec_ordered_column]
vec_ordered_column <- c(setdiff(colnames(mat_compilation_itr),grep('Number',colnames(mat_compilation_itr),value = TRUE)),grep('Number',colnames(mat_compilation_itr),value = TRUE))
mat_compilation_itr <- mat_compilation_itr[,vec_ordered_column]
mat_compilation_itr <- mat_compilation_itr[,names(sort(apply(mat_compilation_itr,2,function(x){sum(!is.na(x))}),decreasing = TRUE))]
mat_compilation_itr <- mat_compilation_itr[!is.na(mat_compilation_itr$Year),]
mat_compilation_itr <- mat_compilation_itr[,setdiff(colnames(mat_compilation_itr),"Year.1")]
setwd(path_output_itr)
saveRDS(mat_compilation_itr,"mat_compilation_itr.rds")
# write.csv(mat_compilation_itr,"mat_compilation_itr.csv",row.names=FALSE)
################################ Parallelization Management ############################



#  mat_compilation_itr %>% group_by("Age","Gender","Year") %>%
#  summarise(MEAN_GAIN_TOTAL=mean(GAIN_TOTAL,na.rm = TRUE),LOG_MEAN_GAIN_TOTAL=log(MEAN_GAIN_TOTAL))  
# 
# 
# 
# 
# mat_corrective_values_compilation_itr <- summarise(mat_corrective_values_compilation_itr,MEAN_GAIN_TOTAL=mean(GAIN_TOTAL,na.rm = TRUE))
# %>% summarise(MEAN_GAIN_TOTAL=mean(GAIN_TOTAL,na.rm = TRUE),LOG_MEAN_GAIN_TOTAL=log(MEAN_GAIN_TOTAL))
#   
# mat_compilation_itr <- mat_compilation_itr[!is.finite(mat_compilation_itr$ITR_UNCORRECTED),] 

