######################################## Loading required Packages ########################################
require(doSNOW)
require(doParallel)
require(foreach)
require(data.table)
require(plyr)
require(dplyr)
######################################## Loading required Packages ########################################

################################ Loading some needed functions ############################
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_line_by_line_driver_jockey_race_geny.R')
################################ Loading some needed functions ############################

################################ Setting Output Path ############################
path_line_by_line_output_tmp  =  "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/ranking_horses/"
path_mat_historical_line_by_line = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_line_by_line_rating.rds"
path_mat_historical_races <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_historical_races.rds"
################################ Setting Output Path ############################

################################ Data Extraction and storage ############################

# reading historical races results
if(!exists("mat_historical_races")) {
  mat_historical_races <- readRDS(path_mat_historical_races)
  mat_historical_races$Cheval <- as.vector(mat_historical_races$Cheval)
  mat_historical_races$Race <- as.vector(mat_historical_races$Race)
  mat_historical_races$Date <- as.Date(mat_historical_races$Date)
}

if(!exists("mat_historical_line_by_line")) {
  mat_historical_line_by_line <- readRDS(path_mat_historical_line_by_line)
  mat_historical_line_by_line$Date <- unlist(lapply(mat_historical_line_by_line$Race,function(x){substr(x,1,10)}))
  mat_historical_line_by_line$Date <- as.Date(mat_historical_line_by_line$Date)
  mat_historical_line_by_line <- mat_historical_line_by_line[nchar(as.character(mat_historical_line_by_line$Date))==10,]
}

mat_recent_races <- mat_historical_races[mat_historical_races$Date> max(mat_historical_line_by_line$Date,na.rm = TRUE),]
################################ Data Extraction and storage ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- ceiling(host_nb_cores*0.75)
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Generate and store data ############################
vec_races <- as.vector(unique(mat_recent_races$Race))
vec_races <- vec_races[!is.na(vec_races)]
# mat_line_by_line_recent_races <- foreach(i=1:length(vec_races),.combine=rbind.fill) %dopar% {
#   mat_recent_races_current <- mat_recent_races[mat_recent_races$Race==vec_races[i],]
#   mat_recent_races_current <- mat_recent_races_current [!is.na(mat_recent_races_current$Race),]
#   if(nrow(mat_recent_races_current)>1) {
#     get_line_by_line_driver_jockey_race_geny (mat_perf_race_complete=mat_recent_races_current,target_item="Horse",path_line_by_line_output = path_line_by_line_output_tmp)
#   }
# }
mat_line_by_line_recent_races = NULL
for(i in 1:length(vec_races)){
  mat_recent_races_current <- mat_recent_races[mat_recent_races$Race==vec_races[i],]
  mat_recent_races_current <- mat_recent_races_current [!is.na(mat_recent_races_current$Race),]
  if(nrow(mat_recent_races_current)>1) {
    mat_line_by_line_recent_races_tmp <- get_line_by_line_driver_jockey_race_geny (mat_perf_race_complete=mat_recent_races_current,target_item="Horse",path_line_by_line_output = path_line_by_line_output_tmp)
    mat_line_by_line_recent_races <- rbind(mat_line_by_line_recent_races,mat_line_by_line_recent_races_tmp)
    print(i)
  }
}
################################ Generate and store data ############################

################################ Setting Output Path ############################
mat_line_by_line_recent_races <- as.data.frame(mat_line_by_line_recent_races)
mat_line_by_line_recent_races$Winner <- as.vector(mat_line_by_line_recent_races$Winner)
mat_line_by_line_recent_races$Looser <- as.vector(mat_line_by_line_recent_races$Looser)
mat_line_by_line_recent_races <- mat_line_by_line_recent_races %>% distinct()
mat_historical_line_by_line <- rbind(mat_historical_line_by_line,mat_line_by_line_recent_races)
mat_historical_line_by_line <- mat_historical_line_by_line [mat_historical_line_by_line$Winner!=mat_historical_line_by_line$Looser,]
saveRDS(mat_historical_line_by_line,path_mat_historical_line_by_line)
################################ Setting Output Path ############################