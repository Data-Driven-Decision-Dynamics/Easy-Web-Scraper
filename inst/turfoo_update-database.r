################################ Loading required packages ################################
library("rlang", lib.loc="~/R/x86_64-pc-linux-gnu-library/3.3")
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(data.table)
require(XML)
require(readr)
require(dplyr)
require(readxl)
require(xml2)
require(doSNOW)
require(doParallel)
require(xlsx)
require(plotly)
require(plyr)
################################ Loading required packages ################################

################################ Loading some needed functions ################################
source('/userhomes/mpaye/Easy-Web-Scraper/R/getLinksPageRacingTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getLinksLandingPageRacingTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getDetailsPageRacingTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getNumericReducKilomEcartTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getDetailsGivenRacingTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getGoodTableTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getResultsGivenRacingTurfo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getCandidateRaceBetTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getNumberPodiumTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getAverageSpeedHorse.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getEcartFormatPlatTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/getReductionBasedEcartPlatTurfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_candidate_race_bet_turfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_horse_score_racing_turfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_line_by_line_analysis_turfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/graph_matrix_bipartite_network.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_line_by_line_race_turfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_horse_ranking_score_turfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_cumul_gains_yearly.R')
# source('/userhomes/mpaye/Easy-Web-Scraper/R/get_horse_record_profile_turfoo.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_date_from_url_race_turfoo.R')
################################ Loading some needed functions ############################

################################ Data Extraction and storage ############################

host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.75))
registerDoParallel(nb_cores_use)
registerDoSNOW(nb_cores_use)

path.tmp.files <-"/userhomes/mpaye/Futanke-Mbayar/01-db/tf/output/01-raw/performances/"
path.mat.historical.races="/userhomes/mpaye/Futanke-Mbayar/01-db/tf/output/02-process/mat.historical.races.rds"
mat.historical.races = readRDS(path.mat.historical.races)
mat.historical.races$Cheval <- as.vector(mat.historical.races$Cheval)
mat.historical.races$Race <- as.vector(mat.historical.races$Race)
mat.historical.races$Date <- as.Date(mat.historical.races$Date)
mat.historical.races <- transform(mat.historical.races,Delay=Sys.Date()-Date)
mat.historical.races$Discipline[mat.historical.races$Discipline== "Trot AttelÃ©"] <- "Trot Attelé" 
mat.historical.races$Discipline[mat.historical.races$Discipline== "Trot MontÃ©"]  <- "Trot Monté"

range.years <- 2018
landing.pages.years <- paste0('http://www.turfoo.fr/programmes-courses/archives/',range.years)
vector.pages.scrape <- unlist(lapply(landing.pages.years,getLinksLandingPageRacingTurfoo))
vector.pages.scrape <- rev(vector.pages.scrape)
vector.pages.program.results.scrape <- unlist(lapply(vector.pages.scrape,getLinksPageRacingTurfoo))
vector.pages.program.results.scrape <- rev(unique(vector.pages.program.results.scrape))
vector.pages.program.results.scrape <- rev(vector.pages.program.results.scrape)
vec.new.date <- unlist(lapply(vector.pages.program.results.scrape, get_date_from_url_race_turfoo))
vec.new.date <- as.Date(vec.new.date)
vector.pages.program.results.scrape.missing <- vector.pages.program.results.scrape[vec.new.date >max(mat.historical.races$Date,na.rm = TRUE)] 
vector.pages.program.results.scrape <- vector.pages.program.results.scrape.missing
vector.pages.program.results.scrape <- vector.pages.program.results.scrape[!is.na(vector.pages.program.results.scrape)]

list.res.new.races <- vector('list',length(vector.pages.program.results.scrape))
names(list.res.new.races) <- 1:length(vector.pages.program.results.scrape)

foreach(i=1:length(vector.pages.program.results.scrape))  %dopar% {
  url.path.res.current <- vector.pages.program.results.scrape[i]
  url.path.prog.current <- sub('resultats-courses','programmes-courses',url.path.res.current)
  infos.page.prog <- getDetailsGivenRacingTurfoo(url.path.prog.current)
  infos.page.res  <- getResultsGivenRacingTurfo(url.path.res.current)
  infos.race.all  <- merge(infos.page.prog,infos.page.res,all.x=TRUE,all.y=TRUE)
  if(nrow(infos.race.all)>0) {
    val.gains <- unlist(strsplit(as.vector(infos.page.prog[1,'Allocation']),','))
    val.gains <- unlist(strsplit(val.gains,' '))
    val.gains <- val.gains[val.gains!=""]
    if(length(grep('\\.',val.gains))>0)
    {
      val.gains <- as.numeric(str_trim(gsub('\\.','',val.gains)))
    }
    val.gains <- as.numeric(val.gains)
    infos.race.all <- infos.race.all[order(infos.race.all$Place),]
    infos.race.all$Gains <- 0
    
    if(length(val.gains)>0)
    {
      infos.race.all$Gains[1:min(nrow(infos.race.all),length(val.gains))] <- val.gains[1:min(nrow(infos.race.all),length(val.gains))]
    }
    setwd(path.tmp.files)
    write.csv(infos.race.all,file=paste(i,Sys.Date(),'matrix-historical.csv',sep='-'),sep=';')
    print(i)
  }
}

################################ Data Extraction and storage ############################

################################ specific to a given extraction ############################
convert_date_specific <- function(vec_date="16-07-20")
{
  vec_date_format <- NA
  if(!is.na(vec_date)) {
    vec_date_split  <- unlist(strsplit(vec_date,"/"))
    vec_date_format <- paste(vec_date_split[3],vec_date_split[2],vec_date_split[1],sep="-")
    return(vec_date_format)
  }
  return(vec_date_format)
}
################################ specific to a given extraction ############################

vec_availables_files <- dir(path.tmp.files )
setwd(path.tmp.files )
mat_new_files <- Reduce(rbind.fill, lapply(vec_availables_files, read.csv))
mat_new_files$Reduction <- gsub("\"","\"\"",mat_new_files$Reduction)
mat_new_files$ReductionFormat <- unlist(lapply(mat_new_files$Reduction, getNumericReducKilomEcartTurfoo))
mat_new_files$Reduction <- mat_new_files$ReductionFormat
mat_new_files$Date <- as.vector(mat_new_files$Date)
mat_new_files$Date <- unlist(lapply(mat_new_files$Date,convert_date_specific))
mat_new_files$Date <- as.Date(mat_new_files$Date)
mat_new_files <- transform(mat_new_files,Delay=Sys.Date()-Date)
mat_new_files <- mat_new_files[!is.na(mat_new_files$Date),]
mat_new_files <- transform(mat_new_files,"IdentifierHorseRace"=paste(mat_new_files$Date,mat_new_files$Race,mat_new_files$Lieu,sep="-"))
mat.historical.races <- rbind(mat.historical.races[,intersect(colnames(mat.historical.races),colnames(mat_new_files))],mat_new_files[,intersect(colnames(mat.historical.races),colnames(mat_new_files))])
saveRDS(mat.historical.races,file=path.mat.historical.races)


 