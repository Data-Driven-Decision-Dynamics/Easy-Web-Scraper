######################################## Loading required Packages ########################################
require(data.table)
require(plyr)
require(dplyr)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_input_data  <- "/userhomes/mpaye/Futanke-Mbayar/01-db/tf/output/01-raw/ranking"
path_output_data <- "/userhomes/mpaye/Futanke-Mbayar/01-db/tf/output/02-process"
################################ Setting Output Path ############################

################################ Setting Output Path ############################
vec_items_rankers <- c("^Driver_Jockey","^Entraineur","^Horse")
setwd(path_input_data)
for(item_ranker in vec_items_rankers ){
  files_line_by_line <- list.files(path=path_input_data,pattern = item_ranker)
  mat_line_by_line  <- lapply(files_line_by_line, fread, sep=",")
  names(mat_line_by_line) <- 1:length(mat_line_by_line)
  mat_line_by_line <- rbindlist(mat_line_by_line,fill = TRUE)
  mat_line_by_line <- as.data.frame(mat_line_by_line)
  file_name <- paste("mat_line_by_line_rating",sub("_","-",sub("\\^","",item_ranker)),sep="_")
  file_name <- paste(file_name,".rds",sep="")
  file_name <-paste(path_output_data,file_name,sep="/")
  saveRDS(mat_line_by_line,file_name)
}
################################ Setting Output Path ############################

################################ Setting Output Path ############################

mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Discipline),]
vec_disciplines <- as.vector(unique(mat_line_by_line$Discipline))
for(discipline in rev(vec_disciplines))
{
  mat_line_by_line_rating <- subset(mat_line_by_line,Discipline %in% discipline)
  mat_line_by_line_rating$Date <- as.Date(mat_line_by_line_rating$Date)
  mat_line_by_line_rating  <- transform(mat_line_by_line_rating,Delay=Sys.Date()-Date)
  mat_line_by_line_rating <-  transform(mat_line_by_line_rating,WeekTempory=mat_line_by_line_rating$Delay/7)
  vec.values.times <- as.numeric(as.vector(mat_line_by_line_rating$WeekTempory))
  mat_line_by_line_rating$Week <- vec.values.times
  mat_line_by_line_rating <-  transform(mat_line_by_line_rating,Score=1)
  mat_line_by_line_rating  <- mat_line_by_line_rating[order(mat_line_by_line_rating$Date),]
  mat_line_by_line_rating  <- unique(mat_line_by_line_rating)
  
  mat_line_by_line_rating <- mat_line_by_line_rating [,c("Week","Winner","Looser","Score")]
  mat_line_by_line_rating <- mat_line_by_line_rating[complete.cases(mat_line_by_line_rating),]
  
  infos_res_glicko_golbal <-  glicko(mat_line_by_line_rating,history=TRUE)
  mat_res_glicko_golbal <- infos_res_glicko_golbal$ratings
  # rm(infos_res_glicko_golbal)
  # gc()
  # mat_res_glicko_golbal <- mat_res_glicko_golbal[,c("Player","Rating")]
  # colnames(mat_res_glicko_golbal) <- sub("Player","Cheval",colnames(mat_res_glicko_golbal))
  # colnames(mat_res_glicko_golbal) <- sub("Rating","Glicko_Global",colnames(mat_res_glicko_golbal))
  # mat_res_glicko_golbal <- merge(mat_res_glicko_golbal,infos_horse_new_race[,c('Cheval','Numero')],by.x="Cheval",by.y="Cheval",all.y = TRUE)
  # mat_res_glicko_golbal <- mat_res_glicko_golbal[,-ncol(mat_res_glicko_golbal)]
  setwd(path_output_data)
  write.xlsx(mat_res_glicko_golbal,file=paste(gsub(" ","-", discipline),".xlsx",sep=""))
  # infos_res_steph_golbal <- steph(mat_line_by_line_rating,history=TRUE)
  # mat_res_steph_golbal <- infos_res_steph_golbal$ratings
  # rm(infos_res_steph_golbal )
  # gc()
  # mat_res_steph_golbal <- mat_res_steph_golbal[,c("Player","Rating")]
  # colnames(mat_res_steph_golbal) <- sub("Player","Cheval",colnames(mat_res_steph_golbal))
  # colnames(mat_res_steph_golbal) <- sub("Rating","Steph_Global",colnames(mat_res_steph_golbal))
  # mat_res_steph_golbal <- merge(mat_res_steph_golbal,infos_horse_new_race[,c('Cheval','Numero')],by.x="Cheval",by.y="Cheval",all.y = TRUE)
  # mat_res_steph_golbal <- mat_res_steph_golbal[,-ncol(mat_res_steph_golbal)]
  # 
  # infos_res_elo_golbal <- elo(mat_line_by_line_rating,history=TRUE)
  # mat_res_elo_golbal <- infos_res_elo_golbal$ratings
  # rm(infos_res_elo_golbal)
  # gc()
  # mat_res_elo_golbal <- mat_res_elo_golbal[,c("Player","Rating")]
  # colnames(mat_res_elo_golbal) <- sub("Player","Cheval",colnames(mat_res_elo_golbal))
  # colnames(mat_res_elo_golbal) <- sub("Rating","Elo_Global",colnames(mat_res_elo_golbal))
  # mat_res_elo_golbal <- merge(mat_res_elo_golbal,infos_horse_new_race[,c('Cheval','Numero')],by.x="Cheval",by.y="Cheval",all.y = TRUE)
  # mat_res_elo_golbal <- mat_res_elo_golbal[,-ncol(mat_res_elo_golbal)]
  # mat_res_rating_golbal <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list(mat_res_elo_golbal,mat_res_glicko_golbal,mat_res_steph_golbal))
  # mat_res_rating_golbal <- transform(mat_res_rating_golbal,EGS_GLOBAL=(Elo_Global+Glicko_Global+Steph_Global)/3)
  # mat_res_rating_golbal <- mat_res_rating_golbal [,c("Cheval","EGS_GLOBAL")]
  
}


for (clus in sort(unique(res_kmeans_distances$cluster)))
{
  ranges_distance <- sort(range(as.numeric(as.vector(subset(mat_cluster_distance,Speciality==clus)[,'Distance']))))
  list_distances [[clus]] <- ranges_distance
  id_distance_cluster <- mat_line_by_line[,'Winner_Distance']>=ranges_distance[1] & mat_line_by_line[,'Looser_Distance']>=ranges_distance[1] & mat_line_by_line[,'Winner_Distance']>=ranges_distance[2] & mat_line_by_line[,'Looser_Distance']>=ranges_distance[2] & mat_line_by_line[,'Discipline']==race_discipline
  mat_line_by_line_subset <- mat_line_by_line[id_distance_cluster,]
  
  mat_line_by_line_subset$Date <- as.Date(mat_line_by_line_subset$Date)
  mat_line_by_line_subset  <- transform(mat_line_by_line_subset,Delay=Sys.Date()-Date)
  mat_line_by_line_subset <-  transform(mat_line_by_line_subset,WeekTempory=mat_line_by_line_subset$Delay/7)
  vec.values.times <- as.numeric(as.vector(mat_line_by_line_subset$WeekTempory))
  mat_line_by_line_subset$Week <- vec.values.times
  mat_line_by_line_subset <-  transform(mat_line_by_line_subset,Score=1)
  mat_line_by_line_subset  <- mat_line_by_line_subset[order(mat_line_by_line_subset$Date),]
  mat_line_by_line_subset  <- unique(mat_line_by_line_subset)
  # 
  # mat_number_race_distance_range <- melt(mat_line_by_line_subset , id.vars=c("Cheval", "Race"), na.rm=TRUE)
  # mat_number_race_distance_range <- dcast(mat_number_race_distance_range, Cheval ~ Race,length,na.rm=TRUE)
  # rownames(mat_number_race_distance_range) <- as.vector(mat_number_race_distance_range[,1])
  # 
  # 
  
  if(length(mat_line_by_line_subset$Winner)>1 & length(mat_line_by_line_subset$Looser)>1 & length(unique(mat_line_by_line_subset$Week))>1)
  {
    mat_line_by_line_subset_rating <-  mat_line_by_line_subset[, c("Week","Winner","Looser","Score")]
    mat_line_by_line_subset_rating <- mat_line_by_line_subset_rating[!is.na(mat_line_by_line_subset_rating$Score),]
    mat_line_by_line_subset_rating <- mat_line_by_line_subset_rating[complete.cases(mat_line_by_line_subset_rating),]
    infos_res_glicko_specific <- glicko(mat_line_by_line_subset_rating,history=TRUE)
    # infos_res_glicko_specific <- glicko(mat.horses.direct.ranking.rating.subset,gamma=mat.horses.direct.ranking[id.distance.cluster,'gamma'],history=TRUE)
    mat_res_glicko_specific <- infos_res_glicko_specific$ratings
    rm(infos_res_glicko_specific)
    gc()
    mat_res_glicko_specific <- mat_res_glicko_specific[,c("Player","Rating")]
    colnames(mat_res_glicko_specific) <- sub("Player","Cheval",colnames(mat_res_glicko_specific))
    colnames(mat_res_glicko_specific) <- sub("Rating","Glicko",colnames(mat_res_glicko_specific))
    colnames(mat_res_glicko_specific) <- c(colnames(mat_res_glicko_specific)[1],paste(paste(ranges_distance,collapse ='-'),colnames(mat_res_glicko_specific)[2],sep="_"))
    mat_res_glicko_specific <- merge(mat_res_glicko_specific,infos_horse_new_race[,c('Cheval','Numero')],by.x="Cheval",by.y="Cheval",all.y = TRUE)
    mat_res_glicko_specific <- mat_res_glicko_specific[,-ncol(mat_res_glicko_specific)]
    
    infos_res_steph_specific <- steph(mat_line_by_line_subset_rating,history=TRUE)
    mat_res_steph_specific <- infos_res_steph_specific$ratings
    rm(infos_res_steph_specific)
    gc()
    mat_res_steph_specific <- mat_res_steph_specific[,c("Player","Rating")]
    colnames(mat_res_steph_specific) <- sub("Player","Cheval",colnames(mat_res_steph_specific))
    colnames(mat_res_steph_specific) <- sub("Rating","Steph",colnames(mat_res_steph_specific))
    colnames(mat_res_steph_specific) <- c(colnames(mat_res_steph_specific)[1],paste(paste(ranges_distance,collapse ='-'),colnames(mat_res_steph_specific)[2],sep="_"))
    mat_res_steph_specific <- merge(mat_res_steph_specific,infos_horse_new_race[,c('Cheval','Numero')],by.x="Cheval",by.y="Cheval",all.y = TRUE)
    mat_res_steph_specific <- mat_res_steph_specific[,-ncol(mat_res_steph_specific)]
    
    infos_res_elo_specific <- elo(mat_line_by_line_subset_rating,history=TRUE)
    mat_res_elo_specific <- infos_res_elo_specific$ratings
    rm(infos_res_elo_specific)
    gc()
    mat_res_elo_specific <- mat_res_elo_specific[,c("Player","Rating")]
    colnames(mat_res_elo_specific) <- sub("Player","Cheval",colnames(mat_res_elo_specific))
    colnames(mat_res_elo_specific) <- sub("Rating","Elo",colnames(mat_res_elo_specific))
    colnames(mat_res_elo_specific) <- c(colnames(mat_res_elo_specific)[1],paste(paste(ranges_distance,collapse ='-'),colnames(mat_res_elo_specific)[2],sep="_"))
    mat_res_elo_specific <- merge(mat_res_elo_specific,infos_horse_new_race[,c('Cheval','Numero')],by.x="Cheval",by.y="Cheval",all.y = TRUE)
    mat_res_elo_specific <- mat_res_elo_specific[,-ncol(mat_res_elo_specific)]
    mat_res_rating_specific <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list(mat_res_elo_specific,mat_res_glicko_specific,mat_res_steph_specific))
    mat_res_rating_specific <- transform(mat_res_rating_specific,EGS=(mat_res_rating_specific[,2]+mat_res_rating_specific[,3]+mat_res_rating_specific[,4])/3)
    colnames(mat_res_rating_specific) [ncol(mat_res_rating_specific)] <- paste("EGS",paste(ranges_distance,collapse ='-'),sep="_")
    mat_res_rating_specific <- mat_res_rating_specific[,c("Cheval",paste("EGS",paste(ranges_distance,collapse ='-'),sep="_"))]
    list_res_rating_specific_compliled [[clus]] <- mat_res_rating_specific
  }
}
list_res_rating_specific_compliled <- list_res_rating_specific_compliled[lapply(list_res_rating_specific_compliled,length)>0]
mat_res_rating_specific_compliled <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_res_rating_specific_compliled)
mat_res_rating_all_together <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list(A=mat_res_rating_golbal,B=mat_res_rating_specific_compliled))
mat_res_rating_all_together <- merge(mat_res_rating_all_together,infos_horse_new_race,by.x="Cheval",by.y="Cheval",all.y = TRUE)
vec_order_column <- c(c("Numero","Cheval","Cote.1","Cote.2","Cote.3","Distance","Musique"),setdiff(colnames(mat_res_rating_all_together),c("Numero","Cheval","Cote.1","Cote.2","Cote.3","Distance","Musique")))
vec_order_column <- intersect(vec_order_column,colnames(mat_res_rating_all_together))
mat_res_rating_all_together <- mat_res_rating_all_together[,vec_order_column]
colnames(mat_res_rating_all_together) <- gsub("\\.","_",colnames(mat_res_rating_all_together))



