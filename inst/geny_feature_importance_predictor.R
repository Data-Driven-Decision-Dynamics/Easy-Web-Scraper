######################################## Loading required Packages ########################################
require(doSNOW)
require(doParallel)
require(foreach)
require(dplyr)
require(data.table)
require(magrittr)
require(WriteXLS)
require(forcats)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_input_data  <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/model/"
# path_output_indiv_data <- "/userhomes/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/model/"
path_output_data <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed"
# "/userhomes/mpaye/Futanke-Mbayar/02-ds/output/model"
################################### Setting Output Path ############################

################################ Loading some needed functions ################################
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_feature_importance_predictor_geny.R')
################################ Loading some needed functions ################################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.75))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Extract list of files to process ############################
vec_files_to_process <- dir(path_input_data)
# vec_infos_files_to_process <- unlist(lapply(vec_files_to_process,function(x){unlist(strsplit(x,"races-"))[2]}))
# vec_dates_files_to_process <- gsub("-pmu.*","",vec_infos_files_to_process)
# vec_selected_files_to_process <- rev(vec_files_to_process[as.Date(vec_dates_files_to_process)>=as.Date("2018-01-01")])
################################ Extract list of files to process ############################

################################ Combine Feature importance ############################
# mat_res_importance_features <- foreach(i=1:length(vec_files_to_process),.combine=rbind.fill) %dopar% {
#   get_feature_importance_predictor_geny(file_name_target=vec_files_to_process[i])
# }

mat_res_importance_features <- NULL
for(i in 1:length(vec_files_to_process)) 
{
  mat_res_importance_features_current <- get_feature_importance_predictor_geny(file_name_target=vec_files_to_process[i])
  mat_res_importance_features <- rbind(mat_res_importance_features,mat_res_importance_features_current)
  print(i)
}

################################ Combine Feature importance ###########################

mat_average_score_feature_melt <- melt(mat_res_importance_features, id=c("Feature", "Distance"),measure.vars='Score',na.rm=TRUE)
mat_average_score_feature_cast <- reshape2::acast(mat_average_score_feature_melt, Feature ~ Distance ~ variable,median)
mat_average_score_feature <- as.data.frame(mat_average_score_feature_cast)
mat_average_score_feature <- as.matrix(mat_average_score_feature)
mat_average_score_feature[is.nan(mat_average_score_feature)] <- 0
vec_start_columnames <- toupper(unlist(lapply(colnames(mat_average_score_feature) ,function(x){unlist(strsplit(x,"\\."))[2]})))
vec_completion_columnames <- toupper(unlist(lapply(colnames(mat_average_score_feature) ,function(x){unlist(strsplit(x,"\\."))[1]})))
colnames(mat_average_score_feature) <- paste(vec_start_columnames,vec_completion_columnames,sep="_")
mat_average_score_feature <- transform(mat_average_score_feature,Cheval=rownames(mat_average_score_feature))
mat_average_score_feature <- mat_average_score_feature[,c("Cheval",setdiff(colnames(mat_average_score_feature),"Cheval"))]
WriteXLS(as.data.frame(mat_average_score_feature), ExcelFileName=paste(path_output_data,"mat_average_feature_importance.xls",sep="/"),SheetNames="1")
         
aa = mat_res_importance_features[mat_res_importance_features$Distance=="2700" &mat_res_importance_features$Lieu=="Vincennes",]
par(mar=c(5,12,4,4))
boxplot(Score~Feature,data=aa,horizontal=TRUE,cex.axis=0.6)

oind <- order(as.numeric(by(aa$Score, aa$Feature, median)))    
aa$Feature <- ordered(aa$Feature, levels=levels(aa$Feature)[oind])   
boxplot(Score ~ Feature, data=aa,horizontal=TRUE,cex.axis=0.6)


# ggplot(aa, aes(x = fct_reorder(Feature, Score, fun = median, .desc =TRUE), y = Score)) + 
#   geom_boxplot(aes(fill = fct_reorder(Feature, Score, fun = median, .desc =TRUE))) + 
#   scale_fill_manual(values = brewer.pal(3, "Dark2"), guide = guide_legend(title = "Species")) +
#   geom_jitter(position=position_jitter(0.2)) +
#   theme_classic(base_size = 14) +
#   xlab("Species") +
#   ylab("Sepal width")

bymedian <- with(aa, reorder(spray, -count, median))
boxplot(count ~ bymedian, data = InsectSprays,
        xlab = "Ty

# 
# 
# for(i in 1:length(vec_files_to_process))
# {
#   tmp = get_feature_importance_predictor_geny(file_name_target=vec_files_to_process[i])
# }
# 
