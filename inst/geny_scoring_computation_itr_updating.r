######################################## Loading required Packages ########################################
require(doSNOW)
require(doParallel)
require(foreach)
require(data.table)
require(plyr)
require(dplyr)
require( magrittr)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_mat_historical_races = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_historical_races.rds"
path_input_itr_per_horse   = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/itr"
path_output_itr = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed"
################################ Setting Output Path ############################

################################ Loading required functions ############################
source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_high_level_global_kpi_geny.R")
source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_high_level_global_kpi_geny.R")
################################ Loading required functions ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- ceiling(host_nb_cores*0.75)
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Data Extraction and storage ############################
mat_historical_races <- readRDS(path_mat_historical_races)
mat_historical_races <- mat_historical_races[nchar(mat_historical_races$Date)==10,]
vec_horses <- unique(as.vector(mat_historical_races$Cheval))
vec_horses <- vec_horses[!is.na(vec_horses)]
################################ Data Extraction and storage ############################

################################ Generate and store data ############################
mat_compilation_details_itr <- foreach(i=1:length(vec_horses),.combine='rbind') %dopar% {
  mat_historical_races_current <- mat_historical_races[mat_historical_races$Cheval==vec_horses[i],]
  mat_historical_races_current <- mat_historical_races_current[!is.na(mat_historical_races_current$Distance),]
  mat_historical_races_current <- subset(mat_historical_races_current,Gains>=0)
  if(nrow(mat_historical_races_current)>0) {
    get_high_level_global_kpi_geny(mat_perf_horse=mat_historical_races_current)
  }
}
################################ Generate and store data ############################

################################ Current ITR file ############################
vec_files_details_itr_historical <- dir(path_input_itr_per_horse)
setwd(path_input_itr_per_horse)
list_compilation_details_itr_historical  <- mclapply (X=vec_files_details_itr_historical,  FUN=fread,mc.cores=ceiling(host_nb_cores*0.75))
names(list_compilation_details_itr_historical) <- 1:length(vec_files_details_itr_historical)
mat_compilation_details_itr <- rbindlist(list_compilation_details_itr_historical,fill = TRUE)
mat_compilation_details_itr <- as.data.frame(mat_compilation_details_itr)
mat_compilation_details_itr <- unique(mat_compilation_details_itr)
################################ Current ITR file ############################

################################ Updating Scoring file ############################
mat_compilation_details_itr$GAIN_START <- as.numeric(mat_compilation_details_itr$GAIN_START)
mat_compilation_details_itr$GAIN_TOTAL <- as.numeric(mat_compilation_details_itr$GAIN_TOTAL)
################################ Updating Scoring file ############################

################################ ITR Computation ############################
vec_disciplines <- as.vector(unique(mat_compilation_details_itr$Discipline))
for(discipline in vec_disciplines)  
{
  mat_compilation_details_itr_current <- subset(mat_compilation_details_itr,Discipline==discipline)
  mat_compilation_details_itr_current <- subset(mat_compilation_details_itr_current,Age>0)
  mat_compilation_details_itr_current$CLASS_AGE <- mat_compilation_details_itr_current$Age
  mat_compilation_details_itr_current$CLASS_AGE[mat_compilation_details_itr_current$CLASS_AGE>5] <- "6+"
  mat_compilation_details_itr_current <- transform(mat_compilation_details_itr_current,GROUP = paste(mat_compilation_details_itr_current$Year,mat_compilation_details_itr_current$Gender,mat_compilation_details_itr_current$CLASS_AGE,mat_compilation_details_itr_current$Cluster_Distance,sep="-"))
  mat_compilation_details_itr_current <- transform(mat_compilation_details_itr_current,LOG_GAIN_START_HORSE=log(GAIN_START))
  mat_scaling_factor <- mat_compilation_details_itr_current %>% dplyr::group_by(GROUP) %>%
    dplyr::summarise(MEAN_GAIN=mean(GAIN_START,na.rm = TRUE),LOG_MEAN_GAIN_GROUP=log(MEAN_GAIN))%>%
    as.data.frame()
  
  mat_compilation_details_itr_current <- merge(mat_compilation_details_itr_current,mat_scaling_factor,by.x="GROUP",by.y="GROUP")
  mat_compilation_details_itr_current <- transform(mat_compilation_details_itr_current,LOG_GAIN_START_HORSE_CORRECTED=LOG_GAIN_START_HORSE-LOG_MEAN_GAIN_GROUP)
  mat_compilation_details_itr_current$LOG_GAIN_START_HORSE_CORRECTED [is.infinite(mat_compilation_details_itr_current$LOG_GAIN_START_HORSE_CORRECTED)] <- NA
  
  mat_scaling_factor <- mat_compilation_details_itr_current %>% dplyr::group_by(CLASS_AGE) %>%
    dplyr::summarise(LOG_GAIN_START_HORSE_CORRECTED_CLASS_MEAN=mean(LOG_GAIN_START_HORSE_CORRECTED,na.rm = TRUE),LOG_GAIN_START_HORSE_CORRECTED_CLASS_SD=sd(LOG_GAIN_START_HORSE_CORRECTED,na.rm = TRUE))%>%
    as.data.frame()
  
  mat_compilation_details_itr_current <- merge(mat_compilation_details_itr_current,mat_scaling_factor,by.x="CLASS_AGE",by.y="CLASS_AGE")
  mat_compilation_details_itr_current <- transform(mat_compilation_details_itr_current,SCORE_ITR=((LOG_GAIN_START_HORSE_CORRECTED-LOG_GAIN_START_HORSE_CORRECTED_CLASS_MEAN)/LOG_GAIN_START_HORSE_CORRECTED_CLASS_SD)*20)
  mat_compilation_details_itr_current <- transform(mat_compilation_details_itr_current,SCORE_ITR=SCORE_ITR+100)
  mat_compilation_details_itr_current <- unique(mat_compilation_details_itr_current)
  
  file_name <- paste("mat_scoring_itr",tolower(gsub(" ","-",gsub("è","e",gsub("é","e",discipline)))),sep="_")
  file_name <- paste(file_name,".rds",sep="")
  file_name <- paste(path_output_itr,file_name,sep="/")
  saveRDS(mat_compilation_details_itr_current,file_name)
}
################################ ITR Computation ############################
