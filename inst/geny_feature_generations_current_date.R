######################################## Loading required Packages ########################################
require(doSNOW)
require(doParallel)
require(foreach)
require(dplyr)
require(data.table)
require(magrittr)
require(PlayerRatings)
require(EloRating)
require(elo)
require(rvest)
require(stringr)
require(curl)
require(WriteXLS)
require(xlsx)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_output_data <- "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/output/indicators/"
path_historical_races = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_historical_races.rds"
path_input_ranking_data = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_line_by_line_rating.rds"
################################### Setting Output Path ############################

################################ Loading some needed functions ################################
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_number_podium_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_features_based_last_race.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_features_based_context_race.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_features_based_success_race.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_features_based_speed_race.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_features_based_ranking_race.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_features_based_distances_affinity_race.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_features_based_global_race.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_average_speed_horse.R')
source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_race_current_date_geny.R")
source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_details_race_geny.R")
source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_table_details_geny.R")
source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_high_level_global_kpi_geny.R")
source("/data/projects/home/mpaye/Easy-Web-Scraper/R/convert_cumulative_wins_individual_geny.R")
source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_ranking_generic_itr_geny.R")
source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_ranking_generic_speed_geny.R")
source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_ranking_generic_elo_geny.R")
source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_ranking_generic_distance_affinity_geny.R")
source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_cluster_distance_race_type.R")
################################ Loading some needed functions ################################

################################ Extract races current day ############################
target_date <- as.character(Sys.Date())
url_path_current_date <- paste("http://www.geny.com/reunions-courses-pmu/_d",target_date,"?",sep="")
vec_races_current_date <- rev(get_race_current_date_geny(url_path_date=url_path_current_date))
# vec_races_current_date <- vec_races_current_date[-grep("vincenn",vec_races_current_date)]
################################ Extract races current day ############################

################################ Setting output directory ############################
setwd(path_output_data)
dir.create(target_date)
################################ Setting output directory ############################

################################ Loading useful data ################################
print("Loading line by line analysis matrix")
mat_line_by_line <- readRDS(path_input_ranking_data)
mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Date),]
mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Discipline),]
mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Looser),]
mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Looser_Distance),]
mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Winner),]
mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Winner_Distance),]
# mat_line_by_line$RACE_TYPE <- NA
# mat_line_by_line$RACE_TYPE[mat_line_by_line$Winner_Distance <=1600 & mat_line_by_line$Looser_Distance <=1600] <-'Sprint' 
# mat_line_by_line$RACE_TYPE[mat_line_by_line$Winner_Distance>1600 & mat_line_by_line$Looser_Distance>1600 & mat_line_by_line$Winner_Distance<=2000 & mat_line_by_line$Looser_Distance<=2000] <-'Flyer'
# mat_line_by_line$RACE_TYPE[mat_line_by_line$Winner_Distance>2000 & mat_line_by_line$Looser_Distance>2000 & mat_line_by_line$Winner_Distance<=2400 & mat_line_by_line$Looser_Distance<=2400] <-'Miler'
# mat_line_by_line$RACE_TYPE[mat_line_by_line$Winner_Distance>2400 & mat_line_by_line$Looser_Distance>2400 & mat_line_by_line$Winner_Distance<=2800 & mat_line_by_line$Looser_Distance<=2800] <-'Classique'
# mat_line_by_line$RACE_TYPE[mat_line_by_line$Winner_Distance>2800 & mat_line_by_line$Looser_Distance>2800 ] <-'Marathon'
print("Line by line analysis matrix loaded")
print("Loading historical performance matrix")
mat_historical_races <- readRDS(path_historical_races)
mat_historical_races$Cheval <- as.vector(mat_historical_races$Cheval)
mat_historical_races$Race <- as.vector(mat_historical_races$Race)
mat_historical_races$Date <- as.Date(mat_historical_races$Date)
mat_historical_races$Prix <- as.numeric(mat_historical_races$Prix)
mat_historical_races$Gains <- as.numeric(mat_historical_races$Gains)
print("Historical performance matrix loaded")
################################ Loading useful data ################################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.5))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Calculate the features ############################
foreach(i=1:length(vec_races_current_date)) %dopar% {
#for(i in 1:length(vec_races_current_date)) {
  require(doSNOW)
  require(doParallel)
  require(foreach)
  require(dplyr)
  require(data.table)
  require(magrittr)
  require(PlayerRatings)
  require(EloRating)
  require(elo)
  require(rvest)
  require(stringr)
  require(curl)
  require(WriteXLS)
  require(xlsx)
  source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_number_podium_geny.R')
  source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_features_based_last_race.R')
  source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_features_based_context_race.R')
  source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_features_based_success_race.R')
  source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_features_based_speed_race.R')
  source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_features_based_ranking_race.R')
  source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_features_based_distances_affinity_race.R')
  source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_features_based_global_race.R')
  source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_average_speed_horse.R')
  source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_race_current_date_geny.R")
  source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_details_race_geny.R")
  source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_table_details_geny.R")
  source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_high_level_global_kpi_geny.R")
  source("/data/projects/home/mpaye/Easy-Web-Scraper/R/convert_cumulative_wins_individual_geny.R")
  source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_ranking_generic_itr_geny.R")
  source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_ranking_generic_speed_geny.R")
  source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_ranking_generic_elo_geny.R")
  source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_ranking_generic_distance_affinity_geny.R")
  source("/data/projects/home/mpaye/Easy-Web-Scraper/R/get_cluster_distance_race_type.R")

  mat_infos_race <- get_details_race_geny(vec_races_current_date[i])
  mat_infos_race_add <- mat_infos_race[,intersect(colnames(mat_infos_race),c("Numero","Cheval","Gender","Age","Depart","Corde","Recul","Distance","Lieu", "Date", "Place"))]
  if("Place" %in% colnames(mat_infos_race_add))
  {
    mat_infos_race_add$Placer <- ifelse(mat_infos_race_add$Place<6,"Oui","Non")
  }
  
  vec_race_identifier <- paste(mat_infos_race$Date,mat_infos_race$Heure,mat_infos_race$Lieu,mat_infos_race$Course,sep="-")
  mat_infos_race <- transform(mat_infos_race,Race =vec_race_identifier)
  
  mat_infos_race_add$Recul <- ifelse(mat_infos_race_add$Recul>0,"Oui","Non")
  # mat_infos_race_add <- mat_infos_race_add[,setdiff(colnames(mat_infos_race_add),"Place")]
  current_discipline <- as.vector(unique(mat_infos_race$Discipline))
  if(current_discipline %in% c("Trot Attelé","Trot Monté")) {
    mat_features_based_last    <- get_features_based_last_race(url_target_race=vec_races_current_date[i],file_name_target=NULL)
    mat_features_based_context <- get_features_based_context_race(url_target_race=vec_races_current_date[i],file_name_target=NULL)
    mat_features_based_success <- get_features_based_success_race(url_target_race=vec_races_current_date[i],file_name_target=NULL)
    mat_features_based_speed   <- get_features_based_speed_race(url_target_race=vec_races_current_date[i],file_name_target=NULL)
    mat_features_based_distances_affinity <- get_features_based_distances_affinity_race(url_target_race=vec_races_current_date[i],file_name_target=NULL)
    mat_features_based_global_scores <- get_features_based_global_race (url_target_race=vec_races_current_date[i],file_name_target=NULL)
    mat_features_based_ranking <- get_features_based_ranking_race (url_target_race=vec_races_current_date[i],file_name_target=NULL)
    list_all_features <- list(A=mat_features_based_distances_affinity,B=mat_features_based_ranking,C=mat_features_based_success,D=mat_features_based_speed,E=mat_features_based_last,F=mat_features_based_global_scores,G=mat_features_based_context,H=mat_infos_race_add)
    list_all_features <- list_all_features[lapply(list_all_features,length)>0]
    if(length(list_all_features)>0) {
      mat_all_features <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_all_features)
    }
    setwd(paste(path_output_data,target_date,sep="/"))
    
    vec_context_column <- c("Numero","Cheval","Age","Gender","Poids","Valeur","Déch.","Jockey","Musique","Cote_1","Cote_2","Ferrage") 
    vec_context_column <- intersect(colnames(mat_all_features),vec_context_column)
    
    mat_all_features <- mat_all_features[,c(vec_context_column,setdiff(colnames(mat_all_features),vec_context_column))]
    
    file_name_output <- as.vector(unique(mat_infos_race$Race))
    file_name_output <- gsub("é","e",file_name_output)
    file_name_output <- gsub("è","e",file_name_output)
    file_name_output <- gsub(" ","-",file_name_output)
    file_name_output <- gsub("\\(","",file_name_output)
    file_name_output <- gsub("\\)","",file_name_output)
    file_name_output <- paste(file_name_output,".xls",sep="")
    
    WriteXLS(mat_all_features, file_name_output)

  }
}
