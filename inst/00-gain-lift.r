vec_race <- subset(matrix_infos_candidate_races,Lieu=="Graignes")[-c(1:3),"Link"]
for(race in vec_race)
{
  get_horse_score_racing_turfoo( url.path.new.race=race)
  print(race)
}








# first put to zero the small number

gains_threshold <- 2000
value_link_threshold <-2
mat_gains_distances_lift <- mat.average.gains.distance.horse
rownames(mat_gains_distances_lift) <- as.vector(mat_gains_distances_lift[,1])
mat_gains_distances_lift <- mat_gains_distances_lift[,-1]
vec_keep=apply(mat_gains_distances_lift,2,sum)/sum(apply(mat_gains_distances_lift,2,sum))
vec_keep=colnames(mat_gains_distances_lift)[vec_keep>0.015]
mat_gains_distances_lift  <- mat_gains_distances_lift [,vec_keep]
mat_gains_distances_lift[mat_gains_distances_lift<gains_threshold ]<-0
mat_gains_distances_lift[mat_gains_distances_lift==0]<-NA

for(distance in colnames(mat_gains_distances_lift))
{
  weight_current_distance <- sum(mat_gains_distances_lift[,distance],na.rm = TRUE)/sum(mat_gains_distances_lift,na.rm = TRUE)
  weight_current_distance <- weight_current_distance*100 
  for(horse in rownames(mat_gains_distances_lift))
  {
    weight_current_horse <- mat_gains_distances_lift[horse,distance]/sum(mat_gains_distances_lift[horse,],na.rm = TRUE)
    weight_current_horse <- weight_current_horse*100
    mat_gains_distances_lift [horse,distance] <- weight_current_horse/weight_current_distance
  }
}
mat_gains_distances_lift[mat_gains_distances_lift<value_link_threshold]<-0
mat_gains_distances_lift[is.na(mat_gains_distances_lift)]<-0
mat_gains_distances_lift <- mat_gains_distances_lift [,vec_keep]
graph_matrix_bipartite_network(mat_gains_distances_lift)

