################################ Loading required packages ################################
.libPaths(c("/userhomes/mpaye/R/x86_64-pc-linux-gnu-library/3.4","/usr/local/lib/R/site-library","/usr/lib/R/site-library")) 
library("rlang", lib.loc="~/R/x86_64-pc-linux-gnu-library/3.3")
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(plyr)
require(dplyr)
require(xml2)
require(doSNOW)
require(doParallel)
require(curl)
require(doMC)
require(lubridate)
################################ Loading required packages ################################

################################ Loading some needed functions ################################
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_details_given_race_letrot.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_results_given_race_letrot.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_correct_revenue_letrot.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_correct_reduction_format_letrot.R')
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_correct_number_letrot.R')
################################ Loading some needed functions ############################

################################ Setting Output Path ############################
path_output_data <- "/userhomes/mpaye/Futanke-Mbayar/01-db/lt/output/01-raw/performances"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.75))
registerDoParallel(nb_cores_use)
# doMC::registerDoMC(nb_cores_use)
################################ Parallelization Management ############################

################################ Generate and store data ############################
seq_date <- seq(as.Date("01-01-2010","%d-%m-%Y"), Sys.Date(), by = "day")
seq_date <- unlist(lapply(seq_date,convert_date_format_letrot))
vec_url_date_scrape <- unlist(lapply(seq_date,get_links_schedule_track_target_day_letrot))
vec_url_date_scrape <- rev(vec_url_date_scrape)
vec_url_date_scrape <- gsub("courses/programme","fiche-course",vec_url_date_scrape)

res_races_scrape <- foreach(i=1:length(vec_url_date_scrape),.combine=rbind.fill) %dopar% {
  require(rvest)
  require(stringr)
  require(curl)
  vec_race_current_date <- get_links_races_target_track_letrot(vec_url_date_scrape[i])
  for(j in 1:length(vec_race_current_date))
  {
    url_path_pro_current <- vec_race_current_date[j]
    url_path_res_current <- sub("partants/tableau","resultats/arrivee-definitive#sub_sub_menu_course",url_path_pro_current)
    
    mat_details_race <- get_details_target_race_letrot(url_path_pro_current)
    mat_results_race <- get_results_target_race_letrot(url_path_res_current)
    mat_results_race <- mat_results_race[,c("Rang","Numero","Cheval","Time","Reduction","Allocation","Status")]
    
    vec_horses_common <- intersect(mat_details_race$Cheval,mat_results_race$Cheval)
    
    if(length(vec_horses_common)>0) {
      mat_infos_race <- merge(mat_details_race,mat_results_race,by.x="Cheval",by.y="Cheval")
      setwd(path_output_data)
      # file_name_output <- gsub("https://www.letrot.com/stats/fiche-course/","",url_path_pro_current)
      # file_name_output <- gsub("/","-",file_name_output)
      # file_name_output <- gsub("-partants-tableau","",file_name_output)
      # file_name_output <- paste(i,file_name_output,sep="_")
      # file_name_output <- paste(file_name_output,".csv",sep="")
      file_name_output <- rev(unlist(strsplit(tempfile(),"/")))[1] 
      file_name_output <- paste(file_name_output,".csv",sep="")
      file_name_output <- paste(path_output_data,file_name_output,sep="/")
      write.csv(mat_infos_race,file=file_name_output,row.names = FALSE)
    }
  }
}
################################ Generate and store data ############################



