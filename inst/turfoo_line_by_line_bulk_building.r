######################################## Loading required Packages ########################################
require(doSNOW)
require(doParallel)
require(foreach)
require(data.table)
require(plyr)
require(dplyr)
######################################## Loading required Packages ########################################

################################ Loading some needed functions ############################
source('/userhomes/mpaye/Easy-Web-Scraper/R/get_line_by_line_driver_jockey_race_turfoo.R')
################################ Loading some needed functions ############################

################################ Setting Output Path ############################
path_line_by_line_output  =  "/userhomes/mpaye/Futanke-Mbayar/01-db/tf/output/01-raw/ranking"
path_mat_historical_races = "/userhomes/mpaye/Futanke-Mbayar/01-db/tf/output/02-process/mat.historical.races.rds"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- ceiling(host_nb_cores*0.25)
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Data Extraction and storage ############################
mat_historical_races = readRDS(path_mat_historical_races)
mat_historical_races$Cheval <- as.vector(mat_historical_races$Cheval)
mat_historical_races$Race <- as.vector(mat_historical_races$Race)
mat_historical_races$Date <- as.Date(mat_historical_races$Date)
mat_historical_races <- transform(mat_historical_races,Delay=Sys.Date()-Date)
mat_historical_races$Discipline[mat_historical_races$Discipline== "Trot AttelÃ©"] <- "Trot Attelé" 
mat_historical_races$Discipline[mat_historical_races$Discipline== "Trot MontÃ©"]  <- "Trot Monté"
vec_races <- as.vector(unique(mat_historical_races$Race))
vec_races <- vec_races[!is.na(vec_races)]
################################ Data Extraction and storage ############################

################################ Generate and store data ############################
foreach(i=56470:length(vec_races)) %dopar% {
  mat_historical_races_current <- mat_historical_races[mat_historical_races$Race==vec_races[i],]
  mat_historical_races_current <- mat_historical_races_current [!is.na(mat_historical_races_current$Race),]
  get_line_by_line_driver_jockey_race_turfoo (mat_perf_race_complete=mat_historical_races_current,target_item="Horse")
  get_line_by_line_driver_jockey_race_turfoo (mat_perf_race_complete=mat_historical_races_current,target_item="Driver_Jockey")
  get_line_by_line_driver_jockey_race_turfoo (mat_perf_race_complete=mat_historical_races_current,target_item="Entraineur")
}
################################ Generate and store data ############################

setwd(path_line_by_line_output)
# files_line_by_line <- list.files(path=path_line_by_line_output,pattern = "^Driver_Jockey")
files_line_by_line <- list.files(path=path_line_by_line_output)
mat_line_by_line <- lapply(files_line_by_line, fread, sep=",")
names(mat_line_by_line) <- 1:length(mat_line_by_line)
mat_line_by_line <- rbindlist(mat_line_by_line,fill = TRUE)
mat_line_by_line <- as.data.frame(mat_line_by_line)

vec_races_found <- as.vector(unique(mat_line_by_line$Race))
vec_missing_races <- setdiff(vec_races,vec_races_found)

for(i in 1:length(vec_missing_races)) {
  mat_historical_races_current <- mat_historical_races[mat_historical_races$Race==vec_missing_races[i],]
  mat_historical_races_current <- mat_historical_races_current [!is.na(mat_historical_races_current$Race),]
  mat_historical_races_current <- mat_historical_races_current [!is.na(mat_historical_races_current$Place),]
  if(nrow(mat_historical_races_current)>1) {
    get_line_by_line_driver_jockey_race_turfoo (mat_perf_race_complete=mat_historical_races_current,target_item="Horse")
    get_line_by_line_driver_jockey_race_turfoo (mat_perf_race_complete=mat_historical_races_current,target_item="Driver_Jockey")
    get_line_by_line_driver_jockey_race_turfoo (mat_perf_race_complete=mat_historical_races_current,target_item="Entraineur")
  }
  print(i)
}



foreach(i=1:length(vec_missing_races)) %dopar% {
  mat_historical_races_current <- mat_historical_races[mat_historical_races$Race==vec_missing_races[i],]
  mat_historical_races_current <- mat_historical_races_current [!is.na(mat_historical_races_current$Race),]
  if(nrow(mat_historical_races_current)>1) {
    get_line_by_line_driver_jockey_race_turfoo (mat_perf_race_complete=mat_historical_races_current,target_item="Horse")
    get_line_by_line_driver_jockey_race_turfoo (mat_perf_race_complete=mat_historical_races_current,target_item="Driver_Jockey")
    get_line_by_line_driver_jockey_race_turfoo (mat_perf_race_complete=mat_historical_races_current,target_item="Entraineur")
  }
}


# mat_historical_races <- read.csv(paste(path_mat_historical_races,name_file_historical_races,sep='/'))
# mat_historical_races <- as.data.frame(mat_historical_races)
# mat_historical_races$Date <- as.vector(mat_historical_races$Date)
# mat_historical_races$Cheval <- as.vector(mat_historical_races$Cheval)
# mat_historical_races$Race <- as.vector(mat_historical_races$Race)
# mat_historical_races$Discipline[mat_historical_races$Discipline== "Trot AttelÃ©"] <- "Trot Attelé" 
# mat_historical_races$Discipline[mat_historical_races$Discipline== "Trot MontÃ©"]  <- "Trot Monté"
# mat_historical_races <- transform(mat_historical_races, IdentifierHorseRace=paste(mat_historical_races$Race,mat_historical_races$Cheval,sep="|"))
# vec_freq_horse_race  <- table(mat_historical_races$IdentifierHorseRace)
# mat_historical_races <- subset(mat_historical_races,IdentifierHorseRace %in% names(vec_freq_horse_race)[vec_freq_horse_race<2])
# setwd(path_mat_historical_races)
# list_historical_races <- split.data.frame(mat_historical_races,mat_historical_races$Race)
# 
# for(i in 1:length(list_historical_races))
# {
#   get_line_by_line_race_turfoo(list_historical_races[[i]])
# }
# 
# setwd(path_line_by_line_output)
# files_line_by_line <- list.files(path=getwd())
# mat_line_by_line <- lapply(files_line_by_line, fread, sep=",")
# names(mat_line_by_line) <- 1:length(mat_line_by_line)
# mat_line_by_line <- rbindlist(mat_line_by_line,fill = TRUE)
# mat_line_by_line <- as.data.frame(mat_line_by_line)
