######################################## Loading required Packages ########################################
require(doSNOW)
require(doParallel)
require(foreach)
require(data.table)
require(plyr)
require(dplyr)
######################################## Loading required Packages ########################################

################################ Loading some needed functions ############################
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_line_by_line_driver_jockey_race_geny.R')
################################ Loading some needed functions ############################

################################ Setting Output Path ############################
path_line_by_line_output  =  "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/ranking"
path_mat_historical_races = "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_historical_races.rds"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- ceiling(host_nb_cores*0.75)
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Data Extraction and storage ############################
mat_historical_races <- readRDS(path_mat_historical_races)
mat_historical_races$Cheval <- as.vector(mat_historical_races$Cheval)
mat_historical_races$Race <- as.vector(mat_historical_races$Race)
mat_historical_races$Date <- as.Date(mat_historical_races$Date)
mat_historical_races <- transform(mat_historical_races,Delay=Sys.Date()-Date)
mat_historical_races <- mat_historical_races[mat_historical_races$Delay<=730,]
vec_discipline <- as.vector(unique(mat_historical_races$Discipline))
vec_discipline <- c("Trot Attelé","Plat","Haies","Steeplechase","Trot Monté","Cross")
################################ Data Extraction and storage ############################

################################ Generate and store data ############################
for(discipline in rev(vec_discipline))
{
  mat_historical_races_focus <- subset(mat_historical_races,Discipline==discipline)
  vec_races <- as.vector(unique(mat_historical_races_focus$Race))
  vec_races <- vec_races[!is.na(vec_races)]
  print(paste("starting :",discipline))
  foreach(i=1:length(vec_races)) %dopar% {
  # for (i in 1:length(vec_races)){
    mat_historical_races_current <- mat_historical_races_focus[mat_historical_races_focus$Race==vec_races[i],]
    mat_historical_races_current <- mat_historical_races_current [!is.na(mat_historical_races_current$Race),]
    if(nrow(mat_historical_races_current)>1) {
      get_line_by_line_driver_jockey_race_geny (mat_perf_race_complete=mat_historical_races_current,target_item="Horse")
    }
  }
  print(paste("finishing :",discipline))
}
################################ Generate and store data ############################
