######################################## Loading required Packages ########################################
require(plyr)
require(dplyr)
require(stringr)
require(foreach)
require(doSNOW)
require(doParallel)
require(parallel)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_historical_races <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed/mat_historical_races.rds"
path_impact_tract_output = "/data/projects/home/mpaye/Futanke-Mbayar/02-ds/output/hippodrome"
################################ Setting Output Path ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.75))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Loading some needed functions ################################
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_impact_track_performances_geny.R')
################################ Loading some needed functions ################################

################################ Loading historical performaces ################################
if(!exists("mat_historical_races")) {
  mat_historical_races <- readRDS(path_historical_races)
  mat_historical_races$Cheval <- as.vector(mat_historical_races$Cheval)
  mat_historical_races$Race <- as.vector(mat_historical_races$Race)
  mat_historical_races$Date <- as.Date(mat_historical_races$Date)
  mat_historical_races$Prix <- as.numeric(mat_historical_races$Prix)
  mat_historical_races$Gains <- as.numeric(mat_historical_races$Gains)
}
################################ Loading historical performaces ################################

################################ Estimating Trach Impact ################################
vec_distances_discipline <- unique(mat_historical_races[mat_historical_races$Discipline=="Trot Attelé","Distance"])
vec_distances_discipline <- as.numeric(vec_distances_discipline)
vec_depart_discipline <- c("Auto-Start","Volte")

mat_grids_params <- expand.grid(Discipline = "Trot Attelé", Distance=vec_distances_discipline,Depart=vec_depart_discipline,Year=2016:2019)
mat_impact_track_performances_compilation <- NULL

mat_impact_track_performances_compilation <- foreach(idx=1:nrow(mat_grids_params),.combine=rbind.fill) %dopar% {
  target_discipline <- mat_grids_params[idx,"Discipline"]
  target_distance <- mat_grids_params[idx,"Distance"]
  target_depart <- mat_grids_params[idx,"Depart"]
  target_year <- mat_grids_params[idx,"Year"]
  require(stringr)
  require(readxl)
  require(dplyr)
  mat_impact_track_performances_compilation_current <- get_impact_track_performances_geny(target_discipline = target_discipline,target_distance = target_distance,target_depart =target_depart,target_year=target_year)
  # mat_impact_track_performances_compilation <- rbind(mat_impact_track_performances_compilation,mat_impact_track_performances_compilation_current)
  # print(paste(idx,nrow(mat_grids_params)))
}
################################ Estimating Trach Impact ################################
