################################ Loading required packages ################################
require(readr)
require(rvest)
require(stringi)
require(stringr)
require(foreach)
require(plyr)
require(dplyr)
require(xml2)
require(doSNOW)
require(doParallel)
require(curl)
require(data.table)
require(parallel)
require(gtools)
################################ Loading required packages ################################

################################ Loading some needed functions ################################
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_results_race_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_table_results_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_race_given_date_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_details_race_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_table_details_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/convert_numeric_values_reduc_ecart_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_amount_win_horse_given_race_geny.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_race_allocation_from_details.R')
source('/data/projects/home/mpaye/Easy-Web-Scraper/R/get_race_group_type_geny.R')
################################ Loading some needed functions ############################

################################ Setting Output Path ############################
path_input_historical <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed"
path_output_data_tmp <- "/data/projects/home/mpaye/Futanke-Mbayar/01-db/gy/output/01-raw/performances-tmp"
################################ Setting Output Path ############################

################################ Reading current data ############################
mat_historical_races <- readRDS(paste(path_input_historical,"mat_historical_races.rds",sep="/"))
mat_historical_races$Date <- unlist(lapply(mat_historical_races$Race,function(x){substr(x,1,10)}))
mat_historical_races$Date <- as.Date(mat_historical_races$Date)
mat_historical_races <- mat_historical_races[nchar(as.character(mat_historical_races$Date))==10,]
most_recent_date <- as.Date(max(mat_historical_races$Date,na.rm = TRUE))
################################ Reading current data ############################

################################ Parallelization Management ############################
host_os  <- as.vector(Sys.info()['sysname'])
host_nb_cores <- parallel::detectCores()
nb_cores_use <- makeCluster(ceiling(host_nb_cores*0.75))
registerDoParallel(nb_cores_use)
################################ Parallelization Management ############################

################################ Generate and store data ############################
seq_date <- seq(most_recent_date+1, Sys.Date(), by = "day")
vec_url_date_scrape   <- paste("http://www.geny.com/reunions-courses-pmu?date=",seq_date,sep="")
vec_url_date_scrape <- rev(vec_url_date_scrape)
res_races_scrape <- foreach(i=1:length(vec_url_date_scrape),.combine=rbind.fill) %dopar% {
# for (i in 1:length(vec_url_date_scrape)) {
  require(curl)
  require(readr)
  require(rvest)
  require(stringi)
  require(stringr)
  require(foreach)
  require(plyr)
  require(dplyr)
  require(xml2)
  require(doSNOW)
  require(doParallel)
  require(data.table)
  require(parallel)
  vec_race_current_date <- get_race_given_date_geny(url_path_date=vec_url_date_scrape[i])
  for(j in 1:length(vec_race_current_date))
  {
    url_path_res_current <- vec_race_current_date[j]
    url_path_pro_current <- sub('arrivee-et-rapports-pmu','partants-pmu',url_path_res_current)
    
    mat_details_race <- get_details_race_geny(url_path_pro_current)
    mat_results_race <- get_results_race_geny(url_path_res_current)
    vec_horses_common <- intersect(mat_details_race$Cheval,mat_results_race$Cheval)
    if(length(vec_horses_common)>0) {
      mat_infos_race <- merge(mat_details_race,mat_results_race,by.x="Cheval",by.y="Cheval")
      mat_infos_race$Reduction <- NA
      if("Ecart" %in% colnames(mat_infos_race)) {
        mat_infos_race$Reduction <- unlist(lapply(mat_infos_race$Ecart, convert_numeric_values_reduc_ecart_geny))
      }
    }
    
    mat_infos_race <- get_amount_win_horse_given_race_geny(mat_perf_race_complete=mat_infos_race)
    setwd(path_output_data_tmp)
    file_name_output <- gsub("http://www.geny.com/partants-pmu/","infos-races-",url_path_pro_current)
    file_name_output <- paste(i,file_name_output,sep="_")
    file_name_output <- paste(file_name_output,".csv",sep="")
    file_name_output <- paste(path_output_data_tmp,file_name_output,sep="/")
    write.csv(mat_infos_race,file=file_name_output,row.names = FALSE)
    print(i)
  }
}
################################ Generate and store data ############################

################################ Parallelization Management ############################
vec_availables_files <- dir(path_output_data_tmp)
setwd(path_output_data_tmp)
list_new_races  <- mclapply (X=vec_availables_files,  FUN=fread, sep=",",mc.cores=ceiling(host_nb_cores*0.5))
# list_new_races  <- mclapply (X=list_new_races, FUN=get_race_allocation_from_details,mc.cores=ceiling(host_nb_cores*0.75))
# list_new_races  <- mclapply (X=list_new_races, FUN=get_amount_win_horse_given_race_geny,mc.cores=ceiling(host_nb_cores*0.75))
names(list_new_races) <- 1:length(list_new_races)
mat_new_races <- rbindlist(list_new_races,fill = TRUE)
mat_new_races <- as.data.frame(mat_new_races)
vec_name_races <- paste(mat_new_races$Date,mat_new_races$Heure,mat_new_races$Lieu,mat_new_races$Course,sep="-")
mat_new_races <- transform(mat_new_races,Race=vec_name_races)
mat_new_races <- mat_new_races %>% distinct()
mat_new_races <- mat_new_races[nchar(mat_new_races$Date)==10,]
mat_new_races$Date <- as.Date(mat_new_races$Date)
mat_historical_races <- rbind.fill(mat_historical_races,mat_new_races)
mat_historical_races <- mat_historical_races %>% distinct()
idx_good_cordes <- mat_historical_races$Corde %in% c("Droite","Gauche")
vec_val_cordes <- mat_historical_races$Corde
mat_historical_races$Corde <- NA
mat_historical_races$Corde[idx_good_cordes] <- vec_val_cordes[idx_good_cordes]
idx_good_depart <- mat_historical_races$Depart %in% c("Auto-Start","Volte")
vec_val_depart <- mat_historical_races$Depart
mat_historical_races$Depart <- NA
mat_historical_races$Depart[idx_good_depart] <- vec_val_depart[idx_good_depart]
mat_historical_races$CD <- paste(mat_historical_races$Corde,mat_historical_races$Depart,sep="-")
mat_historical_races <- mat_historical_races[mat_historical_races$Prix!=1630544958,]
mat_historical_races <- mat_historical_races[mat_historical_races$Prix!=1451164763,]
mat_historical_races$Prix[!is.na(mat_historical_races$Prix)][mat_historical_races$Prix[!is.na(mat_historical_races$Prix)]==600010000] <- 6000
mat_historical_races$Prix[!is.na(mat_historical_races$Prix)][mat_historical_races$Prix[!is.na(mat_historical_races$Prix)]==193250002] <- 52000
mat_historical_races$Prix[!is.na(mat_historical_races$Prix)][mat_historical_races$Prix[!is.na(mat_historical_races$Prix)]>13860216] <- mat_historical_races$Prix[!is.na(mat_historical_races$Prix)][mat_historical_races$Prix[!is.na(mat_historical_races$Prix)]>13860216] /1000
mat_historical_races <- mat_historical_races[!is.na(mat_historical_races$Prix),]
vec_prix_recode <- quantcut(mat_historical_races$Prix,5)
vec_levels_cut_prix <- levels(vec_prix_recode)
vec_labels_cut_prix  <- rev(LETTERS[1:5])
vec_prix_recode <- as.vector(vec_prix_recode)
for(level in 1:length(vec_levels_cut_prix))
{
  vec_prix_recode[vec_prix_recode==vec_levels_cut_prix[level]] <- vec_labels_cut_prix[level]
}
mat_historical_races$Level <- vec_prix_recode
setwd(path_input_historical)
saveRDS(mat_historical_races,"mat_historical_races.rds")
################################ Parallelization Management ############################
