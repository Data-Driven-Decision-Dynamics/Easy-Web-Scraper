######################################## Loading required Packages ########################################
require(data.table)
require(plyr)
require(dplyr)
require(PlayerRatings)
require(EloRating)
######################################## Loading required Packages ########################################

################################ Setting Output Path ############################
path_input_data <- "/userhomes/mpaye/Futanke-Mbayar/01-db/gy/output/02-processed"
################################ Setting Output Path ############################

################################ Setting Output Path ############################
file_line_by_line <- dir(path=path_input_data,pattern = "line_by_line")
mat_line_by_line <- readRDS(paste(path_input_data,file_line_by_line,sep="/"))
mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Discipline),]
vec_disciplines <- as.vector(unique(mat_line_by_line$Discipline))
for(discipline in rev(vec_disciplines))
{
  mat_line_by_line_rating <- subset(mat_line_by_line,Discipline %in% discipline)
  mat_line_by_line_rating$Date <- as.Date(mat_line_by_line_rating$Date)
  mat_line_by_line_rating  <- transform(mat_line_by_line_rating,Delay=Sys.Date()-Date)
  mat_line_by_line_rating <-  transform(mat_line_by_line_rating,WeekTempory=mat_line_by_line_rating$Delay/7)
  vec.values.times <- as.numeric(as.vector(mat_line_by_line_rating$WeekTempory))
  mat_line_by_line_rating$Week <- vec.values.times
  mat_line_by_line_rating <-  transform(mat_line_by_line_rating,Score=1)
  mat_line_by_line_rating  <- mat_line_by_line_rating[order(mat_line_by_line_rating$Date),]
  mat_line_by_line_rating  <- unique(mat_line_by_line_rating)
  mat_line_by_line_rating <- mat_line_by_line_rating [,c("Week","Winner","Looser","Score")]
  mat_line_by_line_rating <- mat_line_by_line_rating[complete.cases(mat_line_by_line_rating),]
  mat_line_by_line_rating$Winner <- as.vector(mat_line_by_line_rating$Winner)
  mat_line_by_line_rating$Looser <- as.vector(mat_line_by_line_rating$Looser)
  infos_res_glicko_golbal <-  PlayerRatings::glicko(mat_line_by_line_rating,history=TRUE)
  
  
  mat_res_glicko_golbal <- infos_res_glicko_golbal$ratings
  rm(infos_res_glicko_golbal)
  gc()
  mat_res_glicko_golbal <- mat_res_glicko_golbal[,c("Player","Rating","Deviation","Games","Win","Loss")]
  colnames(mat_res_glicko_golbal) <- sub("Player","Cheval",colnames(mat_res_glicko_golbal))
  colnames(mat_res_glicko_golbal) <- sub("Rating","Glicko",colnames(mat_res_glicko_golbal))
  infos_res_steph_golbal <- PlayerRatings::steph(mat_line_by_line_rating,history=TRUE)
  mat_res_steph_golbal <- infos_res_steph_golbal$ratings
  rm(infos_res_steph_golbal )
  gc()
  mat_res_steph_golbal <- mat_res_steph_golbal[,c("Player","Rating")]
  colnames(mat_res_steph_golbal) <- sub("Player","Cheval",colnames(mat_res_steph_golbal))
  colnames(mat_res_steph_golbal) <- sub("Rating","Steph",colnames(mat_res_steph_golbal))
  infos_res_elo_golbal <- PlayerRatings::elo(mat_line_by_line_rating,history=TRUE)
  mat_res_elo_golbal <- infos_res_elo_golbal$ratings
  rm(infos_res_elo_golbal)
  gc()
  mat_res_elo_golbal <- mat_res_elo_golbal[,c("Player","Rating")]
  colnames(mat_res_elo_golbal) <- sub("Player","Cheval",colnames(mat_res_elo_golbal))
  colnames(mat_res_elo_golbal) <- sub("Rating","Elo",colnames(mat_res_elo_golbal))
  mat_res_rating_global <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list(mat_res_elo_golbal,mat_res_glicko_golbal,mat_res_steph_golbal))
  mat_res_rating_global <- mat_res_rating_global[,c(c("Cheval","Elo","Glicko","Steph"),setdiff(colnames(mat_res_rating_global),c("Cheval","Elo","Glicko","Steph")))]
  mat_res_rating_global <- transform(mat_res_rating_global,Success=(1-(Loss/Win))*100)
  setwd(path_output_data)
  # write.xlsx(mat_res_rating_global,file=paste(gsub(" ","-", discipline),".xlsx",sep="")) 
  vec_freq_distances <- sort(table(mat_line_by_line$Looser_Distance),decreasing = TRUE)
  vec_freq_distances <- vec_freq_distances /sum(vec_freq_distances)
  cumsum_vec_freq_distances <- cumsum(vec_freq_distances)
  vec_distance_useful <- as.numeric(names(cumsum_vec_freq_distances)[cumsum_vec_freq_distances<=0.96])
  res_kmeans_distances <- kmeans(vec_distance_useful,4)
  mat_cluster_distance <- data.frame(Distance=as.numeric(as.character(vec_distance_useful)),Speciality=res_kmeans_distances$cluster)
  
  list_distances <- vector('list',length(unique(res_kmeans_distances$cluster)))
  names(list_distances) <- unique(res_kmeans_distances$cluster)
  list_res_rating_specific_compliled <- vector("list",length(sort(unique(res_kmeans_distances$cluster))))
  names(list_res_rating_specific_compliled) <- sort(unique(res_kmeans_distances$cluster))
  
  mat_margin_cluster_distance <- mat_cluster_distance %>%
    group_by(Speciality) %>%
    dplyr::summarise(Lower=min(Distance),
                     Upper=max(Distance)) %>%
    as.data.frame()
  mat_margin_cluster_distance <- mat_margin_cluster_distance[order(mat_margin_cluster_distance$Lower),]
  
  for (clus in rev(mat_margin_cluster_distance$Speciality))
  {
    ranges_distance <- sort(range(as.numeric(as.vector(subset(mat_cluster_distance,Speciality==clus)[,'Distance']))))
    list_distances [[clus]] <- ranges_distance
    id_distance_cluster <- mat_line_by_line[,'Winner_Distance']>=ranges_distance[1] & mat_line_by_line[,'Looser_Distance']>=ranges_distance[1] & mat_line_by_line[,'Winner_Distance']>=ranges_distance[2] & mat_line_by_line[,'Looser_Distance']>=ranges_distance[2] & mat_line_by_line[,'Discipline']==discipline
    mat_line_by_line_subset <- mat_line_by_line[id_distance_cluster,]
    if(nrow(mat_line_by_line_subset)>5)
    {
      mat_line_by_line_subset$Date <- as.Date(mat_line_by_line_subset$Date)
      mat_line_by_line_subset  <- transform(mat_line_by_line_subset,Delay=Sys.Date()-Date)
      mat_line_by_line_subset <-  transform(mat_line_by_line_subset,WeekTempory=mat_line_by_line_subset$Delay/7)
      vec.values.times <- as.numeric(as.vector(mat_line_by_line_subset$WeekTempory))
      mat_line_by_line_subset$Week <- vec.values.times
      mat_line_by_line_subset <-  transform(mat_line_by_line_subset,Score=1)
      mat_line_by_line_subset  <- mat_line_by_line_subset[order(mat_line_by_line_subset$Date),]
      mat_line_by_line_subset  <- unique(mat_line_by_line_subset)
      if(length(mat_line_by_line_subset$Winner)>1 & length(mat_line_by_line_subset$Looser)>1 & length(unique(mat_line_by_line_subset$Week))>1)
      {
        mat_line_by_line_subset_rating <-  mat_line_by_line_subset[, c("Week","Winner","Looser","Score")]
        mat_line_by_line_subset_rating <- mat_line_by_line_subset_rating[!is.na(mat_line_by_line_subset_rating$Score),]
        mat_line_by_line_subset_rating <- mat_line_by_line_subset_rating[complete.cases(mat_line_by_line_subset_rating),]
        infos_res_glicko_specific <- glicko(mat_line_by_line_subset_rating,history=TRUE)
        # infos_res_glicko_specific <- glicko(mat.horses.direct.ranking.rating.subset,gamma=mat.horses.direct.ranking[id.distance.cluster,'gamma'],history=TRUE)
        mat_res_glicko_specific <- infos_res_glicko_specific$ratings
        rm(infos_res_glicko_specific)
        gc()
        mat_res_glicko_specific <- mat_res_glicko_specific[,c("Player","Rating")]
        colnames(mat_res_glicko_specific) <- sub("Player","Cheval",colnames(mat_res_glicko_specific))
        colnames(mat_res_glicko_specific) <- sub("Rating","Glicko",colnames(mat_res_glicko_specific))
        colnames(mat_res_glicko_specific) <- c(colnames(mat_res_glicko_specific)[1],paste(paste(ranges_distance,collapse ='-'),colnames(mat_res_glicko_specific)[2],sep="_"))
        
        infos_res_steph_specific <- steph(mat_line_by_line_subset_rating,history=TRUE)
        mat_res_steph_specific <- infos_res_steph_specific$ratings
        rm(infos_res_steph_specific)
        gc()
        mat_res_steph_specific <- mat_res_steph_specific[,c("Player","Rating")]
        colnames(mat_res_steph_specific) <- sub("Player","Cheval",colnames(mat_res_steph_specific))
        colnames(mat_res_steph_specific) <- sub("Rating","Steph",colnames(mat_res_steph_specific))
        colnames(mat_res_steph_specific) <- c(colnames(mat_res_steph_specific)[1],paste(paste(ranges_distance,collapse ='-'),colnames(mat_res_steph_specific)[2],sep="_"))
        
        infos_res_elo_specific <- elo(mat_line_by_line_subset_rating,history=TRUE)
        mat_res_elo_specific <- infos_res_elo_specific$ratings
        rm(infos_res_elo_specific)
        gc()
        mat_res_elo_specific <- mat_res_elo_specific[,c("Player","Rating")]
        colnames(mat_res_elo_specific) <- sub("Player","Cheval",colnames(mat_res_elo_specific))
        colnames(mat_res_elo_specific) <- sub("Rating","Elo",colnames(mat_res_elo_specific))
        colnames(mat_res_elo_specific) <- c(colnames(mat_res_elo_specific)[1],paste(paste(ranges_distance,collapse ='-'),colnames(mat_res_elo_specific)[2],sep="_"))
        mat_res_rating_specific <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list(mat_res_elo_specific,mat_res_glicko_specific,mat_res_steph_specific))
        # mat_res_rating_specific <- transform(mat_res_rating_specific,EGS=(mat_res_rating_specific[,2]+mat_res_rating_specific[,3]+mat_res_rating_specific[,4])/3)
        # colnames(mat_res_rating_specific) [ncol(mat_res_rating_specific)] <- paste("EGS",paste(ranges_distance,collapse ='-'),sep="_")
        # mat_res_rating_specific <- mat_res_rating_specific[,c("Cheval",paste("EGS",paste(ranges_distance,collapse ='-'),sep="_"))]
        list_res_rating_specific_compliled [[clus]] <- mat_res_rating_specific
      }
    }
  }
  list_res_rating_specific_compliled <- list_res_rating_specific_compliled[lapply(list_res_rating_specific_compliled,length)>0]
  mat_res_rating_specific_compliled <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list_res_rating_specific_compliled)
  mat_res_rating_all_together <- Reduce(function(x, y) merge(x, y, by.x="Cheval",by.y="Cheval",all=TRUE), list(A=mat_res_rating_global,B=mat_res_rating_specific_compliled))
  setwd(path_output_data)
  saveRDS(mat_res_rating_all_together,paste(paste("mat_res_ranking",tolower(gsub(" ","-",gsub("é","e",discipline))),sep="-"),".rds",sep=""))
}

# file_line_by_line <- dir(path=path_input_data,pattern = "line_by_line")
# mat_line_by_line <- readRDS(paste(path_input_data,file_line_by_line,sep="/"))
# mat_line_by_line <- mat_line_by_line[!is.na(mat_line_by_line$Discipline),]
# vec_disciplines <- as.vector(unique(mat_line_by_line$Discipline))
# for(discipline in rev(vec_disciplines))
# {
#   mat_line_by_line_rating <- subset(mat_line_by_line,Discipline %in% discipline)
#   mat_line_by_line_rating$Date <- as.Date(mat_line_by_line_rating$Date)
#   mat_line_by_line_rating  <- mat_line_by_line_rating[order(mat_line_by_line_rating$Date),]
#   mat_line_by_line_rating  <- unique(mat_line_by_line_rating)
#   mat_line_by_line_rating <- mat_line_by_line_rating [,c("Winner","Looser","Date")]
#   mat_line_by_line_rating <- mat_line_by_line_rating[complete.cases(mat_line_by_line_rating),]
#   mat_line_by_line_rating$Winner <- as.vector(mat_line_by_line_rating$Winner)
#   mat_line_by_line_rating$Looser <- as.vector(mat_line_by_line_rating$Looser)
#   mat_line_by_line_rating <- mat_line_by_line_rating[complete.cases(mat_line_by_line_rating),]
#   mat_line_by_line_rating <- mat_line_by_line_rating[mat_line_by_line_rating$Winner!=mat_line_by_line_rating$Looser,]
#   infos_res_generic_elo_golbal <- elo.seq(winner=mat_line_by_line_rating$Winner, loser=mat_line_by_line_rating$Looser, Date=mat_line_by_line_rating$Date,runcheck=FALSE)
#   mat_generic_elo_golbal <- data.frame(Cheval=names(extract.elo(infos_res_generic_elo_golbal)),Elo=as.numeric(extract.elo(infos_res_generic_elo_golbal)))
#   
#   write.csv(mat_generic_elo_golbal,file="mat_generic_elo_golbal_haies.csv")
#   saveRDS(infos_res_generic_elo_golbal,"infos_res_generic_elo_golbal_haies.rds")
#   
